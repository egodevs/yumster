<!doctype html>
<html>
  <head>
  <title>Yumster</title>
  <?php include_once( 'partial/meta.php' ); ?>
  </head>
  <body class="e500">
    <div class="max-width">
      <div class="error-wrapper">
        <img class="logo" src="<?php includeImg('logo.png'); ?>"/>
        <img class="e404" src="<?php includeImg('e500.png'); ?>"/>
        <p>Sorry, it appears there has been an internal server error with the page you’re requested. Please try refreshing.</p>
        <a href="index.php" class="btn">home page</a>
      </div>
    </div>
  </body>
</html>