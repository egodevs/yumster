<?php
class ControllerExtensionModuleBanner extends Controller {
	public function index($setting) {
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		$this->document->addStyle('catalog/view/javascript/jquery/swiper/css/swiper.min.css');
		$this->document->addStyle('catalog/view/javascript/jquery/swiper/css/opencart.css');
		$this->document->addScript('catalog/view/javascript/jquery/swiper/js/swiper.jquery.js');

		$data['banners'] = array();

		$results = $this->model_design_banner->getBanner($setting['banner_id']);

		foreach ($results as $result) {
			//if (is_file(DIR_IMAGE . $result['image'])) {
            //oshka  banner with dates
            if(($result['date_start'] == '0000-00-00' OR strtotime($result['date_start']) <= strtotime(date('Y-m-d',time()))) AND ($result['date_end'] == '0000-00-00' OR strtotime($result['date_end']) > strtotime(date('Y-m-d',time())))) {
				$data['banners'][] = array(
					'title' => htmlspecialchars_decode($result['title']),
					'link'  => $result['link'],
					'date_start '  => $result['date_start'],
					'date_end'  => $result['date_end'],
					//'image' => HTTP_SERVER."image/".$result['image']
					'image' => "image/".$result['image']
				);
            }
			//}
		}

		$data['banner_id']=$setting['banner_id'];


		$data['module'] = $module++;

		/*home top categories*/
		if($data['banner_id']==9||$data['banner_id']==10||$data['banner_id']==14) {

			if (isset($results[0]['name']) && $results[0]['name']) {
				$data['section_title'] = $results[0]['name'];
				$lang = $this->language->get('code');
				if (strpos($data['section_title'], '|') !== false) {
					$splitted = explode('|', $data['section_title']);
					if ($lang == 'he') {
						$data['section_title'] = $splitted[1];
					} else {
						$data['section_title'] = $splitted[0];
					}
				}


			}
		}
		if($data['banner_id']==9) {
			return $this->load->view('extension/module/banner_top_categories', $data);
		/*home_second_section*/
		} else if ($data['banner_id']==10){
			return $this->load->view('extension/module/banner_home_second_section', $data);
			/*} else if ($data['banner_id']==13){
                return $this->load->view('extension/module/banner_home_first_section', $data);*/
           } else if($data['banner_id']==14) {
			return $this->load->view('extension/module/banner_home_fourth_section', $data);
            }else {
			return $this->load->view('extension/module/banner', $data);
		}

	}
}