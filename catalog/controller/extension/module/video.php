<?php
class ControllerExtensionModuleVideo extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/video');
		if (isset($setting['video_text1'])) {
			$data['video_text1'] = html_entity_decode($setting['video_text1'], ENT_QUOTES, 'UTF-8');
		} else {
			$data['video_text1'] = '';
		}
		if (isset($setting['video_text2'])) {
			$data['video_text2'] = html_entity_decode($setting['video_text2'], ENT_QUOTES, 'UTF-8');
		} else {
			$data['video_text2'] = '';
		}
		if (isset($setting['video_link'])) {
			$data['video_link'] = html_entity_decode($setting['video_link'], ENT_QUOTES, 'UTF-8');
		} else {
			$data['video_link'] = '';
		}
		if (isset($setting['video_poster_img'])) {
			$data['video_poster_img'] = HTTP_SERVER."image/".html_entity_decode($setting['video_poster_img'], ENT_QUOTES, 'UTF-8');
		} else {
			$data['video_poster_img'] = '';
		}
		if (isset($setting['video_file'])) {
			//$data['video_file'] = HTTP_SERVER."image/".html_entity_decode($setting['video_file'], ENT_QUOTES, 'UTF-8');
			$data['video_file'] = "image/".html_entity_decode($setting['video_file'], ENT_QUOTES, 'UTF-8');
		} else {
			$data['video_file'] = '';
		}
		return $this->load->view('extension/module/video', $data);
	}
}