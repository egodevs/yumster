<?php
class ControllerExtensionPaymenttranzila extends Controller {
	public function index() {
		$this->load->language('extension/payment/tranzila');
		$this->load->model('checkout/order');
        $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		$data['suppliername']=$this->config->get('payment_tranzila_supplier');
		$data['orderid']=$this->session->data['order_id'];
		//$data['sum']=$order_info['total'];
		$data['sum'] = $this->currency->format($order_info['total'], ILS, '',false);
///var_dump($order_info);
		$data['orderemail']=urlencode($order_info['email']);
		$data['orderphone']=urlencode($order_info['telephone']);
		$data['ordername']=urlencode($order_info['payment_firstname']." ".$order_info['payment_lastname']);


		return $this->load->view('extension/payment/tranzila', $data);
	}

	public function callback() {
			
			if(isset($this->request->post['textfield3']));
			{
				$this->load->model('checkout/order');
			$this->model_checkout_order->addOrderHistory($this->request->post['textfield3'], $this->config->get('payment_tranzila_cumorder_status_id'),'', false);
			$this->model_checkout_order->createinvoice($this->request->post['textfield3']);
			echo '<script type="text/javascript">window.top.location.href = "' . $this->url->link('checkout/success', '', true) . '"; </script>';
			
			}
			echo '<script type="text/javascript">window.top.location.href = "' . $this->url->link('checkout/checkout', '', true) . '"; </script>';
			
			
	}
	
	public function failded() {
		if(isset($this->request->post['textfield3']));
			{
				$this->load->model('checkout/order');
		$this->model_checkout_order->addOrderHistory($this->request->post['textfield3'], $this->config->get('payment_tranzila_decorder_status_id')); 
		echo '<script type="text/javascript">window.top.location.href = "' . $this->url->link('extension/failded', '', true) . '"; </script>';
			
		
			}
			echo '<script type="text/javascript">window.top.location.href = "' . $this->url->link('checkout/checkout', '', true) . '"; </script>';
	}
}