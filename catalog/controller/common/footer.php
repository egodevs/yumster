<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');

		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}

		$data['contact'] = $this->url->link('information/contact');
		$data['return'] = $this->url->link('account/return/add', '', true);
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['tracking'] = $this->url->link('information/tracking');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', true);
		$data['affiliate'] = $this->url->link('affiliate/login', '', true);
		$data['special'] = $this->url->link('product/special');
		$data['account'] = $this->url->link('account/account', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['newsletter'] = $this->url->link('account/newsletter', '', true);

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = ($this->request->server['HTTPS'] ? 'https://' : 'http://') . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}

		$data['scripts'] = $this->document->getScripts('footer');

		$current_path = $this->request->get;
		if (isset($current_path['route']) && $current_path['route']) {
			$data['current_path_route'] = $current_path['route'];
		}

		if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post['catalog-email'])) {
			/*get catalog path*/
			$catalog_banner_image = "";
			$this->load->model('design/banner');
			$catalog_banner = $this->model_design_banner->getBanner(12);
			if($catalog_banner[0]['status']==1) {
				if (is_file(DIR_IMAGE . $catalog_banner[0]['image'])) {
					$catalog_banner_image = DIR_IMAGE.$catalog_banner[0]['image'];
				}
			}
			$mail = new Mail($this->config->get('config_mail_engine'));
			/*attach images*/

			$mail_header = DIR_TEMPLATE."yumster/assets/build/img/mail-header.jpg";
			//echo $mail_header."<br>".file_exists($mail_header);
			$mail->AddEmbeddedImage($mail_header, "mail-header", "mail-header.jpg", "base64", "image/jpg");

			/*for admin*/
			$message = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd\"> <html> <head> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"> <title>YumsterKids.Com. Catalog Request</title> </head> <body style=\"font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;\">  <table width=\"680\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 680px;\" > <tbody> <tr> <td style=\"padding:10px 0 0 0;\"><a href=\"https://yumsterkids.com/\" title=\"yumsterkids.com\"><img src=\"cid:mail-header\" alt=\"yumsterkids.com\" width=\"680\" height=\"184\" style=\"border: none;\" /></a></td> </tr> <tr bgcolor=\"#f9fafa\" > <td align=\"center\" style=\"font-size:16px;padding:40px;\">";
			$message .= '<p style="text-align:center">Hi!<br><br>A new request for the catalog was received from the site yumsterkids.com.</p>';
			$message .= '<p style="text-align:center">User address: <a href="mailto:'.$this->request->post['catalog-email'].'">'. $this->request->post['catalog-email'] .'</p>';
			$message .= '<p style="text-align:center">Have a good day!</p>';
			$message .= '</td> </tr> <tr bgcolor="#f9fafa" style="padding:10px;"> <td align="center"><p style="text-align: center"><a href="https://yumsterkids.com" title="yumsterkids.com"><img src="https://yumsterkids.com/catalog/view/theme/yumster/assets/build/img/mail-footer-logo.jpg" alt="yumsterkids.com" style="border: none;"></a></p>
			<p style="text-align: center; font-size:14px;font-weight:bold;">Thanks<br>Yumsterkids Admin</p> 
			<p style="text-align: center">
				<a href="https://twitter.com/"><img src="https://yumsterkids.com/catalog/view/theme/yumster/assets/build/img/mail-footer-twitter.png" width="24" height="24" style="border: none; padding:10px 3px"></a>
				<a href="https://www.instagram.com/yumsterkids/"><img src="https://yumsterkids.com/catalog/view/theme/yumster/assets/build/img/mail-footer-instagram.png" width="24" height="24" style="border: none; padding:10px 3px" /></a>
				<a href="https://www.facebook.com/yumsterkids/"><img src="https://yumsterkids.com/catalog/view/theme/yumster/assets/build/img/mail-footer-facebook.png" width="24" height="24" style="border: none; padding:10px 3px"></a>
				<a href="https://plus.google.com/"><img src="https://yumsterkids.com/catalog/view/theme/yumster/assets/build/img/mail-footer-google-plus.png" width="24" height="24" style="border: none; padding:10px 3px"></a>
			</p></td></tr></tbody></table></body></html>';



			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($this->config->get('config_email'));
			$mail->setFrom($this->config->get('config_email'));
			$mail->setReplyTo($this->request->post['catalog-email']);
			$mail->setSender(html_entity_decode($this->config->get('config_meta_title'), ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(html_entity_decode("Yumsterkds.Com: ".$this->language->get('catalog_request_subject'). " ". $this->request->post['catalog-email']), ENT_QUOTES, 'UTF-8');
			$mail->setHtml($message);
			$mail->send();

			// Send to additional alert emails
			$emails = explode(',', $this->config->get('config_mail_alert_email'));

			foreach ($emails as $email) {
				if ($email && filter_var($email, FILTER_VALIDATE_EMAIL)) {
					$mail->setTo($email);
					$mail->send();
				}
			}


			/*for user*/
			$user_message = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd\"> <html> <head> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"> <title>YumsterKids.Com. Catalog Request</title> </head> <body style=\"font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;\">  <table width=\"680\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 680px;\" > <tbody> <tr> <td style=\"padding:10px 0 0 0;\"><a href=\"https://yumsterkids.com/\" title=\"yumsterkids.com\"><img src=\"https://yumsterkids.com/catalog/view/theme/yumster/assets/build/img/mail-header.jpg\" alt=\"yumsterkids.com\" width=\"680\" height=\"184\" style=\"border: none;\" /></a></td> </tr> <tr bgcolor=\"#f9fafa\" > <td align=\"center\" style=\"font-size:16px;padding:40px;\">";
			$user_message .= "<p style=\"text-align:center\">Hi!</p>";
			$user_message .= "<p style=\"text-align:center\">We will send you a catalog from yumsterkids.com as soon as possible</p>";
			$user_message .= "<p style=\"text-align:center\">Have a good day!</p>";
			$user_message .= '</td> </tr> <tr bgcolor="#f9fafa" style="padding:10px;"> <td align="center"><p style="text-align: center"><a href="https://yumsterkids.com" title="yumsterkids.com"><img src="https://yumsterkids.com/catalog/view/theme/yumster/assets/build/img/mail-footer-logo.jpg" alt="yumsterkids.com" style="border: none;"></a></p>
			<p style="text-align: center; font-size:14px;font-weight:bold;">Thanks<br>Yumsterkids Admin</p> 
			<p style="text-align: center">
				<a href="https://twitter.com/"><img src="https://yumsterkids.com/catalog/view/theme/yumster/assets/build/img/mail-footer-twitter.png" width="24" height="24" style="border: none; padding:10px 3px"></a>
				<a href="https://www.instagram.com/yumsterkids/"><img src="https://yumsterkids.com/catalog/view/theme/yumster/assets/build/img/mail-footer-instagram.png" width="24" height="24" style="border: none; padding:10px 3px" /></a>
				<a href="https://www.facebook.com/yumsterkids/"><img src="https://yumsterkids.com/catalog/view/theme/yumster/assets/build/img/mail-footer-facebook.png" width="24" height="24" style="border: none; padding:10px 3px"></a>
				<a href="https://plus.google.com/"><img src="https://yumsterkids.com/catalog/view/theme/yumster/assets/build/img/mail-footer-google-plus.png" width="24" height="24" style="border: none; padding:10px 3px"></a>
			</p></td></tr></tbody></table></body></html>';

			if(!empty($catalog_banner_image)) {
				$mail->addAttachment($catalog_banner_image);
			}

			$mail->setTo($this->request->post['catalog-email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setReplyTo($this->config->get('config_email'));
			$mail->setSubject(html_entity_decode($this->language->get('catalog_request_subject'). ": Yumsterkds.Com"), ENT_QUOTES, 'UTF-8');
			$mail->setHtml($user_message);
			$mail->send();

		}

		// only for register page for PAYPALif () {
		if (isset($current_path['route']) && ($current_path['route'] == 'account/register' && null !== $this->config->get('module_pp_login_status'))) {
			$data['module_pp_login']=1;

			$data['paypal_client_id'] = $this->config->get('module_pp_login_client_id');
			$data['paypal_return_url'] = $this->url->link('extension/module/pp_login/login', '', true);

			if ($this->config->get('module_pp_login_sandbox')) {
				$data['paypal_sandbox'] = 'sandbox';
			} else {
				$data['paypal_sandbox'] = '';
			}

			if ($this->config->get('module_pp_login_button_colour') == 'grey') {
				$data['paypal_button_colour'] = 'neutral';
			} else {
				$data['paypal_button_colour'] = '';
			}

			$locale = $this->config->get('module_pp_login_locale');

			$this->load->model('localisation/language');

			$languages = $this->model_localisation_language->getLanguages();

			foreach ($languages as $language) {
				if ($language['status'] && ($language['code'] == $this->session->data['language']) && isset($locale[$language['language_id']])) {
					$data['paypal_locale'] = $locale[$language['language_id']];
				}
			}

			if (!isset($data['paypal_locale'])) {
				$data['paypal_locale'] = 'en-gb';
			}

			$scopes = array(
				'profile',
				'email',
				'address',
				'phone'
			);

			if ($this->config->get('module_pp_login_seamless')) {
				$scopes[] = 'https://uri.paypal.com/services/expresscheckout';
			}

			$data['paypal_scopes'] = implode(' ', $scopes);

		}
		
		
		return $this->load->view('common/footer', $data);
	}
}
