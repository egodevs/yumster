<?php
class ControllerCommonHeader extends Controller {
	public function index()
	{
		// Analytics
		$this->load->model('setting/extension');

		$data['analytics'] = array();

		$analytics = $this->model_setting_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get('analytics_' . $analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get('analytics_' . $analytic['code'] . '_status'));
			}
		}

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}

		$data['title'] = $this->document->getTitle();

		/*oshka start body_class code*/
		$current_path = $this->request->get;
		$body_class="";

		if (empty($current_path) || $current_path['route'] == 'common/home' || isset($current_path['fbclid'])) {
			$body_class = 'home index';
		} else {
			if (isset($current_path['route'])) {

				$body_class = explode('/', str_replace('product/', '', $current_path['route']));
				unset($current_path['route']);

				if (isset($current_path['_route_'])) {
					$body_class = array_merge($body_class, explode('/', str_replace('-', '_', $current_path['_route_'])));
					unset($current_path['_route_']);
				}
				foreach ($current_path as $key => $value) {
					$body_class[] = $key . "_" . $value;
				}

				$body_class = 'page_' . implode(" page_", array_unique($body_class));
			}
		}

		$body_class .= ' lang_' . $this->language->get('code');
		$data['body_class'] = $body_class;
		// end body_class code*/

		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts('header');
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		$data['name'] = $this->config->get('config_name');

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->language('common/header');

		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');

			$data['text_wishlist'] = $this->model_account_wishlist->getTotalWishlist();
		} else {
			$data['text_wishlist'] = isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0;
		}

		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));
		
		$data['home'] = $this->url->link('common/home');
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('account/account', '', true);
		$data['register'] = $this->url->link('account/register', '', true);
		$data['login'] = $this->url->link('account/login', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['download'] = $this->url->link('account/download', '', true);
		$data['logout'] = $this->url->link('account/logout', '', true);
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', true);
		$data['contact'] = $this->url->link('information/contact');
		$data['telephone'] = $this->config->get('config_telephone');
		
		$data['language'] = $this->load->controller('common/language');
		$data['currency'] = $this->load->controller('common/currency');
		$data['search'] = $this->load->controller('common/search');
		$data['cart'] = $this->load->controller('common/cart');
		$data['menu'] = $this->load->controller('common/menu');

		/*oshka*/
        //slogan_line
        //$this->load->model('setting/module');
        $slogan_line_block = $this->model_setting_module->getModule('47');
        $data['slogan_line_text']= html_entity_decode($slogan_line_block['module_description'][$this->config->get('config_language_id')]['title'], ENT_QUOTES, 'UTF-8');

		//var_dump($current_path);
		if (strpos($data['body_class'], 'home index') !== false|| strpos($data['body_class'], 'page_search') !== false) {
			return $this->load->view('common/header-home', $data);
		} else if ((strpos($data['body_class'], 'page_checkout') !== false)&&(strpos($data['body_class'], 'page_cart') == false)){
			return $this->load->view('common/header-checkout', $data);
		} else if (strpos($data['body_class'], 'page_account') !== false){
			$data['body_class']="profile-page ".$data['body_class'];
			return $this->load->view('common/header-account', $data);
		}else {
			return $this->load->view('common/header', $data);
		}


		return $this->load->view('common/header', $data);
	}
}
