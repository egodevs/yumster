<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->config->get('config_url'), 'canonical');
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		/*add image for home page modal*/
		$modal = $this->model_design_banner->getBanner(11);
		if($modal && $modal[0]['status']==1) {
			if (is_file(DIR_IMAGE . $modal[0]['image'])) {
			$data['modal_image']=HTTPS_SERVER."image/".$modal[0]['image'];
			$data['modal_timeout'] = $modal[0]['sort_order'];
			}
		}

		$this->response->setOutput($this->load->view('common/home', $data));
	}
}
