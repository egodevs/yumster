jQuery(document).ready( function($) {
    rtl = $('body').hasClass('lang_he');

    window.sr = ScrollReveal({mobile: false, easing: 'ease', viewFactor: 0.7});
    /*sr.reveal('.index-banner .bg',{delay: 300, duration: 1000, scale: 1, distance: 0, afterReveal:function(){
     sr.reveal('.index .header',{ duration: 500, scale: 1, opacity: 1, distance: '-100%'});
     sr.reveal('.index .sidebar',{ duration: 500, scale: 1, opacity: 1, distance: '-100%',origin: 'right'});
     sr.reveal('.index-banner .banner',{ duration: 1000, delay: 600, scale: 1, distance: 0});
     sr.reveal('.index-banner .banner-footer',{ duration: 500, delay: 900, scale: 1, opacity: 1, distance: '-100%',origin: 'top'});
     sr.reveal('.index-banner .point',{ duration: 500, delay: 1500, scale: 1, distance: 0, afterReveal:function(){
     $('.index-banner .point .img span').width('100%');
     sr.reveal('.index-banner .point a',{ duration: 500, delay: 500, scale: 1, distance: 0});
     }});
     }});*/

    /*oshka*/
    console.log($(window).width());
    sr.reveal('.index .header', {duration: 500, delay: 200, scale: 1, opacity: 1, distance: '-100%'});
    if ($(window).width() > 1155) {
        if (rtl) {
            sr.reveal('.index .sidebar', {duration: 500, scale: 1, opacity: 1, distance: '-100%', origin: 'left'});
        } else {
            sr.reveal('.index .sidebar', {duration: 500, scale: 1, opacity: 1, distance: '-100%', origin: 'right'});
        }

    sr.reveal('.index .sidebar',{ duration: 500, delay:300, scale: 1, opacity: 1, distance: '-100%',origin: 'right'});
    }
    sr.reveal('.index-banner .banner',{ duration: 1000, delay: 100, scale: 1, distance: 0});
    sr.reveal('.index-banner .banner-footer',{   duration: 500,delay:400,  scale: 1, opacity: 1, distance: '-100%',origin: 'top'});

    /*oshka*/

    sr.reveal('.index-banners .banner',{ duration: 1000, scale: 1, distance: 0}, 100);

    sr.reveal('.top-categories .title',{ duration: 1000, scale: 1, distance: 0});
    sr.reveal('.top-categories li',{ duration: 1000, scale: 1, distance: 0}, 100);

    sr.reveal('.footer',{ duration: 500, delay: 300, scale: 1, opacity: 1, distance: '-100%',origin: 'top', viewFactor: 0.2});

    sr.reveal('body:not(.index) .header',{ delay:300, duration: 500, scale: 1, opacity: 1, distance: '-100%'});
    if ($(window).width() > 1155) {
        if(rtl){
            sr.reveal('body:not(.index) .sidebar',{ delay:300, duration: 500, scale: 1, opacity: 1, distance: '-100%', origin: 'left', viewFactor: 0});
            sr.reveal('.profile-sidebar',{ delay:300, duration: 500, scale: 1, opacity: 1, distance: '-100%', origin: 'left', viewFactor: 0});
        }else{
            sr.reveal('body:not(.index) .sidebar',{ delay:300, duration: 500, scale: 1, opacity: 1, distance: '-100%', origin: 'right', viewFactor: 0});
            sr.reveal('.profile-sidebar',{ delay:300, duration: 500, scale: 1, opacity: 1, distance: '-100%', origin: 'right', viewFactor: 0});
        }
        sr.reveal('body:not(.index) .sidebar',{ delay:300, duration: 500, scale: 1, opacity: 1, distance: '-100%', origin: 'right', viewFactor: 0});
    }

    sr.reveal('.profile-sidebar',{ delay:300, duration: 500, scale: 1, opacity: 1, distance: '-100%', origin: 'right', viewFactor: 0});

    sr.reveal('.product-wrapper .product-slider',{ delay:600, duration: 500, scale: 1, distance: 0});
    /*sr.reveal('.product-wrapper .product-info',{ delay:900, duration: 500, scale: 1, distance: 0});*/

    sr.reveal('.profile-wrapper',{ delay:600, duration: 500, scale: 1, distance: 0});


    sr.reveal('.catalog-wrapper .product-item',{ duration: 1000, scale: 1, distance: 0}, 100);
    sr.reveal('.favorites-wrapper .product-item',{ duration: 1000, scale: 1, distance: 0}, 100);

    sr.reveal('.inner-container .banner',{ delay:600, duration: 500, scale: 1, distance: 0});
    sr.reveal('.about-points li',{ duration: 1000, delay: 900, scale: 1, distance: 0}, 100);

    sr.reveal('.contact-wrapper',{ delay:600, duration: 500, scale: 1, distance: 0});
    sr.reveal('.contact-wrapper .col-2',{ delay:600, duration: 500, scale: 1, opacity: 1, distance: '-100%', origin: 'left'});

    $(document).on('click', '.dropdown .current', function(e){
        e.preventDefault();
        $(this).closest('.dropdown').toggleClass('visible');
        $('.dropdown').not($(this).closest('.dropdown')).removeClass('visible');
    });

    $(document).on('click', '.dropdown', function(e){
        e.stopPropagation();
    });

    $(document).on('click', 'body', function(e){
        $('.dropdown').removeClass('visible');
        $('.cart-popup').removeClass('visible');
    });

    $(document).on('click', '.menu-toggler', function(e){
        $('body').toggleClass('show-menu');
    });

    $(document).on('click', '.sidebar .parent>a', function(e){
        e.preventDefault();
        /*oshka main page*/
        var isOpen = ($(this).parent().hasClass('show-children'))?"1":"0";
        $('.sidebar .menu .parent').removeClass('show-children');
        $(".sidebar .parent ul").hide();
        if(isOpen!="1") {
            $(this).next('ul').slideToggle(400);
            $(this).closest('.parent').addClass('show-children')
        }
        //console.log(isOpen);
        /*$(".sidebar .parent ul").hide();
        $(this).next('ul').slideToggle(400);
        $(this).closest('.parent').toggleClass('show-children')*/
    });

    $(document).on('click', '.header-right .cart', function(e){
        if($(window).innerWidth()>767){
            e.preventDefault();
            e.stopPropagation();
            $('.cart-popup').toggleClass('visible');
        }
    });

    $(document).on('click', '.cart-popup', function(e){
        e.stopPropagation();
    });

    //$('.menu .show-children>ul').show();

    $(document).on('click', '.product-description .more', function(e){
        e.preventDefault();
        $(this).closest('.product-description').toggleClass('active')
    });

    /*if($('.customselect').length)
     customSelect();*/

    /*  $(document).on('change', '.dropdown select', function(e){
     var $select = $(this).closest('.dropdown');
     $select.find('.current').text($(this).find('option:selected').text());
     });

     $(document).on('click', '.custom-dropdown li', function(e){
     e.preventDefault();
     e.stopPropagation();

     var $this = $(this),
     $select = $this.closest('.custom-dropdown');

     $select.find('li').removeClass('active');
     $this.addClass('active');
     $select.find('select').val($this.attr('data-value')).trigger('change');
     $select.find('.current').text($this.text());
     $select.removeClass('visible');
     });*/


    $(document).on('change', '.dropdown select', function(e){
        var $select = $(this).closest('.dropdown');
        var index = $(this).find('option:not([default])').index($(this).find('option:selected'));
        $select.find('.current').html($select.find('li:eq('+index+')').html());
    });

    $(document).on('click', '.dropdown li[data-value]', function(e){

        var $this = $(this),
            $select = $this.closest('.dropdown');

        $select.find('li').removeClass('active');
        $this.addClass('active');
        $select.find('select').val($this.attr('data-value')).trigger('change');
        $select.find('.current').html($this.html());
        $select.removeClass('visible');
    });

    $(document).on('click', '.quantity .minus', function(e){

        var $this = $(this),
            $input = $this.closest('.quantity').find('.input-quantity'),
            value = parseInt($input.val());

        value = (value>1)?(--value):1;
        $input.val(value);
        /*oshka: change price value on chaning quantity*/
        //find if main product or additional
        if ($this.closest('.product-options-row').hasClass("product-options-main")) {
            var $price = $(".product-price-main").attr("data-initial-price");
        } else if ($this.closest('.product-options-row').hasClass("cart-quantity")) {
            var $price = $this.closest('.product-info').find('.product-price').attr("data-initial-price");
        } else {
            var $price = $this.closest('.product-options-row').find('.product-price').attr("data-initial-price");
        }
        if($price.indexOf("₪") != -1){
            $price_currency = "₪";
        } else {
            $price_currency = "$";
        }
        var $price_value=parseFloat($price.replace($price_currency,'')).toFixed(2);
        var $new_price =  $price_currency + parseFloat($price_value*value).toFixed(2);
        if ($this.closest('.product-options-row').hasClass("product-options-main")) {
            $(".product-price-main").text($new_price);
        }  else if ($this.closest('.product-options-row').hasClass("cart-quantity")) {
            $this.closest('.product-info').find('.product-price').text($new_price);
        } else {
            $this.closest('.product-options-row').find('.product-price').text($new_price);
        }
        /*oshka*/
        if($("body").hasClass("page_cart")){
            $this.closest('.product-options').find('.update_button').trigger('click');
        }
    });

    $(document).on('click', '.quantity .plus', function(e){
        var $this = $(this),
            $input = $this.closest('.quantity').find('.input-quantity'),
            value = parseInt($input.val());

        value = (value<99)?(++value):99;
        $input.val(value);
        /*oshka change price value*/
        //find if main product or additional
        if ($this.closest('.product-options-row').hasClass("product-options-main")) {
            var $price = $(".product-price-main").attr("data-initial-price");
        } else if ($this.closest('.product-options-row').hasClass("cart-quantity")) {
            var $price = $this.closest('.product-info').find('.product-price').attr("data-initial-price");
        } else {
            var $price = $this.closest('.product-options-row').find('.product-price').attr("data-initial-price");
        }
        if($price.indexOf("₪") != -1){
            $price_currency = "₪";
        } else {
            $price_currency = "$";
        }
        var $price_value=parseFloat($price.replace($price_currency,'')).toFixed(2);
        var $new_price =  $price_currency + parseFloat($price_value*value).toFixed(2);
        if ($this.closest('.product-options-row').hasClass("product-options-main")) {
            $(".product-price-main").text($new_price);
        } else if ($this.closest('.product-options-row').hasClass("cart-quantity")) {
            $this.closest('.product-info').find('.product-price').text($new_price);
        } else {
            $this.closest('.product-options-row').find('.product-price').text($new_price);
        }
        /*oshka*/
        if($("body").hasClass("page_cart")){
            $this.closest('.product-options').find('.update_button').trigger('click');
        }
    });
    $(document).on('change keyup paste', '.input-quantity', function(e){
        $(this).closest('.product-options').find('.update_button').trigger('click');
    });

    if($('.other-product-wrapper').length){
        initOtherSlider();
    }

    $(document).on('click', '[data-popup]', function(e){
        e.preventDefault();

        showPopup($(this).attr('data-popup'));
    });

    $(document).on('click', '.popup .close, .popup', function(e){
        e.preventDefault();
        if( !$(this).parent().parent("#dsl_popup").length ) {
            hidePopup($(this).closest('.popup').attr('id'));
        }

    });

    $(document).on('click', '.popup-body, .popup', function(e){
        e.stopPropagation();
    });

    $(document).on('click', '.edit-toggler', function(e){
        e.preventDefault();
        $(this).closest('.checkout-block').removeClass('passed');
    });

    $(document).on('click', '.address-form button', function(e){
        e.preventDefault();
        $(this).closest('.checkout-block').addClass('passed');
        $(this).closest('.checkout-block').next('.checkout-block').removeClass('disabled')
    });

    $(document).on('change', '.payment-form .radio input', function(){
        var $this = $(this);
        $('.card-form').removeClass('visible');
        $('.card-form input').val('');

        if($this.prop('checked')){
            $this.closest('.field').next('.card-form').addClass('visible');
        }
    });

    $(document).on('change', '.checkout-block .radio input', function(){
        $(this).closest('.checkout-block').next('.checkout-block').removeClass('disabled');
    });

    $('textarea').keyup(function(e){
        textAreaAdjust(this);
    });

    $(document).on('click', 'dt', function(e){
        e.preventDefault();
        $(this).toggleClass('active');
    });


    $(document).on('click', '.thumbs .around', function(e){
        e.preventDefault();
        $(this).closest('.product-slider').find('.around-wrapper').toggleClass('visible');
    });

    $(document).on('click', '.thumbs .video', function(e){
        e.preventDefault();
        $(this).closest('.product-slider').find('.video-wrapper').toggleClass('visible');
    });


    $(document).on('click', '.product-slider .around-wrapper .close, .product-slider .video-wrapper .close', function(e){
        e.preventDefault();
        $(this).parent().removeClass('visible');
    });

    $(document).on('click', '.tabs .tab', function(e){
        e.preventDefault();
        var index = $(this).closest('.tabs').find('.tab').index($(this));
        $(this).closest('.tabs').next('.tab-contents').find('.tab-content:eq('+index+')').addClass('active').siblings('.tab-content').removeClass('active');
        $(this).addClass('active').siblings('.tab').removeClass('active');
    });


    /* $(document).on('click', '.add-to-cart', function(e){
     e.preventDefault();
     $(this).addClass('active');
     });*/

    $('.decimal').keypress(function(e){
        e = e || event;
        if (e.ctrlKey || e.altKey || e.metaKey) return;
        var chr = getChar(e);
        if (chr == null) return;
        if ((chr < '0' || chr > '9')) {
            return false;
        }
    });

    if($('.inner-container .product-slider').length){
        initProductSlider($('.inner-container .product-slider'))
    }

    //initBt1();

});

/*function initProductSlider(slider){
 var image_count = slider.find(".thumbs .swiper-slide").length;
 console.log(image_count);
 if (image_count > 2) {
 if (image_count >= 3) image_count=3;
 var galleryThumbs = new Swiper(slider.find('.thumbs .swiper-container'), {
 direction: 'vertical',
 slidesPerView: image_count,
 spaceBetween: 15,
 loop: true,
 slideToClickedSlide: true,
 breakpoints: {
 1590: {
 spaceBetween: 12
 }
 }
 });

 var galleryFull = new Swiper(slider.find('.full .swiper-container'), {
 slidesPerView: image_count,
 spaceBetween: 0,
 loop: true,
 breakpoints: {
 767: {
 slidesPerView: 1,
 centeredSlides: true
 }
 },
 nextButton: slider.find('.full .swiper-button-next'),
 prevButton: slider.find('.full .swiper-button-prev')
 });

 galleryThumbs.params.control = galleryFull;
 galleryFull.params.control = galleryThumbs;
 }

 }*/

function initProductSlider(slider){
    var galleryThumbs = new Swiper(slider.find('.thumbs .swiper-container'), {
        direction: 'vertical',
        slidesPerView: 3,
        spaceBetween: 15,
        loop: true,
        slideToClickedSlide: true,
        breakpoints: {
            1590: {
                spaceBetween: 12
            }
        }
    });

    var galleryFull = new Swiper(slider.find('.full .swiper-container'), {
        zoom: true,
        slidesPerView: 3,
        spaceBetween: 0,
        loop: true,
        breakpoints: {
            767: {
                slidesPerView: 1,
                centeredSlides: true
            }
        },
        nextButton: slider.find('.full .swiper-button-next'),
        prevButton: slider.find('.full .swiper-button-prev')
    });

    galleryThumbs.params.control = galleryFull;
    galleryFull.params.control = galleryThumbs;

}

function destroyProductSlider(slider){

    var galleryThumbs = slider.find('.thumbs .swiper-container')[0].swiper;
    galleryThumbs.destroy(true, true);

    var galleryFull = slider.find('.full .swiper-container')[0].swiper;
    galleryFull.destroy(true, true);

}

function getChar(event) {
    if (event.which == null) {
        if (event.keyCode < 32) return null;
        return String.fromCharCode(event.keyCode)
    }

    if (event.which!=0 && event.charCode!=0) {
        if (event.which < 32) return null;
        return String.fromCharCode(event.which)
    }

    return null;
}

function textAreaAdjust(o) {
    o.style.height = "1px";
    o.style.height = (26+o.scrollHeight)+"px";
}

function customSelect(){
    var $selects = $('.customselect');

    $selects.each(function(){
        var $this = $(this),
            select = document.createElement('div'),
            current = document.createElement('span'),
            dropdown = document.createElement('ul');

        select.className = 'dropdown custom-dropdown';
        current.className = 'current';

        if(typeof $this.attr('readonly') != 'undefined') select.className += ' readonly';

        $(dropdown).empty();

        $this.children('option').each(function(){
            var $option = $(this),
                listItem = document.createElement('li');

            if($option.is(':selected')){
                listItem.className = 'active';
                $(current).text($option.text());
            }
            if(typeof $option.attr('default') != 'undefined'){
                return;
            }

            $(listItem).html($option.text());
            $(listItem).attr('data-value',$option.val());
            $(dropdown).append($(listItem));
        });

        $this.before(select);

        $(select).append(current).append(dropdown).append($this);
    });
};

function showPopup(id){
    $('#'+id).show();
    if($('#'+id).find('.product-slider')){
        initProductSlider($('#'+id+ ' .product-slider'))
    }
    $('body').addClass('show-popup');
};

function hidePopup(id){
    $('#'+id).hide();
    if($('#'+id).find('.product-slider')){
        destroyProductSlider($('#'+id+ ' .product-slider'))
    }
    $('body').removeClass('show-popup');
};

function initOtherSlider(){
    $('.other-product-wrapper').each(function(){
        var $this = $(this);
        var size = parseInt($this.attr('data-size'));
        var size_1 = parseInt($this.attr('data-size-1'));
        var size_2 = parseInt($this.attr('data-size-2'));
        var size_3 = parseInt($this.attr('data-size-3'));

        var swiper = new Swiper($this.find('.swiper-container'), {
            loop: true,
            speed: 400,
            spaceBetween: 22,
            slidesPerView: size,
            breakpoints: {
                1590: {
                    slidesPerView: size_1
                },
                1179: {
                    slidesPerView: size_2,
                    spaceBetween: 16
                },
                767: {
                    slidesPerView: size_3,
                    spaceBetween: 10
                }
            }
        });
    });

    $(document).on('click','.other-product-wrapper .swiper-prev', function(){
        var mySwiper = $(this).closest('.other-product-wrapper').find('.swiper-container')[0].swiper;
        mySwiper.slidePrev();
    });
    $(document).on('click','.other-product-wrapper .swiper-next', function(){
        var mySwiper = $(this).closest('.other-product-wrapper').find('.swiper-container')[0].swiper;
        mySwiper.slideNext();
    });
};

function initBt1() {
    var bt1 = $('#button-1');
    var bt1c = bt1.find('.button__container');
    var $circlesTopLeft = bt1.find('.circle.top-left');
    var $circlesBottomRight = bt1.find('.circle.bottom-right');

    var filter = $('#filter-goo-1 feGaussianBlur');

    var tl = new TimelineLite();
    var tl2 = new TimelineLite();

    var btTl = new TimelineLite({
        paused: true,
        onUpdate: function() {
            filter.attr('x', 0);
        },
        onComplete: function() {
            bt1c.attr('filter','none');
        }
    });

    tl.to($circlesTopLeft, 1.2, { x: -25, y: -25, scaleY: 2, ease: SlowMo.ease.config(0.1, 0.7, false) });
    tl.to($circlesTopLeft[0], 0.1, { scale: 0.2, x: '+=6', y: '-=2' });
    tl.to($circlesTopLeft[1], 0.1, { scaleX: 1, scaleY: 0.8, x: '-=10', y: '-=7' }, '-=0.1');
    tl.to($circlesTopLeft[2], 0.1, { scale: 0.2, x: '-=15', y: '+=6' }, '-=0.1');
    tl.to($circlesTopLeft[0], 1, { scale: 0, x: '-=5', y: '-=15', opacity: 0 });
    tl.to($circlesTopLeft[1], 1, { scaleX: 0.4, scaleY: 0.4, x: '-=10', y: '-=10', opacity: 0 }, '-=1');
    tl.to($circlesTopLeft[2], 1, { scale: 0, x: '-=15', y: '+=5', opacity: 0 }, '-=1');

    var tlBt1 = new TimelineLite();
    var tlBt2 = new TimelineLite();

    tlBt1.set($circlesTopLeft, { x: 0, y: 0, rotation: -45 });
    tlBt1.add(tl);

    tl2.to($circlesBottomRight, 1.2, { x: 25, y: 25, scaleY: 2, ease: SlowMo.ease.config(0.1, 0.7, false) });
    tl2.to($circlesBottomRight[0], 0.1, { scale: 0.2, x: '-=6', y: '+=3' });
    tl2.to($circlesBottomRight[1], 0.1, { scale: 0.8, x: '+=7', y: '+=3' }, '-=0.1');
    tl2.to($circlesBottomRight[2], 0.1, { scale: 0.2, x: '+=15', y: '-=6' }, '-=0.1');
    tl2.to($circlesBottomRight[0], 1, { scale: 0, x: '+=5', y: '+=15', opacity: 0 });
    tl2.to($circlesBottomRight[1], 1, { scale: 0.4, x: '+=7', y: '+=7', opacity: 0 }, '-=1');
    tl2.to($circlesBottomRight[2], 1, { scale: 0, x: '+=15', y: '-=5', opacity: 0 }, '-=1');

    tlBt2.set($circlesBottomRight, { x: 0, y: 0, rotation: -45 });
    tlBt2.add(tl2);

    btTl.add(tlBt1);
    btTl.to(bt1.parent().find('.button__bg'), 0.8, { scaleY: 1.1 }, 0.1);
    btTl.add(tlBt2, 0.2);
    btTl.to(bt1.parent().find('.button__bg'), 1.8, { scale: 1, ease: Elastic.easeOut.config(1.2, 0.4) }, 1.2);

    btTl.timeScale(2.6);

    bt1.on('mousedown', function(e) {
        bt1c.attr('filter','url(#filter-goo-1)');
        btTl.restart();
    });
}

/*OSHKA*/
// Language Desktop
$(document).on('click', '.header-right #form-language li', function(e){
    e.preventDefault();
    var selected_lang = $(this).data('value');
    $('.header-right #form-language input[name=\'code\']').val(selected_lang);
    $('.header-right #form-language').submit();
});
// Language mobile
$('.mobile-menu #form-language select').change(function(){
    var selected_lang = $(this).val();
    $('.mobile-menu #form-language input[name=\'code\']').val(selected_lang);
    $('.mobile-menu #form-language').submit();
});
// currency Desktop
$(document).on('click', '.header-right #form-currency li', function(e){
    e.preventDefault();
    var selected_currency = $(this).data('value');
    $('.header-right #form-currency input[name=\'code\']').val(selected_currency);
    $('.header-right #form-currency').submit();
});
// currency mobile
$('.mobile-menu #form-currency select').change(function(){
    var selected_currency = $(this).val();
    $('.mobile-menu #form-currency input[name=\'code\']').val(selected_currency);
    $('.mobile-menu #form-currency').submit();
});
//add class boy and girl
if (!$("body").hasClass('home')) {
$('.menu > ul > li:first-of-type').addClass("boys");
$('.menu > ul > li:nth-of-type(2)').addClass("girls");
}
$('.product-wrapper .product-slider .thumbs li a.share').click(function(e){
    e.preventDefault();
    $('.addthis_toolbox').slideToggle("slow");
});
/*pagination*/
$(".pagination li a:contains(first),.pagination li a:contains(last)").remove();
$(".pagination li a:contains(prev)").empty().parent().addClass("prev");
$(".pagination li a:contains(next)").empty().parent().addClass("next");

/*category page popup*/
$(document).on('click', '.quick-view-category', function(e){
    e.preventDefault();
    var prodid = $(this).data('prodid');
    var quickid = $(this).attr('data-popup');
    //console.log(prodid);
    $('#quick-view .quick-wrapper').load('index.php?route=product/product&product_id='+prodid+' .product-wrapper > *', function(){
        showPopupQV();
        initProductSlider($('#quick-view .product-slider'));
    });
});


function showPopupQV(){
    $('#quick-view').show();
    $('body').addClass('show-popup');
};
function hidePopupQV(){
    $('#quick-view').hide();
    $('#quick-view .quick-wrapper').html('');
    $('body').removeClass('show-popup');
};

$('body').on('click', '#quick-view .product-info #button-cart', function() {
    $.ajax({
        url: 'index.php?route=checkout/cart/add',
        type: 'post',
        data: $('#quick-view .product-info input[type=\'text\'], #quick-view .product-info input[type=\'hidden\'], #quick-view .product-info input[type=\'radio\']:checked, #quick-view .product-info input[type=\'checkbox\']:checked, #quick-view .product-info select, #quick-view .product-info textarea'),
        dataType: 'json',
        beforeSend: function() {
            //$('#button-cart').button('loading');
        },
        complete: function() {
            //$('#button-cart').button('reset');
        },
        success: function(json) {
            $('.alert-dismissible, .text-danger').remove();
            $('.form-group').removeClass('has-error');
            $('.added-to-cart').empty();


            if (json['error']) {
                if (json['error']['option']) {
                    for (i in json['error']['option']) {
                        var element = $('#input-option' + i.replace('_', '-'));

                        if (element.parent().hasClass('input-group')) {
                            element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                        } else {
                            element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                        }
                    }
                }

                if (json['error']['recurring']) {
                    $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                }

                // Highlight any found errors
                $('.text-danger').parent().addClass('has-error');
            }

            if (json['success']) {
                /*$('.breadcrumb').after('<div class="alert alert-success alert-dismissible">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');*/
                $('<div class="success-text"><img src="catalog/view/theme/yumster/assets/build/img/yes.png">'+json['success']+'</div>').insertBefore('.product-price-wrapper').show().delay(5000).fadeOut();
                $("#button-cart").addClass('active');
                $('#cart > span').html(json['total']);

                $('.cart-popup').load('index.php?route=common/cart/info .cart-popup > *');
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});

$('body').on('click', '.combined-with-wrapper .product-intro .add-to-cart', function() {
    //console.log($(this).parent().find("[name='quantity']").val());
    //console.log($(this).parents('.product-item').attr('id'));
    var curr_id = $(this).parents('.product-item').attr('id');
    //.parents('.fruit').attr('id'))
    $.ajax({
        url: 'index.php?route=checkout/cart/add',
        type: 'post',
        //data: $('#quick-view .product-info input[type=\'text\'], #quick-view .product-info input[type=\'hidden\'], #quick-view .product-info input[type=\'radio\']:checked, #quick-view .product-info input[type=\'checkbox\']:checked, #quick-view .product-info select, #quick-view .product-info textarea'),
        data: $('#'+curr_id+' input[type=\'text\'], #'+curr_id+' input[type=\'hidden\'], #'+curr_id+' input[type=\'radio\']:checked, #'+curr_id+' input[type=\'checkbox\']:checked, #'+curr_id+' select, #'+curr_id+' textarea'),
        dataType: 'json',
        beforeSend: function() {
            //$('#button-cart').button('loading');
        },
        complete: function() {
            //$('#button-cart').button('reset');
        },
        success: function(json) {
            $('.alert-dismissible, .text-danger').remove();
            $('.form-group').removeClass('has-error');
            $('.added-to-cart').empty();


            if (json['error']) {
                if (json['error']['option']) {
                    for (i in json['error']['option']) {
                        var element = $('#input-option' + i.replace('_', '-'));

                        if (element.parent().hasClass('input-group')) {
                            element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                        } else {
                            element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                        }
                    }
                }

                if (json['error']['recurring']) {
                    $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                }

                // Highlight any found errors
                $('.text-danger').parent().addClass('has-error');
            }

            if (json['success']) {
                $('.added-to-cart').append('<img src="catalog/view/theme/yumster/assets/build/img/yes.png">'+json['success']).show().delay(5000).fadeOut();
                $('#'+curr_id+' .add-to-cart').addClass('active');
                $('#cart > span').html(json['total']);

                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $('.cart-popup').load('index.php?route=common/cart/info .cart-popup > *');
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});

/*product slider with only one additional images*/
$('body .product-wrapper').on('click', '.no-swiper-container .swiper-slide:first', function() {
    $(this).closest('.product-slider').find('.full .swiper-slide:first' ).show();
    $(this).closest('.product-slider').find('.full .swiper-slide:eq( 1 )' ).hide();
});
$('body .product-wrapper').on('click', '.no-swiper-container .swiper-slide:eq( 1 )', function() {
    $(this).closest('.product-slider').find('.full .swiper-slide:first' ).hide();
    $(this).closest('.product-slider').find('.full .swiper-slide:eq( 1 )' ).show();
});





/*show aggree*/
$(document).on('click', '.agree', function(e){
    e.preventDefault();
    var element = this;
    var element_content = $(element).attr('href')
    $('#quick-view .quick-wrapper').load(element_content, function(){
        showPopupQV();
    });
});
$(document).on('click', '[href="#size-chart"]', function(e){
    e.preventDefault();
    $('#size-chart').show();
    $('body').addClass('show-popup');
});


$( document ).ready(function() {
    if ($(window).width() < 1155) {
        var poster_url=  $(".homepage-hero-module .fillWidth").data("poster");
        //console.log(poster_url);
        //$(".homepage-hero-module .poster img").attr("src",poster_url);
        $(".homepage-hero-module .fillWidth").attr("poster",poster_url);
    }
$(".mute-video").click( function (){
    if( $(".index-banner video").prop('muted') ) {
        $(".index-banner video").prop('muted', false);
        $(this).find('img').attr('src','/image/catalog/home/mute-off.png');
    } else {
        $(".index-banner video").prop('muted', true);
        $(this).find('img').attr('src','/image/catalog/home/mute-on.png');
    }
});
});

/*main page modal banner*/
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
/*homePopup*/
$(document).ready(function() {
    if (jQuery('#index-modal-banner').length) {
        var modal_timeout=$("#index-modal-banner").attr("data-timeout");
            setTimeout(function(){
                $("#index-modal-banner").show();
            }, modal_timeout*1000);
    }
});
$(document).ready(function() {
    $('.product-options-main .product-price').on('click', function() {
        $('.product-options-main .add-to-cart').trigger('click');
    });

});


function TransposeTable(tableId)
{

    var tbl = $('#' + tableId);
    var tbody = tbl.find('tbody');
    var oldWidth = tbody.find('tr:first td').length;
    var oldHeight = tbody.find('tr').length;
    var newWidth = oldHeight;
    var newHeight = oldWidth;

    var jqOldCells = tbody.find('td');

    var newTbody = $("<tbody></tbody>");
    for(var y=0; y<newHeight; y++)
    {
        var newRow = $("<tr></tr>");
        for(var x=0; x<newWidth; x++)
        {
            newRow.append(jqOldCells.eq((oldWidth*x)+y));
        }
        newTbody.append(newRow);
    }

    tbody.replaceWith(newTbody);
}
$(document).ready(function() {
    if ($(window).width() < 768) {
        $('#size-chart-table .img').remove()
        TransposeTable('size-chart-table');
    }
});

function scrollToAnchor(aid){
    var aTag = $("a[name='"+ aid +"']");
    $('html,body').animate({scrollTop: aTag.offset().top},'slow');
}
/*live search*/
var LiveSearchJs = function () {

    var init = function(options) {
        var live_search = {
            selector: "#search input[name='search']",
            text_no_matches: options.text_empty,
            height: '50px'
        }
        // console.log(options);

        // Initializing drop down list
        var html = '<div class="live-search"><ul></ul><div class="result-text"></div></div>';
        $(live_search.selector).after(html);

        $(live_search.selector).autocomplete({
            'source': function(request, response) {
                var filter_name = $(live_search.selector).val();
                var cat_id = 0;
                var live_search_min_length = options.module_live_search_min_length;
                if (filter_name.length < live_search_min_length) {
                    $('.live-search').css('display','none');
                }
                else{
                    var live_search_href = 'index.php?route=extension/module/live_search&filter_name=';
                    var all_search_href = 'index.php?route=product/search&search=';
                    if(cat_id > 0){
                        live_search_href = live_search_href + encodeURIComponent(filter_name) + '&cat_id=' + Math.abs(cat_id);
                        all_search_href = all_search_href + encodeURIComponent(filter_name) + '&category_id=' + Math.abs(cat_id);
                    }
                    else{
                        live_search_href = live_search_href + encodeURIComponent(filter_name);
                        all_search_href = all_search_href + encodeURIComponent(filter_name);
                    }

                    var html  = '<li style="text-align: center;height:10px;">';
                    html += '<img class="loading" src="catalog/view/javascript/live_search/loading.gif" />';
                    html += '</li>';
                    $('.live-search ul').html(html);
                    $('.live-search').css('display','block');

                    $.ajax({
                        url: live_search_href,
                        dataType: 'json',
                        success: function(result) {
                            var products = result.products;
                            $('.live-search ul li').remove();
                            $('.result-text').html('');
                            if (!$.isEmptyObject(products)) {
                                var show_image       = options.module_live_search_show_image;
                                var show_price       = options.module_live_search_show_price;
                                var show_description = options.module_live_search_show_description;
                                var show_add_button  = options.module_live_search_show_add_button;

                                $('.result-text').html('<a href="'+all_search_href+'" class="view-all-results">'+options.text_view_all_results+' ('+result.total+')</a>');
                                $.each(products, function(index,product) {
                                    var html = '<li>';
                                    // show_add_button
                                    if(show_add_button){
                                        html += '<div class="product-add-cart">';
                                        html += '<a href="javascript:;" onclick="cart.add('+product.product_id+', '+product.minimum+');" class="btn btn-primary">';
                                        html += '<i class="fa fa-shopping-cart"></i>';
                                        html += '</a>';
                                        html += '</div>';
                                    }
                                    html += '<div>';
                                    html += '<a href="' + product.url + '" title="' + product.name + '">';
                                    // show image
                                    if(product.image && show_image){
                                        html += '<div class="product-image"><img alt="' + product.name + '" src="' + product.image + '"></div>';
                                    }
                                    // show name & extra_info
                                    html += '<div class="product-name">' + product.name ;
                                    if(show_description){
                                        html += '<p>' + product.extra_info + '</p>';
                                    }
                                    html += '</div>';
                                    // show price & special price
                                    if(show_price){
                                        if (product.special) {
                                            html += '<div class="product-price"><span class="special">' + product.price + '</span><span class="price">' + product.special + '</span></div>';
                                        } else {
                                            html += '<div class="product-price"><span class="price">' + product.price + '</span></div>';
                                        }
                                    }
                                    html += '</a>';
                                    html += '</div>';

                                    html += '</li>';
                                    $('.live-search ul').append(html);
                                });
                            } else {
                                var html = '';
                                html += '<li style="text-align: center;height:10px;">';
                                html += live_search.text_no_matches;
                                html += '</li>';

                                $('.live-search ul').html(html);
                            }
                            // $('.live-search ul li').css('height',live_search.height);
                            $('.live-search').css('display','block');

                            return false;
                        }
                    });

                }
            },
            'select': function(product) {
                $(live_search.selector).val(product.name);
            }
        });

        $(document).bind( "mouseup touchend", function(e){
            var container = $('.live-search');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.hide();
            }
        });
    }

    return {
        //main function to initiate the module
        init: function(options) {
            init(options);
        }
    };

}();

$(document).ready(function() {
    var options= [];
    options['text_view_all_results']= 'View All';
    options['text_empty']="No results";
    options['module_live_search_show_image']= 1;
    options['module_live_search_show_price']= 1;
    options['module_live_search_show_description']=0;
    options['module_live_search_min_length']= 3;
    options['module_live_search_show_add_button']=0;

    LiveSearchJs.init(options);
});
/*end of live search*/
$(document).ready(function() {
    $('body').on('click', '#search-link', function(){
        $('#search').toggle();
    });
});


/*main mage on click outside shown  menu*/
$(document).mouseup(function(e)
{
    var container = $(".home .menu .parent");

    if (!container.is(e.target) && container.has(e.target).length === 0)
    {
        container.find("ul").hide();
    }
});

/*on product page on mobile put product title to top*/
$(document).ready(function() {
if ($(window).width() < 1000) {
    $(".page_product .product-wrapper div.product-name").insertBefore($(".product-wrapper .product-slider")).css({"flex-grow":0, "flex-shrink":0,"flex-basis":"100%","font-size":"21px","font-family":"GothaProBold"});
    $(".page_product .product-wrapper div.product-sku").insertBefore($(".product-wrapper .product-slider")).css({"flex-grow":0, "flex-shrink":0, "flex-basis":"100%"});
    $(".page_product .product-wrapper .product-price-main").insertBefore($(".product-wrapper .product-slider")).css({"flex-grow":0, "flex-shrink":0, "flex-basis":"100%"});
    $(".page_product .product-description").insertAfter($(".product-options-main:nth-child(2)")).css({"flex-grow":0, "flex-shrink":0, "flex-basis":"100%"});
}
    $(window).bind("resize", function () {
        if ($(this).width() < 1000) {
            $(".page_product .product-wrapper div.product-name").insertBefore($(".product-wrapper .product-slider")).css({"flex-grow":0, "flex-shrink":0,"flex-basis":"100%","font-size":"21px","font-family":"GothaProBold"});
            $(".page_product .product-wrapper div.product-sku").insertBefore($(".product-wrapper .product-slider")).css({"flex-grow":0, "flex-shrink":0, "flex-basis":"100%"});
            $(".page_product .product-wrapper .product-price-main").insertBefore($(".product-wrapper .product-slider")).css({"flex-grow":0, "flex-shrink":0, "flex-basis":"100%"});
            $(".page_product .product-description").insertAfter($(".product-options-main:nth-child(2)")).css({"flex-grow":0, "flex-shrink":0, "flex-basis":"100%"});
        } else {
            $(".product-wrapper .product-info").prepend($(".page_product .product-wrapper div.product-price-main"));
            $(".product-wrapper .product-info").prepend($(".page_product .product-wrapper div.product-sku"));
            $(".product-wrapper .product-info").prepend($(".page_product .product-wrapper div.product-name"));
            $(".page_product .product-description").insertAfter($(".inner-container > .max-width > .product-wrapper"));

        }
    }).trigger('resize');
});
/*product descriprion and delivery*/

$(document).ready(function() {
    /*$('.product-description-title').on('click', function(){
        var toggle_state = 0;
        console.log(toggle_state);
        if(toggle_state == 0) {
            $('.product-description-text').fadeIn("slow");
            $('.product-delivery-text').fadeOut("slow");
            toggle_state=1;
        } else {
            $('.product-description-text').fadeOut("slow");
            $('.product-delivery-text').fadeIn("slow");
            toggle_state=0;
        }

    });*/
    $('.product-description-title, .product-delivery-title').click(function(e) {
        e.preventDefault();

        var $this = $(this);

        if ($this.next().hasClass('show')) {
            $this.next().removeClass('show');
            $this.next().slideUp(350);
        } else {
            $('.product-description-text, .product-delivery-text').removeClass('show').slideUp(350);
            $this.next().toggleClass('show');
            $this.next().slideToggle(350);
        }
    });
});