<?php
class ModelExtensionPaymenttranzila extends Model {
	public function getMethod($address, $total) {
		$this->load->language('extension/payment/tranzila');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('payment_tranzila_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if ($this->config->get('tranzila_total') > 0 && $this->config->get('tranzila_total') > $total) {
			$status = false;
		} elseif (!$this->config->get('payment_tranzila_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();

		if ($status) {
			/*logo*/
			$this->load->model('tool/image');
			if ($this->config->get('payment_tranzila_logo') && file_exists(DIR_IMAGE . $this->config->get('payment_tranzila_logo'))) {
				$logodpg= $this->model_tool_image->resize($this->config->get('payment_tranzila_logo'),$this->config->get('payment_tranzila_width'),$this->config->get('payment_tranzila_height'));
			} else {
				$logodpg = '';
			}	
			/*logo*/
		
			$method_data = array(
				'code'       => 'tranzila',
//				'title'      => $this->config->get('payment_tranzila_title').'<img style="vertical-align:middle; margin-left:5px; "src="'.trim($logodpg).'">',
				'title'      => $this->language->get('text_title'),
				'terms'      => '',
				'sort_order' => $this->config->get('payment_tranzila_sort_order')
			);
		}

		return $method_data;
	}
}