<?php
// Text
$_['text_search']                             = 'חיפוש';
$_['text_brand']                              = 'מותג';
$_['text_manufacturer']                       = 'מותג:';
$_['text_model']                              = 'קוד מוצר:';
$_['text_reward']                             = 'נקודות מצטברות:';
$_['text_points']                             = 'מחיר נקודות מצטברות:';
$_['text_stock']                              = 'זמינות:';
$_['text_instock']                            = 'במלאי';
$_['text_tax']                                = 'ללא מס:';
$_['text_discount']                           = ' או יותר ';
$_['text_option']                             = 'אפשרויות זמינות';
$_['text_minimum']                            = 'למוצר זה כמות מינימלית של %s';
$_['text_reviews']                            = '%s ביקורות';
$_['text_write']                              = 'כתיבת ביקורת';
$_['text_login']                              = 'יש <a href="%s">להתחבר</a> או <a href="%s">להירשם</a> כדי לכתוב ביקורת';
$_['text_no_reviews']                         = 'אין ביקורות למוצר זה.';
$_['text_note']                               = '<span class="text-danger"> הערה:</span> HTML לא מתורגם!';
$_['text_success']                            = 'תודה על הביקורת שלך, היא הוגשה למערכת לאישור.';
$_['text_related']                            = 'מוצרים קשורים';
$_['text_tags']                               = 'תגיות:';
$_['text_error']                              = 'מוצר לא נמצא!';
$_['text_payment_recurring']                    = 'פרופילי תשלום';
$_['text_trial_description']                  = '%s כל %s(s) %d על %d payment(s) אז';
$_['text_payment_description']                = '%s כל %s(s) %d על %d payment(s)';
$_['text_payment_until_canceled_description'] = 'בוטלה כל %s(s) %d עד %s';
$_['text_day']                                = 'יום';
$_['text_week']                               = 'שבוע';
$_['text_semi_month']                         = 'חצי חודש';
$_['text_month']                              = 'חודש';
$_['text_year']                               = 'שנה';

// Entry
$_['entry_qty']                               = 'כמות';
$_['entry_name']                              = 'השם שלך';
$_['entry_review']                            = 'הביקורת שלך';
$_['entry_rating']                            = 'דירוג';
$_['entry_good']                              = 'טוב';
$_['entry_bad']                               = 'רע';
$_['entry_captcha']                           = 'הזן את הקוד בתיבה שלהלן';

// Tabs
$_['tab_description']                         = 'תיאור';
$_['tab_attribute']                           = 'מפרט';
$_['tab_review']                              = 'ביקורות (%s)';

// Error
$_['error_name']                              = 'אזהרה: סקירה שם חייב להיות בין 3 ל 25 תווים!';
$_['error_text']                              = 'אזהרה: סקירה טקסט חייב להיות בין 25 ל- 1000 תווים!';
$_['error_rating']                            = 'אזהרה: נא בחר דירוג סקירה!';
$_['error_captcha']                           = 'אזהרה: קוד אימות לא תואמת את התמונה!';

//oshka
$_['text_details']                = 'פרטים';
$_['text_share']                  = 'שתף';
$_['text_sku']                = 'מק"ט';
$_['text_thank_you']            = 'תודה רבה';

$_['text_delivery_title']        = 'משלוח ותשלום';
$_['text_delivery_text']         = '  <strong>שיטות משלוח </strong>: <br> דואר רשום - 20 ש"ח <br> בקנייה מ-100 ש"ח ועד 199 ש"ח - 10 ש"ח<br> בקנייה מ-200 ש"ח ועד 249 ש"ח - 5 ש"ח<br> בקנייה מעל 250 ש"ח - חינם<br> <br> שליח עד הבית - 50 ש"ח<br> בקנייה מ-100 ש"ח ועד 199 ש"ח - 40 ש"ח<br> בקנייה מ-200 ש"ח ועד 249 ש"ח- 25 ש"ח<br> בקנייה מעל 250 ש"ח - 10 ש"ח<br> <br> איסוף עצמי מהחנויות - חינם <br> <br> <a href="https://yumsterkids.com/delivery" target="_blank">למידע נוסף</a><br> <br> החלפות והחזרות: <br> ניתן להחליף בחנות : אוסישקין 35 רמת השרון <br> <br> <a href="https://yumsterkids.com/delivery" target="_blank">למידע נוסף </a> ';

