<?php
// Text
$_['text_home']          = 'דף הבית';
$_['text_wishlist']      = 'רשימת המבוקשים (%s)';
$_['text_shopping_cart'] = 'עגלת קניות';
$_['text_category']      = 'קטגוריות';
$_['text_account']       = 'החשבון שלי';
$_['text_register']      = 'הירשם';
$_['text_login']         = 'התחבר';
$_['text_order']         = 'היסטוריית ההזמנות';
$_['text_transaction']   = 'תשלומים';
$_['text_download']      = 'הורדות';
$_['text_logout']        = 'התנתק';
$_['text_checkout']      = 'המשך לקופה';
$_['text_search']        = 'חיפוש';
$_['text_all']           = 'הצג הכל';

//oshka
$_['more']               = 'ראה עוד';
$_['about_us']           = 'אודות';
$_['contact_us']         = 'צור קשר';
$_['delivery_returns']   = 'משלוחים והחזרות';
$_['faq']                = 'שאלות נפוצות';
$_['terms_conditions']   = 'תנאי שימוש באתר';
//oshka
$_['checkout_header_title']   = 'לרכישה';
$_['secured']   = 'מאובטח';
$_['search_text']   = ' חפוש ';