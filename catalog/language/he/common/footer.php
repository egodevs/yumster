<?php
// Text
$_['text_information']  = 'מידע נוסף';
$_['text_service']      = 'שירות לקוחות';
$_['text_extra']        = 'תוספות';
$_['text_contact']      = 'צור קשר';
$_['text_return']       = 'החזרות';
$_['text_sitemap']      = 'מפת האתר';
$_['text_manufacturer'] = 'מותגים';
$_['text_voucher']      = 'שוברי מתנה';
$_['text_affiliate']    = 'שותפים';
$_['text_special']      = 'מבצעים';
$_['text_account']      = 'החשבון שלי';
$_['text_order']        = 'היסטוריית ההזמנות';
$_['text_wishlist']     = 'רשימת המבוקשים';
$_['text_newsletter']   = 'רשימת תפוצה';
$_['text_powered']      = 'מופעל על ידי <a href="http://www.opencart.com"> אופן קארט</a><br />%s&copy;%s';

//oshka
$_['menu']               = 'תפריט';
$_['about_us']           = 'אודות';
$_['contact_us']         = 'צור קשר';
$_['delivery_returns']   = 'משלוחים והחזרות';
$_['faq']                = 'שאלות נפוצות';
$_['terms_conditions']   = 'תנאי שימוש באתר';
$_['join_community']     = 'הצטרפו לקהילה';
$_['download_catalog']   = 'הורד את הקטלוג';
$_['your_email']         = 'כתובת המייל שלך';