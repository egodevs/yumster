<?php
// Heading
$_['heading_title'] = 'השתמש בשובר מתנה';

// Text
$_['text_voucher']  = 'שובר מתנה (%s)';
$_['text_success']  = 'עבר בהצלחה: שובר מתנה שלך מומש בהצלחה';

// Entry
$_['entry_voucher'] = 'הזן את קוד שובר המתנה שלך כאן';

// Error
$_['error_voucher'] = 'הזהרה: שובר מתנה לא חוקי או את היתרה כבר מומשה';
$_['error_empty']   = 'אזהרה: נא להזין קוד אישור מתנה';