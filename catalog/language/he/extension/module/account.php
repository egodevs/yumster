<?php
// Heading
$_['heading_title']    = 'אזור אישי';

// Text
$_['text_register']    = 'הרשמה';
$_['text_login']       = 'התחבר';
$_['text_logout']      = 'התנתק';
$_['text_forgotten']   = 'שכחת סיסמה?';
$_['text_account']     = 'איזור אישי';
$_['text_edit']        = 'ערוך פרופיל';
$_['text_password']    = 'סיסמה';
$_['text_address']     = 'כתובת משלוח';
$_['text_wishlist']    = 'רשימת משאלות';
$_['text_order']       = 'ההזמנות שלי';
$_['text_download']    = 'Downloads';
$_['text_reward']      = 'Reward Points';
$_['text_return']      = 'Returns';
$_['text_transaction'] = 'Transactions';
$_['text_newsletter']  = 'ניוזלטר';
$_['text_recurring']   = 'Recurring payments';