<?php
// Text
$_['text_title']				= 'Carte de crédit / Carte de débit (Web-logiciel de paiement)';
$_['text_credit_card']			= 'Détails de la carte de crédit';

// Entry
$_['entry_cc_owner']			= 'Titulaire de la carte';
$_['entry_cc_number']			= 'Numéro de la carte';
$_['entry_cc_expire_date']		= 'Date d\'expiration';
$_['entry_cc_cvv2']				= 'Code de sécurité (CVV2)';