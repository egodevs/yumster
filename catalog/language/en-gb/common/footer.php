<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contact Us';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Certificates';
$_['text_affiliate']    = 'Affiliate';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_powered']      = 'Powered By <a href="http://www.opencart.com">OpenCart</a><br /> %s &copy; %s';
//oshka
$_['menu']               = 'Menu';
$_['about_us']           = 'About Us';
$_['contact_us']         = 'Contact Us';
$_['delivery_returns']   = 'Delivery & Returns';
$_['faq']                = 'FAQ';
$_['terms_conditions']   = 'Terms & Conditions';
$_['join_community']     = 'join our <span>Community</span>';
$_['download_catalog']    = 'download our <span>CATALOG';
$_['your_email']         = 'Your email';
$_['catalog_request_subject']     = 'Catalog request';

