<?php
// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = 'Wish List (%s)';
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category']      = 'Categories';
$_['text_account']       = 'My Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'Order History';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'Show All';
//oshka
$_['more']               = 'More';
$_['about_us']           = 'About Us';
$_['contact_us']         = 'Contact Us';
$_['delivery_returns']   = 'Delivery & Returns';
$_['faq']                = 'FAQ';
$_['terms_conditions']   = 'Terms & Conditions';
//oshka
$_['checkout_header_title']   = 'Check out';
$_['secured']   = 'secured';
$_['search_text']   = 'Search';