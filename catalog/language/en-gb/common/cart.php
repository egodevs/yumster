<?php
// Text
$_['text_items']     = '%s item(s) - %s';
$_['text_empty']     = 'Your shopping cart is empty!';
$_['text_cart']      = 'View Cart';
$_['text_checkout']  = 'Checkout';
$_['text_recurring'] = 'Payment Profile';
$_['you_added_to_cart'] = 'You added to cart';
// oshka
$_['product_saving_total_accumulative_title'] = 'Saving By Sale';
