<?php
// Heading
$_['heading_title'] = 'Failed Payment!';

// Text
$_['text_basket']   = 'Shopping Cart';
$_['text_checkout'] = 'Checkout';
$_['text_failure']  = 'Failed Payment';
$_['text_message']  = '<p>There was a problem processing your payment and the order did not complete.</p>

<p>Possible reasons are:</p>
<ul>
  <li>Insufficient funds</li>
  <li>Verification failed</li>
</ul>

<p>Please try to order again using a different payment method.</p>

<p>If the problem persists please <a href="%s">contact us</a> with the details of the order you are trying to place.</p>
';

$_['mail_subject'] ="Oops! Payment declined.";
$_['mail_title'] ="Oops! Payment declined.";
$_['mail_dear'] = "Dear";
$_['mail_text'] ="<p>Unfortunately, the payment was declined by the credit card company. For more information, please contact us at 1-700-552-3-52 or email <a href=\"mailto:support@yumsterkids.com\">support@yumsterkids.com</a>.</p><p>You can resolve the matter with the credit card company or use a different card to complete your order at our website <a href=\"https://yumsterkids.com/\" target=\"_blank\" rel=\"noreferrer nofollow\">https://yumsterkids.com</a>.</p>";