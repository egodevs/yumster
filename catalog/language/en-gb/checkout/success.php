<?php
// Heading
$_['heading_title']        = 'Thank you!';

// Text
$_['text_basket']          = 'Shopping Cart';
$_['text_checkout']        = 'Checkout';
$_['text_success']         = 'Success';
$_['text_customer']        = '<p style="text-align:center">Your order has been successfully processed!<br><br>Thanks for shopping with us online!<br><br>You can view your order history </p>  <a href="%s" class="btn">review order</a>';
$_['text_guest']           = '<p>Your order has been successfully processed!</p><p>Please direct any questions you have to the <a href="%s">store owner</a>.</p><p>Thanks for shopping with us online!</p>';