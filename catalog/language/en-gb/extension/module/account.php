<?php
// Heading
$_['heading_title']    = 'Account';

// Text
$_['text_register']    = 'Create an account';
$_['text_login']       = 'Login';
$_['text_logout']      = 'Sign out';
$_['text_forgotten']   = 'Forgot your password?';
$_['text_account']     = 'My Account';
$_['text_edit']        = 'Edit Account';
$_['text_password']    = 'Password';
$_['text_address']     = 'Delivery address';
$_['text_wishlist']    = 'Wish List';
$_['text_order']       = 'My orders';
$_['text_download']    = 'Downloads';
$_['text_reward']      = 'Reward Points';
$_['text_return']      = 'Returns';
$_['text_transaction'] = 'Transactions';
$_['text_newsletter']  = 'Newsletter';
$_['text_recurring']   = 'Recurring payments';