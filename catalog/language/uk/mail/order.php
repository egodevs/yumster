<?php
// Text
$_['text_new_subject']          = '%s - Замовлення %s';
$_['text_new_greeting']         = 'Дякуємо вам за інтерес, проявлений до %s продукції. Ваше замовлення було отримано та оброблено після підтвердження платежу.';
$_['text_new_received']         = 'Ви отримали замовлення.';
$_['text_new_link']             = 'Для перегляду Вашого замовлення, натисніть на посилання нижче:';
$_['text_new_order_detail']     = 'Деталі замовлення';
$_['text_new_instruction']      = 'Інструкції';
$_['text_new_order_id']         = 'Ідентифікатор замовлення:';
$_['text_new_date_added']       = 'Дата додавання:';
$_['text_new_order_status']     = 'Стан замовлення:';
$_['text_new_payment_method']   = 'Метод оплати:';
$_['text_new_shipping_method']  = 'Спосіб доставки:';
$_['text_new_email']  			= 'Електронна пошта:';
$_['text_new_telephone']  		= 'Телефон:';
$_['text_new_ip']  				= 'ІР-адреса:';
$_['text_new_payment_address']  = 'Адреса платника';
$_['text_new_shipping_address'] = 'Адреса доставки';
$_['text_new_products']         = 'Товари';
$_['text_new_product']          = 'Товар';
$_['text_new_model']            = 'Модель';
$_['text_new_quantity']         = 'Кількість';
$_['text_new_price']            = 'Ціна';
$_['text_new_order_total']      = 'Всього по замовленню';
$_['text_new_total']            = 'Загалом';
$_['text_new_download']         = 'Як тільки ваш платіж буде підтверджено ви можете натиснути на посилання нижче, щоб отримати доступ до ваших продуктів із завантаженнями:';
$_['text_new_comment']          = 'Коментарі до вашого замовлення:';
$_['text_new_footer']           = 'Буд ласка надішліть відповідь на цей лист, якщо у вас є які-небудь питання.';
$_['text_update_subject']       = '%s - оновленя замовлення %s';
$_['text_update_order']         = 'Ідентифікатор замовлення:';
$_['text_update_date_added']    = 'Дата замовлення:';
$_['text_update_order_status']  = 'Ваше замовлення було оновлено до наступного стану:';
$_['text_update_comment']       = 'Коментарі до вашого замовлення:';
$_['text_update_link']          = 'Для перегляду Вашого замовлення, натисніть на посилання нижче:';
$_['text_update_footer']        = 'Буд ласка надішліть відповідь на цей лист, якщо у вас є які-небудь питання.';
