<?php
// Heading
$_['heading_title']     = 'Téléchargements';

// Text
$_['text_success']      = 'Vous avez modifié les téléchargements avec succès !';
$_['text_list']         = 'Liste de téléchargement';
$_['text_add']          = 'Ajouter un téléchargement';
$_['text_edit']         = 'Editer le  téléchargement';
$_['text_upload']       = 'Votre fichier a été téléchargé avec succès !';

// Column
$_['column_name']       = 'Nom du téléchargement';
$_['column_date_added'] = 'Date d\'ajout';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']        = 'Nom du téléchargement';
$_['entry_filename']    = 'Nom du fichier';
$_['entry_mask']        = 'Masque';

// Help
$_['help_filename']     = 'Vous pouvez télécharger via le bouton de transfert ou utiliser un FTP pour télécharger dans le répertoire de téléchargement et entrer dans les détails ci-dessous.';
$_['help_mask']         = 'Il est recommandé que les noms de fichier et de masque soient différents pour empêcher les personnes de se connecter directement à vos téléchargements.';

// Error
$_['error_permission']  = 'AVERTISSEMENT : Vous n\'êtes pas autorisé à modifier les téléchargements !';
$_['error_name']        = 'Le nom du téléchargement doit comporter entre 3 et 64 caractères !';
$_['error_upload']      = 'Téléchargement requis !';
$_['error_filename']    = 'Le nom de fichier doit contenir entre 3 et 128 caractères !';
$_['error_exists']      = 'Ce fichier n\'existe pas!';
$_['error_mask']        = 'Le nom du masque doit contenir entre 3 et 128 caractères !';
$_['error_filetype']    = 'Type de fichier invalide !';
$_['error_product']     = 'ATTENTION : Ce téléchargement ne peut être supprimé car il est actuellement affecté aux produits %s !';