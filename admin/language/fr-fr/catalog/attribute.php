<?php
// Heading
$_['heading_title']          = 'Attributs';

// Text
$_['text_success']           = 'Vous avez modifié les attributs avec succès !';
$_['text_list']              = 'Liste des Attributs';
$_['text_add']               = 'Ajouter un attribut';
$_['text_edit']              = 'Modifier un attribut';

// Column
$_['column_name']            = 'Nom de l\'attribut';
$_['column_attribute_group'] = 'Groupe d\'attributs';
$_['column_sort_order']      = 'Classement';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Nom de l\'attribut';
$_['entry_attribute_group']  = 'Groupe d\'attributs';
$_['entry_sort_order']       = 'Tri des commandes';

// Error
$_['error_permission']       = 'Avertissement : Vous n\'êtes pas autorisé à modifier les Attributs!';
$_['error_attribute_group']  = 'Groupe d’attributs requis !';
$_['error_name']             = 'Le nom de l\'attribut doit contenir entre 3 et 64 caractères !';
$_['error_product']          = 'Avertissement : Cet Attribut ne peut être supprimé car il est actuellement affecté à  %s produits!';
