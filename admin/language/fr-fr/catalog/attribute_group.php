<?php
// Heading
$_['heading_title']     = 'Groupes d\'attributs';

// Text
$_['text_success']      = 'Vous venez de modifier les groupes d\'attributs avec succès !';
$_['text_list']         = 'Liste des groupes d\'attributs';
$_['text_add']          = 'Ajouter un groupe d\'attributs';
$_['text_edit']         = 'Editer un groupe d\'attributs';

// Column
$_['column_name']       = 'Nom du groupe d\'attributs';
$_['column_sort_order'] = 'Classement';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']        = 'Nom du groupe d\'attributs';
$_['entry_sort_order']  = 'Classement';

// Error
$_['error_permission']  = 'Attention: Vous n\'êtes pas autorisé à modifier les groupes d\'attributs !';
$_['error_name']        = 'Le nom du groupe d\'attributs doit contenir entre 3 et 64 caractères !';
$_['error_attribute']   = 'Avertissement : Ce groupe d\'attributs ne peut être supprimé car il est actuellement affecté à  %s  attributs!';
$_['error_product']     = 'Avertissement : Ce groupe d\'attributs ne peut être supprimé car il est actuellement affecté à  %s  produits!';