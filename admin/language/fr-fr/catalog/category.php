<?php
// Heading
$_['heading_title']          = 'Catégories';

// Text
$_['text_success']           = 'Vous avez modifié les catégories avec succès !';
$_['text_list']              = 'Liste des catégories';
$_['text_add']               = 'Ajouter une catégorie';
$_['text_edit']              = 'Modifier la catégorie';
$_['text_default']           = 'Par défaut';

// Column
$_['column_name']            = 'Nom de la catégorie';
$_['column_sort_order']      = 'Classement';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Nom de la catégorie';
$_['entry_description']      = 'Description ';
$_['entry_meta_title'] 	     = 'Titre de la Balise META';
$_['entry_meta_keyword'] 	 = 'Mots clés de la Balises Meta';
$_['entry_meta_description'] = 'Description de la Balises Meta';
$_['entry_keyword']          = 'Mots clés SEO';
$_['entry_parent']           = 'Parent';
$_['entry_filter']           = 'Filtres ';
$_['entry_store']            = 'Magasins';
$_['entry_image']            = 'Image';
$_['entry_top']              = 'Haut';
$_['entry_column']           = 'Colonnes';
$_['entry_sort_order']       = 'Classement';
$_['entry_status']           = 'Etat';
$_['entry_layout']           = 'Remplacement de la mise en page';

// Help
$_['help_filter']            = '(Saisie semi-automatique)';
$_['help_keyword']           = 'Ne pas utiliser d\'espaces, plutôt remplacer des espaces avec - et assurez-vous que le mot clé est unique au monde.';
$_['help_top']               = 'Afficher dans la barre de menu. Ne fonctionne qu\'avec les catégories parents du haut de la page.';
$_['help_column']            = 'Nombre de colonnes à utiliser pour les 3 catégories du bas. Ne fonctionne que pour les catégories parents du haut de la page.';

// Error
$_['error_warning']          = 'AVERTISSEMENT : Veuillez consulter le formulaire avec soin pour trouver des erreurs !';
$_['error_permission']       = 'AVERTISSEMENT : Vous n\'êtes pas autorisé à modifier les catégories !';
$_['error_name']             = 'Le nom de la catégorie doit être entre 2 et 32 caractères !';
$_['error_meta_title']       = 'Le titre Meta doit être supérieur à 3 caractères et à moins de 255 caractères !';
$_['error_keyword']          = 'Le mot-clé SEO déjà en cours d\'utilisation !';