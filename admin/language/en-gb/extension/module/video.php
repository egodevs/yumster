<?php
// Heading
$_['heading_title']     = 'Full-page video';

// Text
$_['text_extension']    = 'Extensions';
$_['text_success']      = 'Success: You have modified Video module!';
$_['text_edit']         = 'Edit Video Module';

// Entry
$_['entry_name']        = 'Module Name';
$_['entry_title']       = 'Heading Title';
$_['entry_description'] = 'Description';
$_['entry_status']      = 'Status';


// Error
$_['error_permission']  = 'Warning: You do not have permission to modify Video Content module!';
$_['error_name']        = 'Module Name must be between 3 and 64 characters!';

/**oshka*/
$_['text_video']= 'Video for full-background';
$_['entry_video_text1']= 'First big text line';
$_['entry_video_text2']= 'Second small text line';
$_['entry_video_link']= 'Link from below the text-lines';
$_['entry_video_poster']= 'Video poster';
$_['entry_video_file']= 'Upload video file (mp4)';
