<?php
// Heading
$_['heading_title']			  = 'Moneybookers';

// Text
$_['text_payment']			  = 'Платіж';
$_['text_success']			  = 'Успішно: Ви змінили налаштування Moneybookers.';
$_['text_edit']               = 'Редагувати Moneybookers';
$_['text_moneybookers']		  = '<a href="https://www.moneybookers.com/partners/?p=OpenCart" target="_blank"><img src="view/image/payment/moneybookers.png" alt="Moneybookers" title="Moneybookers" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_email']			  = 'Електронна пошта';
$_['entry_secret']		      = 'Таємниця';
$_['entry_total']			  = 'Всього';
$_['entry_order_status']	  = 'Статус замовлення';
$_['entry_pending_status']	  = 'В очікуванні';
$_['entry_canceled_status']	  = 'Скасовано';
$_['entry_failed_status']	  = 'Невдало';
$_['entry_chargeback_status'] = 'Статус: повернення платежу';
$_['entry_geo_zone']		  = 'Регіон';
$_['entry_status']			  = 'Статус';
$_['entry_sort_order']		  = 'Порядок сортування';

// Help
$_['help_total']			  = 'Цей метод стане доступним, коли загальна сума досягне суми мінімального замовлення.';

// Error
$_['error_permission']		  = 'Увага: Ви не маєте достатньо прав на зміну Moneybookers!';
$_['error_email']			  = 'E-Mail обов\'язково!';