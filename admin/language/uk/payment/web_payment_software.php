<?php
// Heading
$_['heading_title']					= 'Web Payment Software';

// Text
$_['text_payment']					= 'Платіж';
$_['text_success']					= 'Успіх: Змінено реквізити рахунку Web Payment Software account details!';
$_['text_edit']                     = 'Редагувати AWeb Payment Software
';
$_['text_web_payment_software']		= '<a href="http://www.web-payment-software.com/" target="_blank"><img src="view/image/payment/wps-logo.jpg" alt="Web Payment Software" title="Web Payment Software" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_test']						= 'Тест';
$_['text_live']						= 'Діючий';
$_['text_authorization']			= 'Авторизація';
$_['text_capture']					= 'Захоплення';

// Entry
$_['entry_login']					= 'ID Продавця';
$_['entry_key']						= 'Ключ продавця';
$_['entry_mode']					= 'Режимі транзакції';
$_['entry_method']					= 'Метод транзакції';
$_['entry_total']					= 'Всього';
$_['entry_order_status']			= 'Статус замовлення';
$_['entry_geo_zone']				= 'Регіон';
$_['entry_status']					= 'Статус';
$_['entry_sort_order']				= 'Порядок сортування';

// Help
$_['help_total']					= 'Цей метод стане доступним, коли загальна сума досягне суми мінімального замовлення.';

// Error
$_['error_permission']				= 'Попередження: Не має дозволу змінювати оплати Web Payment Software!';
$_['error_login']					= 'Login ID обов\'язково!';
$_['error_key']						= 'Необхідний ключ транзакції!';