<?php
// Heading
$_['heading_title']					= 'Paymate';

// Text
$_['text_payment']					= 'Платіж';
$_['text_success']					= 'Успіх: Змінено реквізити рахунку Paymate!';
$_['text_edit']                     = 'Змінити Paymate';
$_['text_paymate']					= '<img src="view/image/payment/paymate.png" alt="Paymate" title="Paymate" style="border: 1px solid #EEEEEE;" />';

// Entry
$_['entry_username']				= 'Paymate ім\'я користувача';
$_['entry_password']				= 'Пароль';
$_['entry_test']					= 'Тестовий режим';
$_['entry_total']					= 'Разом';
$_['entry_order_status']			= 'Статус замовлення';
$_['entry_geo_zone']				= 'Регіон';
$_['entry_status']					= 'Статус';
$_['entry_sort_order']				= 'Порядок сортування';

// Help
$_['help_password']					= 'Просто використовуйте якийсь випадковий пароль. Це необхідно для того, щоб переконайтеся, що відомості про оплату не змінювались після відправки на платіжний шлюз.';
$_['help_total']					= 'Цей метод стане доступним, коли загальна сума досягне суми мінімального замовлення.';

// Error
$_['error_permission']				= 'Увага: у Вас немає дозволу на зміну параметрів платежу WorldPay!';
$_['error_username']				= 'Paymate потрібно ім\'я користувача!';
$_['error_password']				= 'Потрібен пароль!';