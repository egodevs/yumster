<?php
// Heading
$_['heading_title']					 = 'Оформити замовлення PayPal Експрес';

// Text
$_['text_payment']				  	 = 'Платіж';
$_['text_success']				 	 = 'Успішно: Змінено реквізити рахунку PayPal Express Checkout!';
$_['text_edit']                      = 'Редагувати PayPal Експрес оформлення замовлень';
$_['text_pp_express']				 = '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			 = 'Авторизація';
$_['text_sale']						 = 'Акція';
$_['text_clear']					 = 'Очистити';
$_['text_browse']					 = 'Огляд';
$_['text_image_manager']			 = 'Менеджер зображень';
$_['text_ipn']						 = 'IPN URL';

// Entry
$_['entry_username']				 = 'API ім\'я користувача';
$_['entry_password']				 = 'API пароль';
$_['entry_signature']				 = 'API підпис';
$_['entry_test']					 = 'Режим тестування (пісочниці)';
$_['entry_method']					 = 'Метод транзакції';
$_['entry_geo_zone']				 = 'Регіон';
$_['entry_status']					 = 'Статус';
$_['entry_sort_order']				 = 'Порядок сортування';
$_['entry_icon_sort_order']			 = 'Значок порядоку сортування';
$_['entry_debug']					 = 'Ведення журналу налагодження';
$_['entry_total']					 = 'Разом';
$_['entry_currency']				 = 'Валюта за замовчуванням';
$_['entry_recurring_cancellation']	 = 'Дозволити клієнтам скасовувати регулярні платежі';
$_['entry_canceled_reversal_status'] = 'Статус: Повернення відмінене';
$_['entry_completed_status']		 = 'Статус Завершено';
$_['entry_denied_status']			 = 'Статус Заборонено';
$_['entry_expired_status']			 = 'Статус Протермінований';
$_['entry_failed_status']			 = 'Статус Невдало';
$_['entry_pending_status']			 = 'Статус в очікуванні';
$_['entry_processed_status']		 = 'Статус оброблені';
$_['entry_refunded_status']			 = 'Статус повернення';
$_['entry_reversed_status']			 = 'Статус повернення';
$_['entry_voided_status']			 = 'Статус Анульований';
$_['entry_display_checkout']		 = 'Показати значок швидкого оформлення замовлення';
$_['entry_allow_notes']				 = 'Дозволити коментарі';
$_['entry_logo']					 = 'Логотип';
$_['entry_border_colour']			 = 'Колір межі заголовка';
$_['entry_header_colour']			 = 'Колір фону заголовка';
$_['entry_page_colour']				 = 'Колір фону сторінки';

// Tab
$_['tab_general']					 = 'Загальні';
$_['tab_api']				         = 'Деталі API';
$_['tab_order_status']				 = 'Статус замовлення';
$_['tab_checkout']					 = 'Оформити замовлення';

// Help
$_['help_ipn']						 = 'Необхідні для підписки';
$_['help_total']					 = 'Цей метод стане доступним, коли загальна сума досягне суми мінімального замовлення';
$_['help_logo']						 = 'Макс 750px(w) x 90px(h)<br />Ви повинні використовувати тільки логотип, якщо у вас налаштований SSL.';
$_['help_colour']					 = '6 символів HTML код кольору';
$_['help_currency']					 = 'Використовується для пошуку транзакцій';

// Error
$_['error_permission']				 = 'Увага: У вас немає дозволу на зміну налаштувань методу оплати PayPal Express Checkout!';
$_['error_username']				 = 'API ім\'я користувача потрібно!';
$_['error_password']				 = 'Потрібно пароль API!';
$_['error_signature']				 = 'API підпис обов\'язково!';
$_['error_data']					 = 'Дані згідно запиту відсутні';
$_['error_timeout']					 = 'Вичерпано час запиту';