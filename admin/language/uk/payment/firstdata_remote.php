<?php
// Heading
$_['heading_title']					= 'First Data EMEA Web Service API';

// Text
$_['text_firstdata_remote']			= '<img src="view/image/payment/firstdata.png" alt="First Data" title="First Data" style="border: 1px solid #EEEEEE;" />';
$_['text_payment']					= 'Платежі';
$_['text_success']					= 'Успіх: Змінено реквізити рахунку First Data!';
$_['text_edit']                     = 'Редагувати First Data EMEA Web Service API';
$_['text_card_type']				= 'Тип карти';
$_['text_enabled']					= 'Ввімкнено';
$_['text_merchant_id']				= 'ID Магазину';
$_['text_subaccount']				= 'Субрахунок';
$_['text_user_id']					= 'ID користувача';
$_['text_capture_ok']				= 'Прийом пройшов успішно';
$_['text_capture_ok_order']			= 'Прийом був успішним, статус замовлення оновлений до успішного - врегульований ';
$_['text_refund_ok']				= 'Повернення коштів пройшло успішно';
$_['text_refund_ok_order']			= 'Повернення коштів було успішним, статус замовлення оновлений до повертається';
$_['text_void_ok']					= 'Анулювання було успішним, статус замовлення оновлений до анульоване';
$_['text_settle_auto']				= 'Продаж';
$_['text_settle_delayed']			= 'Попередньо затверджене';
$_['text_mastercard']				= 'MasterCard';
$_['text_visa']						= 'Visa';
$_['text_diners']					= 'Diners';
$_['text_amex']						= 'American Express';
$_['text_maestro']					= 'Maestro';
$_['text_payment_info']				= 'Інформація про оплату';
$_['text_capture_status']			= 'Оплату прийняли';
$_['text_void_status']				= 'Оплата анульована';
$_['text_refund_status']			= 'Оплата повертається';
$_['text_order_ref']				= 'Посилання на замовлення';
$_['text_order_total']				= 'Загалом, затверджене';
$_['text_total_captured']			= 'Загалом прийняли';
$_['text_transactions']				= 'Сплаченні рахунки';
$_['text_column_amount']			= 'Сума';
$_['text_column_type']				= 'Тип';
$_['text_column_date_added']		= 'СТВОРЕНО';
$_['text_confirm_void']				= 'Ви впевнені, що ви хочете, щоб анулювати платіж?';
$_['text_confirm_capture']			= 'Ви впевнені, що ви хочете, щоб прийняти платіж?';
$_['text_confirm_refund']			= 'Ви дійсно бажаєте повернути гроші на оплату?';

// Entry
$_['entry_certificate_path']		= 'Сертифікат шлях';
$_['entry_certificate_key_path']	= 'Закритий ключ шлях';
$_['entry_certificate_key_pw']		= 'Закритий ключ пароль';
$_['entry_certificate_ca_path']		= 'CA шлях';
$_['entry_merchant_id']				= 'ID Магазину';
$_['entry_user_id']					= 'ID користувача';
$_['entry_password']				= 'Пароль';
$_['entry_total']					= 'Всього';
$_['entry_sort_order']				= 'Порядок сортування';
$_['entry_geo_zone']				= 'Регіон';
$_['entry_status']					= 'Стан';
$_['entry_debug']					= 'Ведення журналу налагодження';
$_['entry_auto_settle']				= 'Тип врегулювання';
$_['entry_status_success_settled']	= 'Успіх - врегульований';
$_['entry_status_success_unsettled'] = 'Успіх - не врегульований';
$_['entry_status_decline']			 = 'Відхилено';
$_['entry_status_void']				 = 'Анульовані';
$_['entry_status_refund']			 = 'Повернуті';
$_['entry_enable_card_store']		 = 'Увімкнути карти зберігання купонів';
$_['entry_cards_accepted']			 = 'Типи карток, що приймаються';

// Help
$_['help_total']					 = 'Цей метод стане доступним, коли загальна сума досягне суми мінімального замовлення';
$_['help_certificate']				 = 'Сертифікати й закриті ключі слід зберігати за межами ваших публічних веб-папок';
$_['help_card_select']				 = 'Попросіть користувача вибирати його тип карти, перш ніж їх буде переспрямовано';
$_['help_notification']				 = 'Потрібно вказати цю URL-адресу з First Data, щоб отримати платіж сповіщення';
$_['help_debug']					 = 'Дозволяючи налагодження буде писати конфіденційні дані до файлу журналу. Вам завжди слід вимикати, якщо вказівка інакша.';
$_['help_settle']					 = 'Якщо ви використовуєте попереднього авторизацію,то ви повинні її завершити протягом 3-5 днів, в іншому випадку буде видалено транзакцію';

// Tab
$_['tab_account']					 = 'API інформація';
$_['tab_order_status']				 = 'Статус замовлення';
$_['tab_payment']					 = 'Налаштування оплати';

// Button
$_['button_capture']				= 'Прийом';
$_['button_refund']					= 'Повернення коштів';
$_['button_void']					= 'Анулювання';

// Error
$_['error_merchant_id']				= 'Потрібний ID Магазину';
$_['error_user_id']					= 'Необхідний  ID користувача';
$_['error_password']				= 'Потрібен пароль';
$_['error_certificate']				= 'Потрібний шлях до сертифікату';
$_['error_key']						= 'Потрібний ключ до сертифікату';
$_['error_key_pw']					= 'Потрібно сертифікат ключа паролю ';
$_['error_ca']						= 'Вимагається сертифікат авторизації (CA) ';