<?php
// Heading
$_['heading_title']					 = 'Realex перенаправлення';

// Text
$_['text_payment']				  	 = 'Платіж';
$_['text_success']					 = 'Успіх: Ви змінили деталі облікового запису Realex!';
$_['text_edit']                      = 'Редагувати Realex перенаправлення';
$_['text_live']						 = 'Наживо';
$_['text_demo']						 = 'Демо';
$_['text_card_type']				 = 'Тип карти';
$_['text_enabled']					 = 'Увімкнено';
$_['text_use_default']				 = 'Використовувати за замовчуванням';
$_['text_merchant_id']				 = 'ID мерчанта';
$_['text_subaccount']				 = 'Субрахунок';
$_['text_secret']					 = 'Спільний секрет';
$_['text_card_visa']				 = 'Visa';
$_['text_card_master']				 = 'MasterCard';
$_['text_card_amex']				 = 'American Express';
$_['text_card_switch']				 = 'Switch/Maestro';
$_['text_card_laser']				 = 'Laser';
$_['text_card_diners']				 = 'Diners';
$_['text_capture_ok']				 = 'Прийом пройшов успішно';
$_['text_capture_ok_order']			 = 'Прийом був успішним, статус замовлення оновлений до успішного - врегульований';
$_['text_rebate_ok']				 = 'Знижка пройшла успішно';
$_['text_rebate_ok_order']			 = 'Знижка пройшла успішно, статус замовлення для знижки оновлюється';
$_['text_void_ok']					 = 'Void was successful, order status updated to voided';
$_['text_settle_auto']				 = 'Автоматично';
$_['text_settle_delayed']			 = 'Відкладено';
$_['text_settle_multi']				 = 'Мульті';
$_['text_url_message']				 = 'You must supply the store URL to your Realex account manager before going live';
$_['text_payment_info']				 = 'Інформація про оплату';
$_['text_capture_status']			 = 'Оплату прийняли';
$_['text_void_status']				 = 'Платіж анульований';
$_['text_rebate_status']			 = 'Payment rebated';
$_['text_order_ref']				 = 'Посилання на замовлення';
$_['text_order_total']				 = 'Всього авторизовано';
$_['text_total_captured']			 = 'Загалом прийняли';
$_['text_transactions']				 = 'Транзакції';
$_['text_column_amount']			 = 'Кількість';
$_['text_column_type']				 = 'Тип';
$_['text_column_date_added']		 = 'Створено';
$_['text_confirm_void']				 = 'Ви впевнені, що хочете анулювати платіж?';
$_['text_confirm_capture']			 = 'Ви впевнені, що ви хочете, щоб прийняти платіж?';
$_['text_confirm_rebate']			 = 'Ви впевнені, що хочете платіж зі знижкою?';
$_['text_realex']					 = '<img src="view/image/payment/realex.png" alt="Realex" title="Realex" style="border: 1px solid #EEEEEE;" />';

// Entry
$_['entry_merchant_id']				 = 'Ідентифікатор торговця';
$_['entry_secret']					 = 'Shared secret';
$_['entry_rebate_password']			 = 'Rebate password';
$_['entry_total']					 = 'Всього';
$_['entry_sort_order']				 = 'Порядок сортування';
$_['entry_geo_zone']				 = 'Регіон';
$_['entry_status']					 = 'Статус';
$_['entry_debug']					 = 'Ведення журналу налагодження';
$_['entry_live_demo']				 = 'Live / Демо';
$_['entry_auto_settle']				 = 'Тип врегулювання';
$_['entry_card_select']				 = 'Виберіть картку';
$_['entry_tss_check']				 = 'TSS checks';
$_['entry_live_url']				 = 'Діюча URL-адреса прямого зв\'язку';
$_['entry_demo_url']				 = 'Демо URL-адреса зв\'язку';
$_['entry_status_success_settled']	 = 'Success - settled';
$_['entry_status_success_unsettled'] = 'Success - not settled';
$_['entry_status_decline']			 = 'Відхилити';
$_['entry_status_decline_pending']	 = 'Decline - offline auth';
$_['entry_status_decline_stolen']	 = 'Decline - lost or stolen card';
$_['entry_status_decline_bank']		 = 'Decline - bank error';
$_['entry_status_void']				 = 'Анульовані';
$_['entry_status_rebate']			 = 'Rebated';
$_['entry_notification_url']		 = 'URL-адреса Сповіщення';

// Help
$_['help_total']					 = 'Сума замовлення повинна бути більше, перш ніж цей метод оплати стане активним';
$_['help_card_select']				 = 'Ask the user to choose their card type before they are redirected';
$_['help_notification']				 = 'You need to supply this URL to Realex to get payment notifications';
$_['help_debug']					 = 'Дозволивши налагодження, воно буде писати конфіденційні дані до файлу журналу. Вам завжди слід забороняти, доки ви не отримали інших вказівок';
$_['help_dcc_settle']				 = 'If your subaccount is DCC enabled you must use Autosettle';

// Tab
$_['tab_api']					     = 'Деталі API';
$_['tab_account']		     		 = 'Accounts';
$_['tab_order_status']				 = 'Стан замовлення';
$_['tab_payment']					 = 'Платіжні реквізити';
$_['tab_advanced']					 = 'Додатково';

// Button
$_['button_capture']				 = 'Capture';
$_['button_rebate']					 = 'Знижка / повернення';
$_['button_void']					 = 'Анулювати';

// Error
$_['error_merchant_id']				 = 'Merchant ID is required';
$_['error_secret']					 = 'Shared secret is required';
$_['error_live_url']				 = 'Діюча URL-адреса обов\'язкова';
$_['error_demo_url']				 = 'Demo URL is required';
$_['error_data_missing']			 = 'Дані відсутні';
$_['error_use_select_card']			 = 'You must have "Select Card" enabled for subaccount routing by card type to work';