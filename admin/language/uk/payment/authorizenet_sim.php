<?php
// Heading
$_['heading_title']			= 'Authorize.Net (SIM)';

// Text
$_['text_payment']			= 'Платежі';
$_['text_success']			= 'Успіх: Змінено реквізити рахунку Authorize.Net (AIM)!';
$_['text_edit']             = 'Редагування Authorize.Net з (AIM)';
$_['text_authorizenet_sim']	= '<a onclick="window.open(\'http://reseller.authorize.net/application/?id=5561142\');"><img src="view/image/payment/authorizenet.png" alt="Authorize.Net" title="Authorize.Net" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_merchant']		= 'ID Торговця';
$_['entry_key']				= 'Ключ транзакції';
$_['entry_callback']		= 'URL-посилання на відповідь';
$_['entry_md5']				= 'MD5 Кодове значення';
$_['entry_test']			= 'Тестовий режим';
$_['entry_total']			= 'Всього';
$_['entry_order_status']	= 'Стан замовлення';
$_['entry_geo_zone']		= 'Геозона';
$_['entry_status']			= 'Стан';
$_['entry_sort_order']		= 'Порядок сортування';

// Help
$_['help_callback']			= 'Будь ласка, увійдіть до ситеми і встановіть це в <a href="https://secure.authorize.net" target="_blank" class="txtLink"> https://secure.authorize.net</a>.';
$_['help_md5']				= 'MD5 кодова функція дозволяє Вам бути переконаними, що транзакції відповіді надійні, отримані від Authorize.Net. Будь ласка увійдіть і встановити це в <a href="https://secure.authorize.net" target="_blank" class="txtLink"> https://secure.authorize.net</a>. (Необов\'язково)';
$_['help_total']			= 'Цей метод стане доступним, коли загальна сума досягне суми мінімального замовлення.';

// Error
$_['error_permission']		= 'Попередження: У Вас немає дозволу на змінення платіжних Authorize.Net (SIM)!';
$_['error_merchant']		= 'Необхідне ID торговця!';
$_['error_key']				= 'Необхідний ключ транзакції!';