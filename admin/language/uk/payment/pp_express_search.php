<?php
// Heading
$_['heading_title']					= 'Пошук транзакцій';

// Column
$_['tbl_column_date']				= 'Дата';
$_['tbl_column_type']				= 'Тип';
$_['tbl_column_email']				= 'Email';
$_['tbl_column_name']				= 'Имя';
$_['tbl_column_transid']			= 'Ідентифікатор транзакції';
$_['tbl_column_status']				= 'Статус';
$_['tbl_column_currency']			= 'Валюта';
$_['tbl_column_amount']				= 'Сума';
$_['tbl_column_fee']				= 'Комісія';
$_['tbl_column_netamt']				= 'Net Amount';
$_['tbl_column_action']				= 'Действия';

// Text
$_['text_pp_express']				= 'Оформити замовлення PayPal Експрес';
$_['text_date_search']				= 'Пошук за датою';
$_['text_searching']				= 'Поиск';
$_['text_name']						= 'Ім’я';
$_['text_buyer_info']				= 'Інформація про покупця';
$_['text_view']						= 'Огляд';
$_['text_format']					= 'Формат';

// Entry
$_['entry_trans_all']				= 'Усі';
$_['entry_trans_sent']				= 'Надіслано';
$_['entry_trans_received']			= 'Отримано';
$_['entry_trans_masspay']			= 'Масовий платіж';
$_['entry_trans_money_req']			= 'Грошовий запит';
$_['entry_trans_funds_add']			= 'Кошти додано';
$_['entry_trans_funds_with']		= 'Виведені кошти';
$_['entry_trans_referral']			= 'Реферер';
$_['entry_trans_fee']				= 'Комісія';
$_['entry_trans_subscription']		= 'Підписка';
$_['entry_trans_dividend']			= 'Дивіденди';
$_['entry_trans_billpay']			= 'Сплатити рахунок';
$_['entry_trans_refund']			= 'Повернення коштів';
$_['entry_trans_conv']				= 'Конвертувати валюти';
$_['entry_trans_bal_trans']			= 'Переказ балансу';
$_['entry_trans_reversal']			= 'Reversal';
$_['entry_trans_shipping']			= 'Доставка';
$_['entry_trans_bal_affect']		= 'Balance Affecting';
$_['entry_trans_echeque']			= 'E Check';
$_['entry_date']					= 'Дата';
$_['entry_date_start']				= 'Розпочати';
$_['entry_date_end']				= 'Закінчити';
$_['entry_date_to']					= 'до';
$_['entry_transaction']				= 'Транзакція';
$_['entry_transaction_type']		= 'Тип';
$_['entry_transaction_status']		= 'Стан';
$_['entry_email']					= 'Електронна пошта';
$_['entry_email_buyer']				= 'Покупець';
$_['entry_email_merchant']			= 'Отримувач';
$_['entry_receipt']					= 'Ідентифікатор отримувача';
$_['entry_transaction_id']			= 'Ідентифікатор транзакції';
$_['entry_invoice_no']				= 'Номер рахунку-фактури';
$_['entry_auction']					= 'Auction item number';
$_['entry_amount']					= 'Кількість';
$_['entry_recurring_id']			= 'Recurring Profile ID';
$_['entry_salutation']				= 'Привітання';
$_['entry_firstname']				= 'Перший';
$_['entry_middlename']				= 'Середній';
$_['entry_lastname']				= 'Останній';
$_['entry_suffix']					= 'Суфікс';
$_['entry_status_all']				= 'Усі';
$_['entry_status_pending']			= 'В очікуванні';
$_['entry_status_processing']		= 'Обробляється';
$_['entry_status_success']			= 'Успішно';
$_['entry_status_denied']			= 'Заборонено';
$_['entry_status_reversed']			= 'Повернено';