<?php
// Text
$_['text_payment_info']				= 'Інформація про оплату';
$_['text_capture_status']			= 'Захопити статус';
$_['text_amount_auth']				= 'Всього затверджено';
$_['text_amount_captured']			= 'Сума збору';
$_['text_amount_refunded']			= 'Cума повернутого';
$_['text_capture_amount']			= 'Захопити cума';
$_['text_complete_capture']			= 'Закінчити збір';
$_['text_transactions']				= 'Транзакції';
$_['text_complete']					= 'Завершено';
$_['text_confirm_void']				= 'Якщо ви скасуєте, ви не зможете захопити більше коштів';
$_['text_view']						= 'Вигляд';
$_['text_refund']					= 'Повернення коштів';
$_['text_resend']					= 'Повторити надсилання';
$_['success_transaction_resent']	= 'Транзакцію було успішно перезапущено';

// Column
$_['column_trans_id']				= 'Ідентифікатор транзакції';
$_['column_amount']					= 'Кількість';
$_['column_type']					= 'Тип платежу';
$_['column_status']					= 'Статус';
$_['column_pend_reason']			= 'Причина очікування';
$_['column_date_added']				= 'Створено';
$_['column_action']					= 'Дія';

// Button
$_['button_void']					= 'Скасувати';
$_['button_capture']				= 'Захопити';

// Error
$_['error_capture_amt']				= 'Введіть суму для захоплення';
$_['error_timeout']					= 'Вичерпано час запиту';
$_['error_transaction_missing']		= 'Транзакцію не знайдено';