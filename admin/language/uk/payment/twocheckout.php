<?php
// Heading
$_['heading_title']					= '2CheckOut';

// Text
$_['text_payment']					= 'Платіж';
$_['text_success']					= 'Успіх: Змінено реквізити рахунку 2Checkout!';
$_['text_edit']                     = 'Редагувати 2Checkout';
$_['text_twocheckout']				= '<a href="https://www.2checkout.com/2co/affiliate?affiliate=1596408" target="_blank"><img src="view/image/payment/2checkout.png" alt="2Checkout" title="2Checkout" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_account']					= '2CheckOut Ідентифікатор облікового запису';
$_['entry_secret']					= 'Секретне слово';
$_['entry_display']					= 'Швидке замовлення';
$_['entry_test']					= 'Тестовий режим';
$_['entry_total']					= 'Разом';
$_['entry_order_status']			= 'Статус замовлення';
$_['entry_geo_zone']				= 'Регіон';
$_['entry_status']					= 'Стан';
$_['entry_sort_order']				= 'Порядок сортування';

// Help
$_['help_secret']					= 'Секретне слово для підтвердження операції з (має бути так само, як це визначено на сторінці конфігурації торговий рахунок).';
$_['help_total']					= 'Цей метод стане доступним, коли загальна сума досягне суми мінімального замовлення.';

// Error
$_['error_permission']				= 'Попередження: Вас немає дозволу на змінення оплати 2Checkout!';
$_['error_account']					= '№ рахунку Необхідні!';
$_['error_secret']					= 'Секретне слово необхідні!';