<?php
// Heading
$_['heading_title']					 = 'First Data EMEA підключення (3DSecure включений)';

// Text
$_['text_payment']					 = 'Платежі';
$_['text_success']					 = 'Успіх: Змінено реквізити рахунку First Data!';
$_['text_edit']                      = 'Редагувати First Data EMEA підключення (3DSecure включений)';
$_['text_notification_url']			 = 'URL-адреса Сповіщення';
$_['text_live']						 = 'Лайв';
$_['text_demo']						 = 'Демо';
$_['text_enabled']					 = 'Ввімкнено';
$_['text_merchant_id']				 = 'ID Магазину';
$_['text_secret']					 = 'Спільний секрет';
$_['text_capture_ok']				 = 'Прийом пройшов успішно';
$_['text_capture_ok_order']			 = 'Прийом був успішним, статус замовлення оновлений до успішного - врегульований';
$_['text_void_ok']					 = 'Анулювання було успішним, статус замовлення оновлено до анулювання';
$_['text_settle_auto']				 = 'Продаж';
$_['text_settle_delayed']			 = 'Попередньо затверджене';
$_['text_success_void']				 = 'Операція була анульована';
$_['text_success_capture']			 = 'Транзакції були прийняті';
$_['text_firstdata']				 = '<img src="view/image/payment/firstdata.png" alt="First Data" title="First Data" style="border: 1px solid #EEEEEE;" />';
$_['text_payment_info']				 = 'Інформація про оплату';
$_['text_capture_status']			 = 'Оплату прийняли';
$_['text_void_status']				 = 'Платіж анульований';
$_['text_order_ref']				 = 'Посилання на замовлення';
$_['text_order_total']				 = 'Всього затверджено';
$_['text_total_captured']			 = 'Загалом прийняли';
$_['text_transactions']				 = 'Сплаченні рахунки';
$_['text_column_amount']			 = 'Сума';
$_['text_column_type']				 = 'Тип';
$_['text_column_date_added']		 = 'СТВОРЕНО';
$_['text_confirm_void']				 = 'Ви впевнені, що хочете анулювати платіж?';
$_['text_confirm_capture']			 = 'Ви впевнені, що ви хочете, щоб прийняти платіж?';

// Entry
$_['entry_merchant_id']				 = 'ID Магазину';
$_['entry_secret']					 = 'Спільний секрет';
$_['entry_total']					 = 'Всього';
$_['entry_sort_order']				 = 'Порядок сортування';
$_['entry_geo_zone']				 = 'Регіон';
$_['entry_status']					 = 'Стан';
$_['entry_debug']					 = 'Реєстрація налагодження';
$_['entry_live_demo']				 = 'Діючий / Демо';
$_['entry_auto_settle']			  	 = 'Тип врегулювання';
$_['entry_card_select']				 = 'Вибрати картку';
$_['entry_tss_check']				 = 'TSS checks';
$_['entry_live_url']				 = 'Діюча URL-адреса прямому зв\'язку';
$_['entry_demo_url']				 = ' Демо URL-адреса зв\'язку';
$_['entry_status_success_settled']	 = 'Успіх - врегульований';
$_['entry_status_success_unsettled'] = 'Успіх - не врегульований';
$_['entry_status_decline']		 	 = 'Відхилити';
$_['entry_status_void']				 = 'Анульовані';
$_['entry_enable_card_store']		 = 'Увімкнути карти зберігання купонів';

// Help
$_['help_total']					 = 'Цей метод стане доступним, коли загальна сума досягне суми мінімального замовлення.';
$_['help_notification']				 = 'Потрібно вказати цю URL-адресу з First Data, щоб отримати платіж сповіщення';
$_['help_debug']					 = 'Включення налагодження напише конфіденційні дані до файлу журналу. Ви завжди повинні вимикати, якщо вказівка інакша';
$_['help_settle']					 = 'Якщо ви використовуєте попередню авторизацію,то ви повинні її завершити протягом 3-5 днів, в іншому випадку буде видалено транзакцію'; 

// Tab
$_['tab_account']					 = 'API інформація';
$_['tab_order_status']				 = 'Статус замовлення';
$_['tab_payment']					 = 'Налаштування оплати';
$_['tab_advanced']					 = 'Додатково';

// Button
$_['button_capture']				 = 'Прийом';
$_['button_void']					 = 'Анулювання';

// Error
$_['error_merchant_id']				 = 'Потрібний ID Магазину';
$_['error_secret']					 = 'Спільний секрет необхідно';
$_['error_live_url']				 = 'Діючу URL-адресу необхідно';
$_['error_demo_url']				 = 'Демо URL-адресу необхідно';
$_['error_data_missing']			 = 'Дані відсутні';
$_['error_void_error']				 = 'Не в змозі анулювати транзакцію';
$_['error_capture_error']			 = 'Не вдалося захопити транзакції';