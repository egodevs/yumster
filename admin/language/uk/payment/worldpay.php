<?php
// Heading
$_['heading_title']					= 'WorldPay';

// Text
$_['text_payment']					= 'Платіж';
$_['text_success']					= 'Успіх: Ви змінили деталі облікового запису WorldPay!';
$_['text_edit']                     = 'Редагувати WorldPay';
$_['text_worldpay']					= '<a href="https://business.worldpay.com/partner/opencart" target="_blank"> <IMG SRC = "перегляд / зображення / оплата / worldpay.png" Alt = "Worldpay" назва = "Worldpay" стиль = "кордону: 1px тверде #EEEEEE;" /> </a>';
$_['text_successful']				= 'Вкл. - Завжди успішно';
$_['text_declined']					= 'Вкл. - Завжди відхилено';
$_['text_off']						= 'Вимкнути';

// Entry
$_['entry_merchant']				= 'ID Продавця';
$_['entry_password']				= 'Платіжний пароль';
$_['entry_callback']				= 'URL-посилання на відповідь';
$_['entry_test']					= 'Тестовий режим';
$_['entry_total']					= 'Всього';
$_['entry_order_status']			= 'Статус замовлення';
$_['entry_geo_zone']				= 'Регіон';
$_['entry_status']					= 'Статус';
$_['entry_sort_order']				= 'Порядок сортування';

// Help
$_['help_password']					= 'Це повинно встановлюватися в панелі управління WorldPay.';
$_['help_callback']					= 'Це повинно встановлюватися в панелі управління WorldPay. Вам також потрібно буде включити «Enable the Shopper Response/Дозволити відповіді покупців».';
$_['help_total']					= 'Сума замовлення повинна бути більше, перш ніж цей метод оплати стане активним.';

// Error
$_['error_permission']				= 'Увага: у Вас немає дозволу на зміну параметрів платежу WorldPay!';
$_['error_merchant']				= 'Потрібно вказати ID Продавця!';
$_['error_password']				= 'Потрібно ввести пароль!';