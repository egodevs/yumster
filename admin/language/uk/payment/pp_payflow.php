<?php
// Heading
$_['heading_title']					= 'PayPal Payflow Pro';

// Text
$_['text_success']					= 'Успіх: Змінено реквізити рахунку PayPal Direct (UK)!';
$_['text_edit']                     = 'Редагувати PayPal Payflow Pro';
$_['text_pp_payflow']				= '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			= 'Авторизація';
$_['text_sale']						= 'Продаж';

// Entry
$_['entry_vendor']					= 'Постачальник';
$_['entry_user']					= 'Користувач';
$_['entry_password']				= 'Пароль';
$_['entry_partner']					= 'Партнер';
$_['entry_test']					= 'Тестовий режим';
$_['entry_transaction']				= 'Метод транзакції';
$_['entry_total']					= 'Всього';
$_['entry_order_status']			= 'Статус замовлення';
$_['entry_geo_zone']				= 'Регіон';
$_['entry_status']					= 'Статус';
$_['entry_sort_order']				= 'Порядок сортування';

// Help
$_['help_vendor']					= 'Your merchant login ID that you created when you registered for the Website Payments Pro account';
$_['help_user']						= 'If you set up one or more additional users on the account, this value is the ID of the user authorised to process transactions. If, however, you have not set up additional users on the account, USER has the same value as VENDOR';
$_['help_password']					= 'Пароль довжиною від 6 до 32 символів, що ви встановили підчас реєстрації облікового запису';
$_['help_partner']					= 'The ID provided to you by the authorised PayPal Reseller who registered you for the Payflow SDK. If you purchased your account directly from PayPal, use the PayPal Pro instead';
$_['help_test']						= 'Використовувати діюче або тестове (пісочниці) шлюзового сервера до процесу угоди?';
$_['help_total']					= 'The checkout total the order must reach before this payment method becomes active';

// Error
$_['error_permission']				= 'Попередження: У вас немає дозволу змінювати оплати PayPal Website Payment Pro (UK)!';
$_['error_vendor']					= 'Постачальника не вказано!';
$_['error_user']					= 'Користувача не вказано!';
$_['error_password']				= 'Потрібен пароль!';
$_['error_partner']					= 'Потрібен партнер!';