<?php
// Heading
$_['heading_title']					= 'Realex пульт дистанційного';

// Text
$_['text_payment']					= 'Платіж';
$_['text_success']					= 'Успіх: Ви змінили деталі облікового запису Realex!';
$_['text_edit']                     = 'Редагувати Realex віддалене управління';
$_['text_card_type']				= 'Тип карти';
$_['text_enabled']					= 'Увімкнено';
$_['text_use_default']				= 'Використовувати за замовчуванням';
$_['text_merchant_id']				= 'Ідентифікатор торговця';
$_['text_subaccount']				= 'Sub Account';
$_['text_secret']					= 'Спільний секрет';
$_['text_card_visa']				= 'Visa';
$_['text_card_master']				= 'MasterCard';
$_['text_card_amex']				= 'American Express';
$_['text_card_switch']				= 'Switch/Maestro';
$_['text_card_laser']				= 'Laser';
$_['text_card_diners']				= 'Diners';
$_['text_capture_ok']				= 'Прийом пройшов успішно';
$_['text_capture_ok_order']			= 'Прийом був успішним, статус замовлення оновлений до успішного - врегульований';
$_['text_rebate_ok']				= 'Знижка пройшла успішно';
$_['text_rebate_ok_order']			= 'Rebate was successful, order status updated to rebated';
$_['text_void_ok']					= 'Void was successful, order status updated to voided';
$_['text_settle_auto']				= 'Автоматично';
$_['text_settle_delayed']			= 'Відкладено';
$_['text_settle_multi']				= 'Мульті';
$_['text_ip_message']				= 'You must supply your server IP address to your Realex account manager before going live';
$_['text_payment_info']				= 'Інформація про оплату';
$_['text_capture_status']			= 'Оплату прийняли';
$_['text_void_status']				= 'Платіж анульований';
$_['text_rebate_status']			= 'Payment rebated';
$_['text_order_ref']				= 'Посилання на замовлення';
$_['text_order_total']				= 'Всього авторизовано';
$_['text_total_captured']			= 'Загалом прийняли';
$_['text_transactions']				= 'Транзакції';
$_['text_confirm_void']				= 'Ви впевнені, що хочете анулювати платіж?';
$_['text_confirm_capture']			= 'Ви впевнені, що ви хочете, щоб прийняти платіж?';
$_['text_confirm_rebate']			= 'Ви впевнені, що хочете платіж зі знижкою?';
$_['text_realex_remote']			= '<img src="view/image/payment/realex.png" alt="Realex" title="Realex" style="border: 1px solid #EEEEEE;" />';

// Column
$_['text_column_amount']			= 'Кількість';
$_['text_column_type']				= 'Тип';
$_['text_column_date_added']		= 'Створено';

// Entry
$_['entry_merchant_id']				= 'Ідентифікатор торговця';
$_['entry_secret']					= 'Спільний секрет';
$_['entry_rebate_password']			= 'Rebate password';
$_['entry_total']					= 'Разом';
$_['entry_sort_order']				= 'Порядок сортування';
$_['entry_geo_zone']				= 'Регіон';
$_['entry_status']					= 'Статус';
$_['entry_debug']					= 'Ведення журналу налагодження';
$_['entry_auto_settle']				= 'Тип врегулювання';
$_['entry_tss_check']				= 'TSS checks';
$_['entry_card_data_status']		= 'Card info logging';
$_['entry_3d']						= 'Увімкнути 3D Secure';
$_['entry_liability_shift']			= 'Accept non-liability shifting scenarios';
$_['entry_status_success_settled']	= 'Успіх - врегульований';
$_['entry_status_success_unsettled'] = 'Успіх - не врегульований';
$_['entry_status_decline']			= 'Відхилити';
$_['entry_status_decline_pending']	= 'Decline - offline auth';
$_['entry_status_decline_stolen']	= 'Decline - lost or stolen card';
$_['entry_status_decline_bank']		= 'Decline - bank error';
$_['entry_status_void']				= 'Анульовані';
$_['entry_status_rebate']			= 'Rebated';

// Help
$_['help_total']					= 'The checkout total the order must reach before this payment method becomes active';
$_['help_card_select']				= 'Попросіть користувача вибирати його тип карти, перш ніж їх буде переспрямовано';
$_['help_notification']				= 'You need to supply this URL to Realex to get payment notifications';
$_['help_debug']					= 'Дозволяючи налагодження буде писати конфіденційні дані до файлу журналу. Вам завжди слід вимикати, якщо вказівка інакша.';
$_['help_liability']				= 'Accepting liability means you will still accept payments when a user fails 3D secure.';
$_['help_card_data_status']			= 'Logs last 4 cards digits, expire, name, type and issuing bank information';

// Tab
$_['tab_api']					    = 'Деталі API';
$_['tab_account']				    = 'Облікові записи';
$_['tab_order_status']				= 'Статус замовлення';
$_['tab_payment']					= 'Payment Settings';

// Button
$_['button_capture']				= 'Capture';
$_['button_rebate']					= 'Rebate / refund';
$_['button_void']					= 'Анулювати';

// Error
$_['error_merchant_id']				= 'Потрібно вказати ID Продавця';
$_['error_secret']					= 'Shared secret is required';