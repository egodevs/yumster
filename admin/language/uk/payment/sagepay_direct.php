<?php
// Heading
$_['heading_title']					= 'SagePay прямі';

// Text
$_['text_payment']					= 'Платіж';
$_['text_success']					= 'Успіх: Ви змінили деталі облікового запису SagePay!';
$_['text_edit']                     = 'Редагувати SagePay Direct';
$_['text_sagepay_direct']			= '<a href="https://support.sagepay.com/apply/default.aspx?PartnerID=E511AF91-E4A0-42DE-80B0-09C981A3FB61" target="_blank"> <img src="view/image/payment/sagepay.png" alt="SagePay" title="SagePay" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_sim']						= 'Симулятор';
$_['text_test']						= 'Тест';
$_['text_live']						= 'Наживо';
$_['text_defered']					= 'Відкладено';
$_['text_authenticate']				= 'Автентифіковано';
$_['text_release_ok']				= 'Проведення платежу було успішним';
$_['text_release_ok_order']			= 'Release was successful, order status updated to success - settled';
$_['text_rebate_ok']				= 'Знижка пройшла успішно';
$_['text_rebate_ok_order']			= 'Rebate was successful, order status updated to rebated';
$_['text_void_ok']					= 'Void was successful, order status updated to voided';
$_['text_payment_info']				= 'Інформація про оплату';
$_['text_release_status']			= 'Платіж проведений';
$_['text_void_status']				= 'Платіж анульований';
$_['text_rebate_status']			= 'Платіж зі знижкою';
$_['text_order_ref']				= 'Посилання на замовлення';
$_['text_order_total']				= 'Всього авторизовано';
$_['text_total_released']			= 'Всього сплачено';
$_['text_transactions']				= 'Транзакції';
$_['text_column_amount']			= 'Сума';
$_['text_column_type']				= 'Тип';
$_['text_column_date_added']		= 'Створено';
$_['text_confirm_void']				= 'Ви впевнені, що хочете анулювати платіж?';
$_['text_confirm_release']			= 'Are you sure you want to release the payment?';
$_['text_confirm_rebate']			= 'Are you sure you want to rebate the payment?';

// Entry
$_['entry_vendor']					= 'Постачальник';
$_['entry_test']					= 'Тестовий режим';
$_['entry_transaction']				= 'Transaction Method';
$_['entry_total']					= 'Разом';
$_['entry_order_status']			= 'Статус замовлення';
$_['entry_geo_zone']				= 'Регіон';
$_['entry_status']					= 'Стан';
$_['entry_sort_order']				= 'Порядок сортування';
$_['entry_debug']					= 'Ведення журналу налагодження';
$_['entry_card']					= 'Store Cards';
$_['entry_cron_job_token']			= 'Секретний ключ';
$_['entry_cron_job_url']			= 'URL-адреса запланованого завдання';
$_['entry_last_cron_job_run']		= 'Час запуску останній запланованого завдання:';

// Help
$_['help_total']					= 'Сума замовлення повинна бути більше, перш ніж цей метод оплати стане активним.';
$_['help_debug']					= 'Enabling debug will write sensitive data to a log file. You should always disable unless instructed otherwise';
$_['help_transaction']				= 'Transaction method MUST be set to Payment to allow subscription payments';
$_['help_cron_job_token']			= 'Зробити це складним для розшифровування';
$_['help_cron_job_url']				= 'Встановити заплановане завдання для виклику даної URL-адреси';

// Button
$_['btn_release']					= 'Сплатити';
$_['btn_rebate']					= 'Знижка / повернення';
$_['btn_void']						= 'Анулювати';

// Error
$_['error_permission']				= 'Увага: у Вас немає дозволу на зміну параметрів платежу WorldPay!';
$_['error_vendor']					= 'Vendor ID Required!';