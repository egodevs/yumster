<?php
// Heading
$_['heading_title']					= 'Klarna рахунок';

// Text
$_['text_payment']					= 'Платежі';
$_['text_success']					= 'Успіх: Змінено модуль Klarna оплати!';
$_['text_edit']                     = 'Редагування рахунку Klarna ';
$_['text_klarna_invoice']			= '<a href="https://merchants.klarna.com/signup?locale=en&partner_id=d5c87110cebc383a826364769047042e777da5e8&utm_campaign=Platform&utm_medium=Partners&utm_source=Opencart" target="_blank"><img src="https://cdn.klarna.com/public/images/global/logos/v1/basic/global_basic_logo_std_blue-black.png?width=60&eid=opencart" alt="Klarna Invoice" title="Klarna Invoice" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live']						= 'Діючий';
$_['text_beta']						= 'Бета';
$_['text_sweden']					= 'Швеція';
$_['text_norway']					= 'Норвегія';
$_['text_finland']					= 'Фінляндія';
$_['text_denmark']					= 'Данія';
$_['text_germany']					= 'Німеччина';
$_['text_netherlands']				= 'Нідерланди';

// Entry
$_['entry_merchant']				= 'Klarna купець ID';
$_['entry_secret']					= 'Klarna Secret';
$_['entry_server']					= 'Сервер';
$_['entry_total']					= 'Всього';
$_['entry_pending_status']			= 'В очікуванні';
$_['entry_accepted_status']			= 'Прийнятий статус';
$_['entry_geo_zone']				= 'Геозона';
$_['entry_status']					= 'Стан';
$_['entry_sort_order']				= 'Порядок сортування';

// Help
$_['help_merchant']					= '(Інтернет-магазин id) для використання в Klarna послуги (через Klarna).';
$_['help_secret']					= 'Відкритий secret для використання на Klarna послуги (через Klarna).';
$_['help_total']					= 'Цей метод стане доступним, коли загальна сума досягне суми мінімального замовлення.';

// Error
$_['error_permission']				= 'Попередження: Немає дозволу змінювати платіжних Klarna частину платежів!';