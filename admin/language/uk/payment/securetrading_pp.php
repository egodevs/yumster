<?php
$_['heading_title'] = 'Безпечний торговий оплати сторінок';

$_['text_securetrading_pp'] = '<img src="view/image/payment/secure_trading.png" alt="Secure Trading" title="Secure Trading" style="border: 1px solid #EEEEEE;" /><img src="view/image/payment/secure_trading.png" alt="Secure Trading" title="Secure Trading" style="border: 1px solid #EEEEEE;" />';
$_['text_payment'] = 'Платіж';
$_['text_enabled'] = 'Увімкнено';
$_['text_disabled'] = 'Вимкнено';
$_['text_yes'] = 'Так';
$_['text_no'] = 'Ні';
$_['text_all_geo_zones'] = 'Всі регіони';
$_['text_process_immediately'] = 'Примінити негайно';
$_['text_wait_x_days'] = 'Чекайте %d днів';
$_['text_success'] = 'Успіх: Змінено безпечний торговий модуль!';
$_['text_pending_settlement'] = 'Pending Settlement';
$_['text_authorisation_reversed'] = 'Авторизацію успішно скасовано';
$_['text_refund_issued'] = 'Refund was successfully issued';
$_['text_pending_settlement_manually_overriden'] = 'Pending Settlement, manually overriden';
$_['text_pending_suspended'] = 'Призупинений';
$_['text_pending_settled'] = 'Settled';

$_['entry_site_reference'] = 'Посилання сайту';
$_['entry_username'] = 'Ім’я користувача';
$_['entry_password'] = 'Пароль';
$_['entry_site_security_status'] = 'Use Site Security hash';
$_['entry_site_security_password'] = 'Пароль безпеки сайту';
$_['entry_notification_password'] = 'Notification password';
$_['entry_order_status'] = 'Стан замовлення';
$_['entry_declined_order_status'] = 'Declined order status';
$_['entry_refunded_order_status'] = 'Статус замовлення за яким повернуто гроші';
$_['entry_authorisation_reversed_order_status'] = 'Authorisation reversed order status';
$_['entry_settle_status'] = 'Settlement status';
$_['entry_settle_due_date'] = 'Settlement due date';
$_['entry_geo_zone'] = 'Регіон';
$_['entry_sort_order'] = 'Порядок сортування';
$_['entry_status'] = 'Стан';
$_['entry_total'] = 'Разом';
$_['entry_parent_css'] = 'Parent CSS';
$_['entry_child_css'] = 'Child CSS';
$_['entry_cards_accepted'] = 'Приймаються картки';
$_['entry_reverse_authorisation'] = 'Reverse Authorisation:';
$_['entry_refunded'] = 'Повернуті:';
$_['entry_refund'] = 'Issue refund (%s):';

$_['error_permission'] = 'У вас немає прав для змінення цього модуля';
$_['error_site_reference'] = 'Необхідно ввести посилання на сайт';
$_['error_notification_password'] = 'Notification password is required';
$_['error_cards_accepted'] = 'Accepted cards is required';
$_['error_username'] = 'Потрібне ім\'я користувача';
$_['error_password'] = 'Password is required';
$_['error_connection'] = 'Could not connect to Secure Trading';
$_['error_data_missing'] = 'Дані відсутні';

$_['help_username'] = 'Your webservice username';
$_['help_password'] = 'Your webservice password';
$_['help_refund'] = 'Please include the decimal point and the decimal part of the amount';
$_['help_total'] = 'Сума замовлення повинна бути більше, перш ніж цей метод оплати стане активним';

$_['button_reverse_authorisation'] = 'Reverse Authorisation';
$_['button_refund'] = 'Повернення коштів';


// Order page - payment tab
$_['text_payment_info'] = 'Інформація про оплату';
$_['text_release_status'] = 'Платіж проведений';
$_['text_void_status'] = 'Reverse Authorisation';
$_['text_rebate_status'] = 'Payment rebated';
$_['text_order_ref'] = 'Посилання на замовлення';
$_['text_order_total'] = 'Всього авторизовано';
$_['text_total_released'] = 'Всього сплачено';
$_['text_transactions'] = 'Транзакції';
$_['text_column_amount'] = 'Кількість';
$_['text_column_type'] = 'Тип';
$_['text_column_created'] = 'Створено';
$_['text_release_ok'] = 'Проведення платежу було успішним';
$_['text_release_ok_order'] = 'Release was successful, order status updated to success - settled';
$_['text_rebate_ok'] = 'Знижка пройшла успішно';
$_['text_rebate_ok_order'] = 'Знижка пройшла успішно, статус замовлення для знижки оновлюється';
$_['text_void_ok'] = 'Анулювання було успішним, статус замовлення оновлено до анульованого';

$_['text_confirm_void'] = 'Ви дійсно бажаєте скасувати дозвіл?';
$_['text_confirm_release'] = 'Ви впевнені, що хочете провести платіж?';
$_['text_confirm_rebate'] = 'Are you sure you want to rebate the payment?';

$_['btn_release'] = 'Release';
$_['btn_rebate'] = 'Знижка / повернення';
$_['btn_void'] = 'Reverse Authorisation';