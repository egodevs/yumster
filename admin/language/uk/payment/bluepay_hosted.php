<?php
// Heading
$_['heading_title']            = 'BluePay Hosted Form';

// Text
$_['text_payment']             = 'Платежі';
$_['text_success']             = 'Успішно змінено реквізити рахунку BluePay Hosted Form';
$_['text_edit']                = 'Редагування BluePay Hosted Form';
$_['text_bluepay_hosted'] = '<a href="http://www.bluepay.com/preferred-partner/opencart" target="_blank"><img src="view/image/payment/bluepay.jpg" alt="BluePay Hosted Form" title="BluePay Hosted Form" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_test']                = 'Тест';
$_['text_live']                = 'Діючий';
$_['text_sale']                = 'Продаж';
$_['text_authenticate']        = 'Автентифіковано';
$_['text_release_ok']          = 'Реліз був успішним';
$_['text_release_ok_order']    = 'Реліз був успішним';
$_['text_rebate_ok']           = 'Реліз був успішним';
$_['text_rebate_ok_order']     = 'Знижка пройшла успішно, статус замовлення для знижки оновлюється';
$_['text_void_ok']             = 'Анулювання було успішним, статус замовлення оновлено до анулювання';
$_['text_payment_info']        = 'Інформація про оплату';
$_['text_release_status']			= 'Платіж проведений';
$_['text_void_status']				= 'Платіж анульований';
$_['text_rebate_status']			= 'Платіж зі знижкою';
$_['text_order_ref']				= 'Посилання на замовлення';
$_['text_order_total']				= 'Всього затверджено';
$_['text_total_released']			= 'Всього випущено';
$_['text_transactions']				= 'Сплаченні рахунки';
$_['text_column_amount']			= 'Сума';
$_['text_column_type']				= 'Тип';
$_['text_column_date_added']		= 'Дата додавання';
$_['text_confirm_void']				= 'Ви впевнені, що хочете анулювати платіж?';
$_['text_confirm_release']			= 'Ви впевнені, що хочете провести платіж?';
$_['text_confirm_rebate']			= 'Ви впевнені, що хочете платіж зі знижкою?';

// Entry
$_['entry_account_name']			= 'Ім\'я облікового запису';
$_['entry_account_id']				= 'Account ID';
$_['entry_secret_key']				= "Секретний ключ";
$_['entry_test']					= 'Тестовий режим';
$_['entry_transaction']				= 'Метод транзакції';
$_['entry_card_amex']				= 'Amex';
$_['entry_card_discover']			= 'Дізнайтеся';
$_['entry_total']					= 'Всього';
$_['entry_order_status']			= 'Стан замовлення';
$_['entry_geo_zone']				= 'Геозона';
$_['entry_status']					= 'Стан';
$_['entry_sort_order']				= 'Порядок сортування';
$_['entry_debug']					= 'Реєстрація налагодження';

// Help
$_['help_total']					= 'Цей метод стане доступним, коли загальна сума досягне суми мінімального замовлення.';
$_['help_debug']					= 'Включення налагодження напише конфіденційні дані до файлу журналу. Ви завжди повинні вимикати, якщо вказівка інакша';
$_['help_transaction']				= 'Слід установити метод транзакції для оплати, щоб дозволити підписки платежів';
$_['help_cron_job_token']			= 'Зробити це складним для вгадування';
$_['help_cron_job_url']				= 'Встановити заплановане завдання для виклику даної URL-адреси';

// Button
$_['btn_release']					= 'Реліз';
$_['btn_rebate']					= 'Знижка / повернення';
$_['btn_void']						= 'Анулювання';

// Error
$_['error_permission']				= 'Увага: у Вас немає дозволу на зміну параметрів платежу WorldPay!';
$_['error_account_name']			= 'Необхідно ім\'я облікового запису ';
$_['error_account_id']				= 'Необхіден ідентифікатор облікового запису ';
$_['error_secret_key']				= 'Необхідний секретний ключ';