<?php
// Heading
$_['heading_title']					= 'Payza';

// Text
$_['text_payment']					= 'Платіж';
$_['text_success']					= 'Успіх: Змінено реквізити рахунку Payza!';
$_['text_edit']                     = 'Редагувати Payza';

// Entry
$_['entry_merchant']				= 'ID Продавця';
$_['entry_security']				= 'Захисний код';
$_['entry_callback']				= 'Оповіщення URL';
$_['entry_total']					= 'Всього';
$_['entry_order_status']			= 'Статус замовлення';
$_['entry_geo_zone']				= 'Регіон';
$_['entry_status']					= 'Статус';
$_['entry_sort_order']				= 'Порядок сортування';

// Help
$_['help_callback']					= 'Це повинно бути встановлено в панелі керування Payza. Вам також потрібно встановити "Статус IPN" на увімкнено.';
$_['help_total']					= 'Сума замовлення повинна бути більше, перш ніж цей метод оплати стане активним.';

// Error
$_['error_permission']				= 'Увага: у Вас немає дозволу на зміну параметрів платежу Payza!';
$_['error_merchant']				= 'Потрібно вказати ID Продавця!';
$_['error_security']				= 'Потрібен захисний код!';