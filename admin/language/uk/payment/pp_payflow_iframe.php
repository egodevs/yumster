<?php
// Heading
$_['heading_title']					= 'PayPal Payflow Pro iFrame';
$_['heading_refund']				= 'Повернення коштів';

// Text
$_['text_payment']					= 'Платіж';
$_['text_success']					= 'Успіх: Змінено реквізити рахунку PayPal Pro iFrame!';
$_['text_edit']                     = 'Редагувати PayPal Payflow Pro';
$_['text_pp_payflow_iframe']		= '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			= 'Авторизація';
$_['text_sale']						= 'Продаж';
$_['text_authorise']				= 'Авторизувати';
$_['text_capture']					= 'Відкладений прийом';
$_['text_void']						= 'Скасувати';
$_['text_payment_info']				= 'Інформація про оплату';
$_['text_complete']					= 'Готово';
$_['text_incomplete']				= 'Не закінчено';
$_['text_transaction']				= 'Транзакції';
$_['text_confirm_void']				= 'If you void you cannot capture any further funds';
$_['text_refund']					= 'Повернення коштів';
$_['text_refund_issued']			= 'Повернення коштів пройшло успішно';
$_['text_redirect']					= 'Перенаправлення';
$_['text_iframe']					= 'Плаваючий кадр';
$_['help_checkout_method']			= "Please use Redirect method if do not have SSL installed or if you do not have Pay with PayPal option disabled on your hosted payment page.";

// Column
$_['column_transaction_id']			= 'Ідентифікатор транзакції';
$_['column_transaction_type']		= 'Тип транзакції';
$_['column_amount']					= 'Сума';
$_['column_time']					= 'Час';
$_['column_actions']				= 'Дії';

// Tab
$_['tab_settings']					= 'Налаштування';
$_['tab_order_status']				= 'Статус замовлення';
$_['tab_checkout_customisation']	= 'Checkout Customisation';

// Entry
$_['entry_vendor']					= 'Постачальник';
$_['entry_user']					= 'Користувач';
$_['entry_password']				= 'Пароль';
$_['entry_partner']					= 'Партнер';
$_['entry_test']					= 'Тестовий режим';
$_['entry_transaction']				= 'Метод транзакції';
$_['entry_total']					= 'Разом';
$_['entry_order_status']			= 'Стан замовлення';
$_['entry_geo_zone']				= 'Регіон';
$_['entry_status']					= 'Стан';
$_['entry_sort_order']				= 'Порядок сортування';
$_['entry_transaction_id']			= 'Ідентифікатор транзакції';
$_['entry_full_refund']				= 'Повне повернення коштів';
$_['entry_amount']					= 'Сума';
$_['entry_message']					= 'Повідомлення';
$_['entry_ipn_url']					= 'IPN URL';
$_['entry_checkout_method']			= 'Checkout Method';
$_['entry_debug']					= 'Режим налагодження';
$_['entry_transaction_reference']	= 'Посилання на транзакцію';
$_['entry_transaction_amount']		= 'Transaction Amount';
$_['entry_refund_amount']			= 'Сума відшкодування';
$_['entry_capture_status']			= 'Статус прийнято';
$_['entry_void']					= 'Анулювати';
$_['entry_capture']					= 'Прийняти';
$_['entry_transactions']			= 'Транзакції';
$_['entry_complete_capture']		= 'Complete Capture';
$_['entry_canceled_reversal_status'] = 'Canceled Reversal Status:';
$_['entry_completed_status']		= 'Статус Завершено:';
$_['entry_denied_status']			= 'Denied Status:';
$_['entry_expired_status']			= 'Статус Протерміновано:';
$_['entry_failed_status']			= 'Статус Невдало:';
$_['entry_pending_status']			= 'Статус в очікуванні:';
$_['entry_processed_status']		= 'Processed Status:';
$_['entry_refunded_status']			= 'Refunded Status:';
$_['entry_reversed_status']			= 'Reversed Status:';
$_['entry_voided_status']			= 'Статус Анульований:';
$_['entry_cancel_url']				= 'Cancel URL:';
$_['entry_error_url']				= 'Error URL:';
$_['entry_return_url']				= 'Return URL:';
$_['entry_post_url']				= 'Silent POST URL:';

// Help
$_['help_vendor']					= 'Your merchant login ID that you created when you registered for the Website Payments Pro account';
$_['help_user']						= 'If you set up one or more additional users on the account, this value is the ID of the user authorised to process transactions. If, however, you have not set up additional users on the account, USER has the same value as VENDOR';
$_['help_password']					= 'The 6 to 32 character password that you defined while registering for the account';
$_['help_partner']					= 'The ID provided to you by the authorised PayPal Reseller who registered you for the Payflow SDK. If you purchased your account directly from PayPal, use the PayPal Pro instead';
$_['help_test']						= 'Use the live or testing (sandbox) gateway server to process transactions?';
$_['help_total']					= 'Цей метод стане доступним, коли загальна сума досягне суми мінімального замовлення';
$_['help_debug']					= 'Logs additional information';

// Button
$_['button_refund']					= 'Повернення коштів';
$_['button_void']					= 'Анулювати';
$_['button_capture']				= 'Capture';

// Error
$_['error_permission']				= 'Warning: You do not have permission to modify payment PayPal Website Payment Pro iFrame (UK)!';
$_['error_vendor']					= 'Постачальника не вказано!';
$_['error_user']					= 'Користувача не вказано!';
$_['error_password']				= 'Потрібен пароль!';
$_['error_partner']					= 'Partner Required!';
$_['error_missing_data']			= 'Відсутні дані';
$_['error_missing_order']			= 'Не вдалося знайти замовлення';
$_['error_general']					= 'Сталася помилка';
$_['error_capture_amt']				= 'Введіть суму для прийому';