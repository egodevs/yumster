<?php
// Heading
$_['heading_title']		 = 'Free Checkout';

// Text
$_['text_payment']		 = 'Платіж';
$_['text_success']		 = 'Успіх: Змінено Free Checkout платіжний модуль!';
$_['text_edit']          = 'Редагувати Free Checkout';

// Entry
$_['entry_order_status'] = 'Статус замовлення';
$_['entry_status']		 = 'Статус';
$_['entry_sort_order']	 = 'Порядок замовлення';

// Error
$_['error_permission']	  = 'Попередження: Вас немає дозволу на змінення оплати Free Checkout!';