<?php
// Heading
$_['heading_title']					= 'PayPoint';

// Text
$_['text_payment']					= 'Платіж';
$_['text_success']					= 'Успіх: Змінено реквізити рахунку Paymate!';
$_['text_edit']                     = 'Змінити PayPoint';
$_['text_paypoint']					= '<a href="https://www.paypoint.net/partners/opencart" target="_blank"><img src="view/image/payment/paypoint.png" alt="PayPoint" title="PayPoint" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live']						= 'Виробництво';
$_['text_successful']				= 'Завжди успішно';
$_['text_fail']						= 'Завжди ні';

// Entry
$_['entry_merchant']				= 'Публічний ID';
$_['entry_password']				= 'Віддалений пароль';
$_['entry_test']					= 'Тестовий режим';
$_['entry_total']					= 'Всього';
$_['entry_order_status']			= 'Статус замовлення';
$_['entry_geo_zone']				= 'Регіон';
$_['entry_status']					= 'Статус';
$_['entry_sort_order']				= 'Порядок сортування';

// Help
$_['help_password']					= 'Залиште пустим, якщо у вас немає "Digest Key Authentication" увімкнуто на вашому рахунку.';
$_['help_total']					= 'Цей метод стане доступним, коли загальна сума досягне суми мінімального замовлення.';

// Error
$_['error_permission']				= 'Увага: у Вас немає дозволу на зміну параметрів платежу WorldPay!';
$_['error_merchant']				= 'Потрібно вказати ID Продавця!';