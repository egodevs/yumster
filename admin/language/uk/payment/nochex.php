<?php
// Heading
$_['heading_title']					= 'NOCHEX';

// Text
$_['text_payment']					= 'Платіж';
$_['text_success']					= 'Успіх: Змінено реквізити рахунку NOCHEX!';
$_['text_edit']                     = 'Змінити NOCHEX';
$_['text_nochex']					= '<a href="https://secure.nochex.com/apply/merchant_info.aspx?partner_id=172198798" target="_blank"><img src="view/image/payment/nochex.png" alt="NOCHEX" title="NOCHEX" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_seller']					= 'Продавець / особистий рахунок';
$_['text_merchant']					= 'Торговий рахунок';

// Entry
$_['entry_email']					= 'E-Mail';
$_['entry_account']					= 'Тип облікового запису';
$_['entry_merchant']				= 'ID продавця';
$_['entry_template']				= 'Передати шаблон';
$_['entry_test']					= 'Тест';
$_['entry_total']					= 'Разом';
$_['entry_order_status']			= 'Статус замовлення';
$_['entry_geo_zone']				= 'Регіон';
$_['entry_status']					= 'Статус';
$_['entry_sort_order']				= 'Порядок сортування';

// Help
$_['help_total']					= 'Цей метод стане доступним, коли загальна сума досягне суми мінімального замовлення.';

// Error
$_['error_permission']				= 'Попередження: Вас немає дозволу на зміни оплати LIQPAY!';
$_['error_email']					= 'E-Mail обов\'язково!';
$_['error_merchant']				= 'Потрібно вказати ID Продавця!';