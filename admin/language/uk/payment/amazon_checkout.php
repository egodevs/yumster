<?php
// Heading
$_['heading_title']					= 'Amazon Payments';

// Text
$_['text_payment']					= 'Платежі';
$_['text_success']					= 'Успіх: Amazon Payments модуль було оновлено';
$_['text_edit']                     = 'Редагувати Amazon Payment';
$_['text_germany']					= 'Німеччина';
$_['text_uk']						= 'Великобританія';
$_['text_live']						= 'Діючий';
$_['text_sandbox']					= 'Пісочниця';
$_['text_upload_success']			= 'Файл переданий успішно ';
$_['text_button_settings']			= 'Параметри налаштунок кнопки Оформити замовлення';
$_['text_orange']					= 'Помаранчевий';
$_['text_tan']						= 'Коричневий';
$_['text_white']					= 'Білий';
$_['text_light']					= 'Світлий';
$_['text_dark']						= 'Темний';
$_['text_medium']					= 'Середній';
$_['text_large']					= 'Великий';
$_['text_x_large']					= 'Дуже великий';
$_['text_download']					= '<a href="%s" target="_blank"> <u>завантажити</u></a> файл шаблону з Amazon Seller Central.';
$_['text_amazon_details']			= 'Amazon подробиці';
$_['text_amazon_order_id']			= 'Amazon код замовлення';
$_['text_upload']					= 'Завантажити';
$_['text_upload_template']			= 'Завантажте заповнений шаблон, натиснувши на кнопку нижче. Переконайтеся, що він зберігається як файл з розділювальними символами табуляції.';
$_['text_amazon_checkout']			= '<a onclick="window.open(\'http://go.amazonservices.com/UKCBASPOpenCart.html\');"><img src="view/image/payment/amazon.png" alt="Amazon Payments" title="Amazon Payments" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_amazon_join']				= 'Для створення облікового запису Amazon Payments <a href="http://go.amazonservices.com/UKCBASPOpenCart.html" target="_blank" title="Click here to join Amazon Payments" class="alert-link"> натисніть тут.</a>';

// Column
$_['column_submission_id']			= 'Подання ID';
$_['column_status']					= 'Статус';
$_['column_text']					= 'Відповідь';
$_['column_amazon_order_item_code'] = 'Код продукту замовлення Amazon ';

// Entry
$_['entry_merchant_id']				= 'ID Торговця';
$_['entry_access_key']				= 'Ключ доступу';
$_['entry_access_secret']			= 'Секретний ключ';
$_['entry_checkout_mode']			= 'Режим перевірки';
$_['entry_marketplace']				= 'Торговий майданчик';
$_['entry_pending_status']			= 'Очікування';
$_['entry_ready_status']		    = 'Статус: Готовий до відправки';
$_['entry_shipped_status']			= 'Статус: Відгрузка';
$_['entry_canceled_status']			= 'Статус: Замовлення відмінене';
$_['entry_cron_job_url']			= 'URL-адреса запланованого завдання';
$_['entry_cron_job_token']			= 'Секретний символ';
$_['entry_cron_job_last_run']		= 'Час останнього виконання запланованих завдань';
$_['entry_ip']						= 'ІР-адреса';
$_['entry_ip_allowed']				= 'Допустимі IP-адреси';
$_['entry_total']	         		= 'Всього';
$_['entry_geo_zone']			    = 'Геозона';
$_['entry_status']					= 'Стан';
$_['entry_sort_order']				= 'Порядок сортування';
$_['entry_colour']					= 'Колір';
$_['entry_background']				= 'Фон';
$_['entry_size']				    = 'Розмір';

// Help
$_['help_cron_job_url']				= 'Встановити заплановане завдання для виклику даної URL-адреси';
$_['help_adjustment']				= ' Поля виділені жирним принаймні одне "-adj" обов\'язкові для заповнення.';
$_['help_allowed_ips']				= 'Залиште пустим, якщо потрібно, щоб всі бачили кнопку підтвердження ';
$_['help_ip']						= 'Наприклад 203.0.113.0 < br / > не заповнюйте, щоб дозволити всі IP-адреси.';
$_['help_cron_job_token']			= 'Зробити це довго і важко для вгадування';

// Tab
$_['tab_order_adjustment']			= 'Корегування замовлення';

// Error
$_['error_permissions']				= 'У вас немає прав для змінення цього модуля';
$_['error_access_secret']		    = 'Потрібно секретний ключ ';
$_['error_access_key']		        = 'Потрібний ключ доступу';
$_['error_merchant_id']		        = 'Потрібний ID продавця';
$_['error_curreny']					= 'Ваш магазин повинен мати %s валюти, також має бути встановлений і включений';
$_['error_upload']					= 'Помилка вивантаження';