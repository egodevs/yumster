<?php
// Heading
$_['heading_title']         = 'Authorize.Net (AIM)';

// Text
$_['text_payment']          = 'Платіж';
$_['text_success']          = 'Успіх: Змінено реквізити рахунку Authorize.Net (AIM)!';
$_['text_edit']             = 'Редагування Authorize.Net з (AIM)';
$_['text_test']             = 'Тест';
$_['text_live']             = 'Діючий';
$_['text_authorization']    = 'Авторизація';
$_['text_capture']          = 'Захоплення';
$_['text_authorizenet_aim'] = '<a onclick="window.open(\'http://reseller.authorize.net/application/?id=5561142\');"><img src="view/image/payment/authorizenet.png" alt="Authorize.Net" title="Authorize.Net" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_login']           = 'Ідентифікатор входу';
$_['entry_key']             = 'Ключ транзакції';
$_['entry_hash']            = 'MD5 хеш';
$_['entry_server']          = 'Транзакції-сервер';
$_['entry_mode']            = 'Режим транзакції';
$_['entry_method']          = 'Метод транзакції';
$_['entry_total']           = 'Всього';
$_['entry_order_status']    = 'Стан замовлення';
$_['entry_geo_zone']        = 'Геозона';
$_['entry_status']          = 'Стан';
$_['entry_sort_order']      = 'Порядок сортування';

// Help
$_['help_total']            = 'Цей метод стане доступним, коли загальна сума досягне суми мінімального замовлення.';

// Error
$_['error_permission']      = 'Попередження: Вас немає дозволу на змінення платіжних Authorize.Net (SIM)!';
$_['error_login']           = 'Ідентифікатор входу обов\'язково!';
$_['error_key']             = 'Необхідний ключ транзакції!';