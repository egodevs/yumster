<?php
// Heading
$_['heading_title']					= 'Sage Payment Solutions (США)';

// Text
$_['text_payment']					= 'Платіж';
$_['text_success']					= 'Успіх: Ви змінили деталі облікового запису WorldPay!';
$_['text_edit']                     = 'Редагувати Sage Payment Solutions (США)';

// Entry
$_['entry_merchant_id']				= 'ID Продавця';
$_['entry_merchant_key']			= 'Ключ продавця';
$_['entry_total']					= 'Всього';
$_['entry_order_status']			= 'Статус замовлення';
$_['entry_geo_zone']				= 'Регіон';
$_['entry_status']					= 'Статус';
$_['entry_sort_order']				= 'Порядок сортування';

// Help
$_['help_total']					= 'Цей метод стане доступним, коли загальна сума досягне суми мінімального замовлення.';

// Error
$_['error_permission']				= 'Увага: у Вас немає дозволу на зміну параметрів платежу WorldPay!';
$_['error_merchant_id']				= 'Необхідний ID продавця!';
$_['error_merchant_key']			= 'Необхідний ключ покупця!';