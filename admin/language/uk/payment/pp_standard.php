<?php
// Heading
$_['heading_title']					 = 'Стандарт платежі PayPal';

// Text
$_['text_payment']					 = 'Платіж';
$_['text_success']					 = 'Успіх: Ви змінили деталі облікового запису PayPal!';
$_['text_edit']                      = 'Редагувати PayPal Payments Standard';
$_['text_pp_standard']				 = '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			 = 'Авторизація';
$_['text_sale']						 = 'Продаж'; 

// Entry
$_['entry_email']					 = 'Електронна пошта';
$_['entry_test']					 = 'Режим пісочниці';
$_['entry_transaction']				 = 'Метод транзакції';
$_['entry_debug']					 = 'Режим налагодження';
$_['entry_total']					 = 'Всього';
$_['entry_canceled_reversal_status'] = 'Canceled Reversal Status';
$_['entry_completed_status']		 = 'Статус Завершено';
$_['entry_denied_status']			 = 'Статус Заборонено';
$_['entry_expired_status']			 = 'Статус Протермінований';
$_['entry_failed_status']			 = 'Статус Невдало';
$_['entry_pending_status']			 = 'Статус в очікуванні';
$_['entry_processed_status']		 = 'Статус оброблені';
$_['entry_refunded_status']			 = 'Статус повернення';
$_['entry_reversed_status']			 = 'Статус повернення';
$_['entry_voided_status']			 = 'Статус Анульований';
$_['entry_geo_zone']				 = 'Регіон';
$_['entry_status']					 = 'Стан';
$_['entry_sort_order']				 = 'Порядок сортування';

// Tab
$_['tab_general']					 = 'Загальне';
$_['tab_order_status']       		 = 'Стан замовлення';

// Help
$_['help_test']						 = 'Use the live or testing (sandbox) gateway server to process transactions?';
$_['help_debug']			    	 = 'Logs additional information to the system log';
$_['help_total']					 = 'Цей метод стане доступним, коли загальна сума досягне суми мінімального замовлення';

// Error
$_['error_permission']				 = 'Увага: у Вас немає дозволу на зміну параметрів платежу PayPal!';
$_['error_email']					 = 'E-Mail обов\'язково!';