<?php
// Heading
$_['heading_title']					= 'PayPal Pro';

// Text
$_['text_success']					= 'Успіх: Змінено реквізити рахунку PayPal Website Payment Pro Checkout!';
$_['text_edit']                     = 'Редагувати PayPal Pro';
$_['text_pp_pro']					= '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			= 'Авторизація';
$_['text_sale']						= 'Продаж';

// Entry
$_['entry_username']				= 'API ім\'я користувача';
$_['entry_password']				= 'API пароль';
$_['entry_signature']				= 'API підпис';
$_['entry_test']					= 'Тестовий режим';
$_['entry_transaction']				= 'Метод транзакції:';
$_['entry_total']					= 'Всього';
$_['entry_order_status']			= 'Статус замовлення';
$_['entry_geo_zone']				= 'Регіон';
$_['entry_status']					= 'Статус';
$_['entry_sort_order']				= 'Порядок сортування';

// Help
$_['help_test']						= 'Використовувати діюче або тестове (пісочниці) шлюзового сервера до процесу угоди?';
$_['help_total']					= 'Цей метод стане доступним, коли загальна сума досягне суми мінімального замовлення';

// Error
$_['error_permission']				= 'Попередження: Немає дозволу змінювати оплати PayPal Website Payment Pro Checkout!';
$_['error_username']				= 'API ім\'я користувача потрібно!';
$_['error_password']				= 'Потрібно пароль API!';
$_['error_signature']				= 'API підпис обов\'язково!';