<?php
$_['heading_title'] = 'Веб служба Secure Trading';

$_['tab_settings'] = 'Налаштування';
$_['tab_myst'] = 'MyST';

$_['text_securetrading_ws'] = '<img src="view/image/payment/secure_trading.png" alt="Secure Trading" title="Secure Trading" style="border: 1px solid #EEEEEE;" />';
$_['text_payment'] = 'Платіж';
$_['text_enabled'] = 'Увімкнено';
$_['text_disabled'] = 'Вимкнено';
$_['text_yes'] = 'Так';
$_['text_no'] = 'Ні';
$_['text_all_geo_zones'] = 'Всі регіони';
$_['text_process_immediately'] = 'Опрацювати зараз';
$_['text_wait_x_days'] = 'Чекайте %d днів';
$_['text_wait'] = 'Зачекайте, будь ласка.';
$_['text_authorisation_reversed'] = 'Авторизацію успішно скасовано';
$_['text_refund_issued'] = 'Повернення коштів пройшло успішно';
$_['text_success'] = 'Успіх: Змінено безпечний торговий модуль!';
$_['text_pending_settlement'] = 'Pending Settlement';
$_['text_manual_settlement'] = 'Manual Settlement';
$_['text_suspended'] = 'Призупинено';
$_['text_cancelled'] = 'Скасований';
$_['text_settling'] = 'Settling';
$_['text_settled'] = 'Settled';
$_['text_no_transactions'] = 'Нема транзакцій для показу';
$_['text_ok'] = 'Ok';
$_['text_denied'] = 'Заборонено';
$_['text_transactions'] = 'Транзакції';
$_['text_pending_settlement'] = 'Pending Settlement';
$_['text_pending_settlement_manually_overriden'] = 'Pending Settlement, manually overriden';
$_['text_pending_suspended'] = 'Призупинено';
$_['text_pending_settled'] = 'Settled';

$_['entry_site_reference'] = 'Посилання сайту';
$_['entry_username'] = 'Логін';
$_['entry_password'] = 'Пароль';
$_['entry_csv_username'] = 'CSV логін';
$_['entry_csv_password'] = 'CSV пароль';
$_['entry_3d_secure'] = 'Використовуйте 3D Secure';
$_['entry_cards_accepted'] = 'Приймаються картки';
$_['entry_order_status'] = 'Стан замовлення';
$_['entry_failed_order_status'] = 'Статус невдалого замовлення';
$_['entry_declined_order_status'] = 'Статус замовлення, що було відмінено';
$_['entry_refunded_order_status'] = 'Статус замовлення за яким повернуто гроші';
$_['entry_authorisation_reversed_order_status'] = 'Authorisation reversed order status';
$_['entry_settle_status'] = 'Settlement status';
$_['entry_settle_due_date'] = 'Settlement due date';
$_['entry_geo_zone'] = 'Регіон';
$_['entry_sort_order'] = 'Порядок сортування';
$_['entry_status'] = 'Статус';
$_['entry_total'] = 'Разом';
$_['entry_reverse_authorisation'] = 'Reverse Authorisation:';
$_['entry_refunded'] = 'Повернуті:';
$_['entry_refund'] = 'Issue refund (%s):';
$_['entry_currency'] = 'Валюта';
$_['entry_status_code'] = 'Код помилки';
$_['entry_payment_type'] = 'Тип платежу';
$_['entry_request'] = 'Запит';
$_['entry_settle_status'] = 'Settle Status';
$_['entry_date_from'] = 'Дата від';
$_['entry_date_to'] = 'Дата, до';
$_['entry_hour'] = 'Година';
$_['entry_minute'] = 'Хвилина';

$_['column_order_id'] = 'Ідентифікатор замовлення';
$_['column_transaction_reference'] = "Посилання на транзакцію";
$_['column_customer'] = "Покупець";
$_['column_total'] = "Разом";
$_['column_currency'] = "Валюта";
$_['column_settle_status'] = "Settle status";
$_['column_status'] = "Статус";
$_['column_type'] = "Тип";
$_['column_payment_type'] = "Тип платежу";

$_['error_permission'] = 'У вас немає прав для змінення цього модуля';
$_['error_site_reference'] = 'Необхідно ввести посилання на сайт';
$_['error_cards_accepted'] = 'Accepted cards is required';
$_['error_username'] = 'Потрібне ім\'я користувача';
$_['error_password'] = 'Потрібен пароль';
$_['error_connection'] = 'Could not connect to Secure Trading';
$_['error_data_missing'] = 'Дані відсутні';

$_['help_refund'] = 'Please include the decimal point and the decimal part of the amount';
$_['help_csv_username'] = 'Username of the Transaction Download service';
$_['help_csv_password'] = 'Password of the Transaction Download service';
$_['help_total'] = 'Сума замовлення повинна бути більше, перш ніж цей метод оплати стане активним';

$_['button_reverse_authorisation'] = 'Reverse Authorisation';
$_['button_refund'] = 'Повернення коштів';
$_['button_show'] = 'Показати';
$_['button_download'] = 'Завантажити';

// Order page - payment tab
$_['text_payment_info'] = 'Інформація про оплату';
$_['text_release_status'] = 'Платіж проведений';
$_['text_void_status'] = 'Reverse Authorisation';
$_['text_rebate_status'] = 'Платіж зі знижкою';
$_['text_order_ref'] = 'Посилання на замовлення';
$_['text_order_total'] = 'Всього авторизовано';
$_['text_total_released'] = 'Всього сплачено';
$_['text_transactions'] = 'Транзакції';
$_['text_column_amount'] = 'Кількість';
$_['text_column_type'] = 'Тип';
$_['text_column_created'] = 'Створено';
$_['text_release_ok'] = 'Проведення платежу було успішним';
$_['text_release_ok_order'] = 'Release was successful, order status updated to success - settled';
$_['text_rebate_ok'] = 'Знижка пройшла успішно';
$_['text_rebate_ok_order'] = 'Знижка пройшла успішно, статус замовлення для знижки оновлюється';
$_['text_void_ok'] = 'Анулювання було успішним, статус замовлення оновлено до анульованого';

$_['text_confirm_void'] = 'Ви дійсно бажаєте скасувати дозвіл?';
$_['text_confirm_release'] = 'Ви впевнені, що хочете провести платіж?';
$_['text_confirm_rebate'] = 'Ви впевнені, що хочете провести платіж зі знижкою?';

$_['btn_release'] = 'Реліз';
$_['btn_rebate'] = 'Знижка / повернення';
$_['btn_void'] = 'Зворотня авторизація';