<?php
// Heading
$_['heading_title']		 = 'LIQPAY';

// Text
$_['text_payment']		 = 'Платежі';
$_['text_success']		 = 'Успіх: Реквізити рахунку LIQPAY змінено!';
$_['text_edit']          = 'Редагувати LIQPAY';
$_['text_pay']			 = 'LIQPAY';
$_['text_card']			 = 'Платіжна картка';
$_['text_liqpay']		 = '<img src="view/image/payment/liqpay.png" alt="LIQPAY" title="LIQPAY" style="border: 1px solid #EEEEEE;" />';

// Entry
$_['entry_merchant']	 = 'Публічний ID';
$_['entry_signature']	 = 'Підпис';
$_['entry_type']		 = 'Тип';
$_['entry_total']		 = 'Всього';
$_['entry_order_status'] = 'Стан замовлення';
$_['entry_geo_zone']	 = 'Геозона';
$_['entry_status']		 = 'Статус';
$_['entry_sort_order']	 = 'Порядок сортування';

// Help
$_['help_total']		 = 'Цей метод стане доступним, коли загальна сума замовлення досягне встановленої суми.';

// Error
$_['error_permission']	 = 'Попередження: Вас немає дозволу на зміни оплати LIQPAY!';
$_['error_merchant']	 = 'Необхідний публічний ID!';
$_['error_signature']	 = 'Підпис обов\'язковий!';