<?php
// Heading
$_['heading_title'] 				= 'Новый список Amazon';
$_['text_title_advanced'] 			= 'Розширений список';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon ЄС';

// Buttons
$_['button_new'] 					= 'Створити новий продукт';
$_['button_amazon_price'] 			= 'Отримати ціни з Amazon';
$_['button_list'] 					= 'Список на Amazon';
$_['button_remove_error'] 			= 'Видалити повідомлення про помилки';
$_['button_save_upload'] 			= 'Збереження та завантаження';
$_['button_browse'] 				= 'Огляд';
$_['button_saved_listings'] 		= 'Перегляд збережених списків';
$_['button_remove_links'] 			= 'Видалити посилання/зв\'язки';
$_['button_create_new_listing'] 	= 'Створити новий список';

// Help
$_['help_sku'] 						= "Унікальний ID товару, який присвоєний продавцем";
$_['help_restock_date'] 			= "Дата, коли Ви зможете доставити будь-які поставлені позиції покупцеві. Вкажіть дату не пізніше 30 днів з дати публікації, інакше замовлення, отримані автоматично, можуть бути скасовані.";
$_['help_sale_price'] 				= "Ціна повинна мати дату початку і закінчення";

//Text
$_['text_products_sent'] 			= 'Товари були відправлені в обробку';
$_['button_view_on_amazon'] 		= 'Дивитись на Amazon';
$_['text_list'] 					= 'Список на Amazon';
$_['text_new'] 						= 'Новий';
$_['text_used_like_new'] 			= 'Вживаний - як новий';
$_['text_used_very_good'] 			= 'Вживаний - стан дуже хороший';
$_['text_used_good'] 				= 'Вживаний - стан хороший';
$_['text_used_acceptable'] 			= 'Вживаний - стан прийнятний';
$_['text_collectible_like_new'] 	= 'Предмет колекції - як новий';
$_['text_collectible_very_good'] 	= 'Предмет колекції - дуже хороший стан';
$_['text_collectible_good'] 		= 'Предмет колекції - хороший стан';
$_['text_collectible_acceptable'] 	= 'Предмет колекції - прийнятний стан';
$_['text_refurbished'] 				= 'Відновлений';
$_['text_product_not_sent'] 		= 'Продукт не був надісланий до Amazon. Причина:%s';
$_['text_not_in_catalog'] 			= "Або, якщо це не в каталозі&nbsp;&nbsp;&nbsp;";
$_['text_placeholder_search'] 		= 'Введіть назву продукту, UPC, EAN, ISBN або ASIN';
$_['text_placeholder_condition'] 	= 'Це поле використовується для опису стану Вашого товару.';
$_['text_characters'] 				= 'символи';
$_['text_uploaded'] 				= 'Збережені списки вивантажені!';
$_['text_saved_local'] 				= 'Список збережений, але ще не вивантажений';
$_['text_product_sent'] 			= 'Товар було успішно надіслано до Amazon.';
$_['text_links_removed'] 			= 'Зв\'язки товару з Amazon видалені';
$_['text_product_links'] 			= 'Зв\'язки товару';
$_['text_has_saved_listings'] 		= 'Цей товар має один або декілька збережених списків, що не вивантажені';
$_['text_edit_heading'] 			= 'Редагувати список';
$_['text_germany'] 					= 'Німеччина';
$_['text_france'] 					= 'Франція';
$_['text_italy'] 					= 'Італія';
$_['text_spain'] 					= 'Іспанія';
$_['text_united_kingdom'] 			= 'Великобританія';

// Columns
$_['column_image'] 					= 'Зображення';
$_['column_asin'] 					= 'ASIN';
$_['column_price'] 					= 'Ціна';
$_['column_action'] 				= 'Дія';
$_['column_name'] 					= 'Назва товару';
$_['column_model'] 					= 'Модель';
$_['column_combination'] 			= 'Комбінація';
$_['column_sku'] 					= 'SKU';
$_['column_amazon_sku'] 			= 'Елемент SKU, Amazon';

// Entry
$_['entry_sku'] 					= 'SKU';
$_['entry_condition'] 				= 'Стан';
$_['entry_condition_note'] 			= 'Примітка про стан';
$_['entry_price'] 					= 'Ціна';
$_['entry_sale_price'] 				= 'Відпускна ціна';
$_['entry_sale_date'] 				= 'Період продажу';
$_['entry_quantity'] 				= 'Кількість';
$_['entry_start_selling'] 			= 'Доступно з дати';
$_['entry_restock_date'] 			= 'Дата поповнення';
$_['entry_country_of_origin'] 		= 'Країна виробник';
$_['entry_release_date'] 			= 'Дата випуску';
$_['entry_from'] 					= 'Дата з';
$_['entry_to'] 						= 'Дата до';
$_['entry_product'] 				= 'Список товарів';
$_['entry_category'] 				= 'Категорія Amazon';
$_['entry_browse_node'] 			= 'Виберіть вузол перегляду';
$_['entry_marketplace'] 			= 'Ринок';

//Tabs
$_['tab_main'] 						= 'Головна';
$_['tab_required'] 					= 'Обов\'язкова інформація';
$_['tab_additional'] 				= 'Додаткові опції';

// Error
$_['error_required'] 				= 'Це поле є обов\'язковим!';
$_['error_not_saved'] 				= 'Список не був збережений. Перевірте, чи ви заповнили всі поля';
$_['error_char_limit'] 				= 'Занадто багато символів';
$_['error_length'] 					= 'Мінімальна довжина';
$_['error_upload_failed'] 			= 'Не вдалося завантажити продукт SKU: "%s". Причина: "%s" Процес скасовано.';
$_['error_load_nodes'] 				= 'Неможливо завантажити огляд вузлів';
$_['error_connecting'] 				= 'Проблема підключення до API. Перевірте параметри розширення Amazon Pro OpenBay. Якщо проблема не усувається, зверніться в службу підтримки.';
$_['error_text_missing'] 			= 'Ви повинні ввести деталі пошуку';
$_['error_missing_asin'] 			= 'ASIN відсутній';
$_['error_marketplace_missing'] 	= 'Будь ласка, виберіть ринок';
$_['error_condition_missing'] 		= "Будь ласка, виберіть стан";
$_['error_amazon_price'] 			= 'Не вдалося отримати ціну від Amazon';
$_['error_stock'] 					= 'Ви не можете перерахувати пункт з менш ніж 1 шт. в наявності';
$_['error_sku'] 					= 'Ви повинні ввести SKU для елемента';
$_['error_price'] 					= 'Ви повинні ввести ціну за пункт';
$_['error_sending_products'] 		= 'Не вдалося відправити продукти для включення в список. Зверніться в службу підтримки';
$_['error_no_products_selected'] 	= 'Жоден продукт не був обраний для включення';
$_['error_not_searched'] 			= 'Пошукайте потрібні вам товари, перш ніж намагатися додати. Елементи повинні відповідати каталогом Amazon';