<?php
// Heading
$_['heading_title']         		= 'Amazon США';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_dashboard']				= 'Панель Amazon США';

// Text
$_['text_heading_settings'] 		= 'Налаштування';
$_['text_heading_account'] 			= 'Змінити тарифний план';
$_['text_heading_links'] 			= 'Посилання на елементи';
$_['text_heading_register'] 		= 'Реєстрація';
$_['text_heading_bulk_listing'] 	= 'Основной список';
$_['text_heading_stock_updates'] 	= 'Оновлення магазину';
$_['text_heading_saved_listings'] 	= 'Збережені списки';
$_['text_heading_bulk_linking'] 	= 'Основний зв\'язок';