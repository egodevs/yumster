<?php
// Heading
$_['heading_title']        				= 'Налаштування магазину';
$_['text_openbay']						= 'OpenBay Pro';
$_['text_amazon']						= 'Amazon ЄС';

// Text
$_['text_api_status']               	= 'Стан з\'єднання API';
$_['text_api_ok']                   	= 'З\'єднання встановлено, авторизація виконана';
$_['text_api_auth_error']           	= 'З\'єднання встановлено, помилка авторизації';
$_['text_api_error']                	= 'Помилка підключення';
$_['text_order_statuses']           	= 'Статуси замовлення';
$_['text_unshipped']                	= 'Невідправлений';
$_['text_partially_shipped']        	= 'Частково відправлений';
$_['text_shipped']                  	= 'Доставлений';
$_['text_canceled']                 	= 'Скасований';
$_['text_other']                    	= 'Інші';
$_['text_marketplaces']             	= 'Магазини';
$_['text_markets']                  	= 'Виберіть магазини, з яких Ви хочете імпортувати Ваші замовлення';
$_['text_de']                       	= 'Німеччина';
$_['text_fr']                       	= 'Франція';
$_['text_it']                       	= 'Італія';
$_['text_es']                       	= 'Іспанія';
$_['text_uk']                       	= 'Великобританія';
$_['text_setttings_updated']        	= 'Налаштування були успішно оновлені.';
$_['text_new'] 							= 'Новий';
$_['text_used_like_new'] 				= 'Вживаний - як новий';
$_['text_used_very_good'] 				= 'Вживаний - дуже хороший стан';
$_['text_used_good'] 					= 'Вживаний - хороший стан';
$_['text_used_acceptable'] 				= 'Вживаний - прийнятний стан';
$_['text_collectible_like_new'] 		= 'Предмет колекції - як новий';
$_['text_collectible_very_good'] 		= 'Предмет колекції - дуже хороший стан';
$_['text_collectible_good'] 			= 'Предмет колекції - хороший стан';
$_['text_collectible_acceptable'] 		= 'Предмет колекції - прийнятний стан';
$_['text_refurbished'] 					= 'Відновлений';

// Error
$_['error_permission']         			= 'Ви не маєте повноважень для доступу до цього модулю';

// Entry
$_['entry_status']                 		= 'Статус';
$_['entry_token']                    	= 'Маркер';
$_['entry_string1']              		= 'Рядок шифрування 1';
$_['entry_string2']              		= 'Рядок шифрування 2';
$_['entry_import_tax']               	= 'Податок на імпортні товари';
$_['entry_customer_group']           	= 'Групи клієнтів';
$_['entry_tax_percentage']           	= 'Змінити ціну';
$_['entry_default_condition']        	= 'Стан продукту за замовчуванням';
$_['entry_marketplace_default']			= 'Ринок за замовчуванням';
$_['entry_notify_admin']             	= 'Повідомляти адміністратора про нове замовлення';
$_['entry_default_shipping']         	= 'Доставка за замовчуванням';

// Tabs
$_['tab_settings']            			= 'Деталі API';
$_['tab_listing']                  		= 'Оголошення';
$_['tab_orders']                   		= 'Замовлення';

// Help
$_['help_import_tax']          			= 'Використовувати, якщо Amazon не надає дані про податки';
$_['help_customer_group']      			= 'Вибрати групу користувачів для підтвердження відправки замовлень';
$_['help_default_shipping']    			= 'Використовується в якості попередньо обраного варіанту в порядку масового оновлення';
$_['help_entry_marketplace_default']	= 'Магазин за замовчуванням для пошуку і вибору товарів';
$_['help_tax_percentage']           	= 'Додавати відсотки у вартість товару за замовчуванням';