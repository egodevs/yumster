<?php
// Heading
$_['heading_title'] 				= 'Збережені Оголошення';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon США';

// Text
$_['text_description']              = 'Це збережений список товару, який готовий до завантаження на Amazon.';
$_['text_uploaded_alert']           = 'Збережені оголошення завантажені!';
$_['text_delete_confirm']           = 'Ви впевнені?';
$_['text_complete']           		= 'Завантажені оголошення';

// Column
$_['column_name']              		= 'Ім`я';
$_['column_model']             		= 'Модель';
$_['column_sku']               		= 'SKU';
$_['column_amazon_sku']        		= 'Елемент SKU, Amazon';
$_['column_action']           		= 'Дія';