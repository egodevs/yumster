<?php
// Heading
$_['heading_title']                     = 'Профіль';
$_['text_openbay']                      = 'OpenBay Pro';
$_['text_ebay']                         = 'eBay';

//Tabs
$_['tab_returns']          				= 'Повернення';
$_['tab_template']         				= 'Шаблон';
$_['tab_gallery']          				= 'Галерея';
$_['tab_settings']         				= 'Налаштування';

//Shipping Profile
$_['text_shipping_dispatch_country']    = 'Доставка з країни';
$_['text_shipping_postcode']            = 'Розташування поштовий/Zip код';
$_['text_shipping_location']            = 'Місто або держава розташування';
$_['text_shipping_despatch']            = 'Час відправки';
$_['text_shipping_despatch_help']       = 'Це Максимальна кількість днів, ви будете приймати для надсилання елементу';
$_['text_shipping_nat']                 = 'Державні транспортні послуги';
$_['text_shipping_intnat']              = 'Міжнародні транспортні послуги';
$_['text_shipping_first']               = 'Перший елемент';
$_['text_shipping_add']                 = 'Додаткові елементи';
$_['text_shipping_service']             = 'Служба';
$_['text_shipping_in_desc']             = 'Інформація про вантаж в описі';
$_['text_shipping_getitfast']           = 'Отримати якомога швидше!';
$_['text_shipping_zones']               = 'Відправка на території';
$_['text_shipping_worldwide']           = 'По всьому світу';
$_['text_shipping_type_nat']           	= 'Відправка державними службами';
$_['text_shipping_type_int']           	= 'Міжнародний тип доставки';
$_['text_shipping_flat']           		= 'Фіксована ставка';
$_['text_shipping_calculated']          = 'Розраховується';
$_['text_shipping_freight']          	= 'Вантажний';
$_['text_shipping_handling']          	= 'Оплата за відвантаження';
$_['text_shipping_cod']           		= 'Накладеним платежем';
$_['text_shipping_handling_nat']    	= 'Оплата за обробку замовлення (держ. Податок)';
$_['entry_shipping_handling_int']    	= 'Оплата за обробку замовлення (міжнар. Податок)';

//Returns profile
$_['text_returns_accept']       		= 'Повернено вірно';
$_['text_returns_inst']         		= 'Політика повернення';
$_['text_returns_days']         		= 'Дні повернення';
$_['text_returns_days10']       		= '10 днів';
$_['text_returns_days14']       		= '14 днів';
$_['text_returns_days30']       		= '30 днів';
$_['text_returns_days60']       		= '60 днів';
$_['text_returns_type']         		= 'Тип повернення';
$_['text_returns_type_money']   		= 'Повернення грошей';
$_['text_returns_type_exch']    		= 'Повернення грошей або обмін';
$_['text_returns_costs']        		= 'Повернення витрат з доставки';
$_['text_returns_costs_b']      		= 'Покупець платить';
$_['text_returns_costs_s']      		= 'Продавець платить';
$_['text_returns_restock']      		= 'Плата за поповнення';

//Template profile
$_['text_template_choose']      		= 'Стандартний шаблон';
$_['text_template_choose_help'] 		= 'Стандартний шаблон буде автоматично завантажений при виборі списку заощадити час';
$_['text_image_gallery']        		= 'Розмір зображення в галереї';
$_['text_image_gallery_help']   		= 'Розмір пікселів галереї зображень, які додаються до шаблону.';
$_['text_image_thumb']          		= 'Розмір ескізу зображення';
$_['text_image_thumb_help']     		= 'Розмір пікселів ескізів зображень, які додаються до шаблону.';
$_['text_image_super']          		= 'Дуже великий розмір';
$_['text_image_gallery_plus']   		= 'Галерея плюс';
$_['text_image_all_ebay']       		= 'Додати всі зображення на eBay';
$_['text_image_all_template']   		= 'Додайте всі зображення до шаблону';
$_['text_image_exclude_default']		= 'Виключити зображення за замовчуванням';
$_['text_image_exclude_default_help']	= 'Тільки для основних функцій! Не міститиме зображення продукту за замовчуванням у списку теми зображення';
$_['text_confirm_delete']       		= 'Ви впевнені, що бажаєте видалити профіль?';
$_['text_width']      					= 'Ширина';
$_['text_height']      					= 'Висота';
$_['text_px']      						= 'пікс.';

//General profile
$_['text_general_private']      		= 'Перелік елементів списку як на приватному аукціоні';
$_['text_general_price']        		= 'Зміна ціни в %';
$_['text_general_price_help']   		= '0 за промовчанням, зменшить -10 на 10%, 10 збільшить на 10% (використовується тільки на масовому списку)';

//General profile options
$_['text_profile_name']         		= 'Ім`я';
$_['text_profile_default']      		= 'За замовчуванням';
$_['text_profile_type']         		= 'Тип';
$_['text_profile_desc']         		= 'Опис';
$_['text_profile_action']       		= 'Дія';

// Profile types
$_['text_type_shipping']       			= 'Доставка';
$_['text_type_returns']       			= 'Повернення';
$_['text_type_template']       			= 'Галерея шаблонів';
$_['text_type_general']       			= 'Загальні параметри';

//Success messages
$_['text_added']                		= 'Додано новий профіль';
$_['text_updated']              		= 'Профіль було оновлено';

//Errors
$_['error_permission']        			= 'Ви не маєте дозволу для редагування профілів';
$_['error_name']           				= 'Слід ввести ім\'я профілю';
$_['error_no_template']          		= 'Ідентифікатора шаблону не існує';
$_['error_missing_settings'] 			= 'Не можна додавати, редагувати або видаляти профілі поки ви не синхронізуєте настройки eBay';