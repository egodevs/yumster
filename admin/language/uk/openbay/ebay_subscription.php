<?php
// Heading
$_['heading_title']             = 'Підписка';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_ebay']                 = 'eBay';

// Buttons
$_['button_plan_change']  		= 'Зміна плану';

// Columns
$_['column_plan']  				= 'Назва плану';
$_['column_call_limit']  		= 'Обмеження дзвінків';
$_['column_price']  			= 'Ціна (за / міс.)';
$_['column_description']  		= 'Опис';
$_['column_current']  			= 'Поточний план';

// Text
$_['text_subscription_current'] = 'Поточний план';
$_['text_subscription_avail']   = 'Доступні плани';
$_['text_subscription_avail1']  = 'Змінення планів буде негайним і невикористані дзвінки не будуть зараховані.';
$_['text_subscription_avail2']  = 'Щоб повернутися до основного плану будь ласка скасуйте вашу активну підписку в PayPal.';
$_['text_ajax_acc_load_plan']   = 'PayPal код підписки: ';
$_['text_ajax_acc_load_plan2']  = ', ви повинні скасувати всі інші підписки від нас';
$_['text_load_my_plan']         = 'Завантаження вашого плану';
$_['text_load_plans']           = 'Завантаження доступних планів';

// Errors
$_['error_ajax_load']      		= 'На жаль не вдалося отримати відповідь. Спробуйте пізніше.';