<?php
// Heading
$_['heading_title'] 				= 'Збережені списки';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon ЄС';

// Text
$_['text_description']              = 'Це список товару, який збережений і готовий до завантаження на Amazon.';
$_['text_uploaded_alert']           = 'Збережений список(ки) завантаження!';
$_['text_delete_confirm']           = 'Ви впевнені?';
$_['text_complete']           		= 'Списки завантажені';

// Column
$_['column_name']              		= 'Ім`я';
$_['column_model']             		= 'Модель';
$_['column_sku']               		= 'SKU';
$_['column_amazon_sku']        		= 'Елемент SKU, Amazon';
$_['column_action']           		= 'Дія';