<?php
// Heading
$_['heading_title']					= 'Переглянути список eBay';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_ebay']						= 'eBay';

// Text
$_['text_revise']               	= 'Переглянути перелік';
$_['text_loading']                  = 'Отримання інформації з eBay про товар';
$_['text_error_loading']            = 'Сталася помилка при отриманні інформації від eBay';
$_['text_saved']                    = 'Список був збережений';
$_['text_alert_removed']            = 'Продукт не був доданий';
$_['text_alert_ended']              = 'Завершено на eBay';

// Buttons
$_['button_view']					= 'Переглянути список';
$_['button_remove']					= 'Видалити посилання';
$_['button_end']                    = 'Кінець списку';
$_['button_retry']					= 'Повторити';

// Entry
$_['entry_title']					= 'Заголовок';
$_['entry_price']					= 'Ціна продажу (включаючи податок)';
$_['entry_stock_store']				= 'Місцевий магазин';
$_['entry_stock_listed']			= 'Магазин eBay';
$_['entry_stock_reserve']			= 'Рівень запасів';
$_['entry_stock_matrix_active']		= 'Матриця магазину (активна)';
$_['entry_stock_matrix_inactive']	= 'Матриця магазину (неактивна)';

// Column
$_['column_sku']					= 'Код var / артикул';
$_['column_stock_listed']			= 'Внесено в список';
$_['column_stock_reserve']			= 'Резерв';
$_['column_stock_total']			= 'В наявності';
$_['column_price']					= 'Ціна';
$_['column_status']					= 'Активний';
$_['column_add']					= 'додати';
$_['column_combination']			= 'Комбінація';

// Help
$_['help_stock_store']				= 'Це рівень запасів на OpenCart';
$_['help_stock_listed']				= 'Це поточний рівень запасу на eBay';
$_['help_stock_reserve']			= 'Це максимальний рівень запасу на eBay (0 = немає резервної межі)';

// Error
$_['error_ended']					= 'Пов\'язаний список закінчився, ви не зможете редагувати його. Необхідноі видалити посилання.';
$_['error_reserve']					= 'Ви не можете встановити резерв вищий, ніж є в наявності на складі';