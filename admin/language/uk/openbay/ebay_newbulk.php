<?php
//Heading
$_['text_page_title']               = 'Масова реклама';
$_['text_ebay']               		= 'eBay';
$_['text_openbay']               	= 'Для OpenBay';

// Buttons
$_['text_none']                     = 'Без звуку';
$_['text_preview']                  = 'Переглянути';
$_['text_add']                      = 'додати';
$_['text_preview_all']              = 'Перевірити всі';
$_['text_submit']                   = 'Відправити';
$_['text_features']                 = 'Можливості';
$_['text_catalog']                  = 'Виберіть каталог';
$_['text_catalog_search']           = 'Пошук по каталогу';
$_['text_search_term']           	= 'Пошук терміну';
$_['text_close']           			= 'Закрити';

//Form options / text
$_['text_pixels']                   = 'пікселів';
$_['text_other']                    = 'Інші';

//Profile names
$_['text_profile']            		= 'Профіль';
$_['text_profile_theme']            = 'Тема профілю';
$_['text_profile_shipping']         = 'Профіль доставка';
$_['text_profile_returns']          = 'Повернути профіль';
$_['text_profile_generic']          = 'Створити профіль';

//Text
$_['text_title']                    = 'Заголовок';
$_['text_price']                    = 'Ціна';
$_['text_stock']                    = 'Склад';
$_['text_search']                   = 'Шукати';
$_['text_loading']                  = 'завантаження даних';
$_['text_preparing0']               = 'Підготовка';
$_['text_preparing1']               = 'або';
$_['text_preparing2']               = 'елементи';
$_['entry_condition']                = 'Стан';
$_['text_duration']                 = 'Тривалість';
$_['text_category']                 = 'Категорія';
$_['text_exists']                   = 'Деякі пункти вже перераховані на eBay, тому були видалені';
$_['text_error_count']              = 'Ви вибрали %s предметів, це може зайняти деякий час, щоб обробити ваші дані';
$_['text_verifying']                = 'Перевірка елементів';
$_['text_processing']               = 'Обробка <span id="activeItems"></span> елементів';
$_['text_listed']                   = 'Елемент у списку! ID: ';
$_['text_ajax_confirm_listing']     = 'Чи справді внести ці елементи до списку?';
$_['text_bulk_plan_error']          = 'Поточний план не дозволяє для групового завантаження, оновити план <a href="%s"> тут</a>';
$_['text_item_limit']               = 'Ви не можете переглянути ці елементи тому що, ви перевищуєте обмеження вашого плану, оновити план <a href="%s"> тут</a>';
$_['text_search_text']              = 'Введіть текст пошуку';
$_['text_catalog_no_products']      = 'Каталог елементів не знайдено';
$_['text_search_failed']            = 'Не вдалося виконати пошук';
$_['text_esc_key']                  = 'Спливаюче вікно сховано, але завантаження може ще не закінчитись ';
$_['text_loading_categories']       = 'Категорії завантаження ';
$_['text_loading_condition']        = 'Умови завантаження продукту ';
$_['text_loading_duration']         = 'Завантаження списку тривалості';
$_['text_total_fee']         		= 'Загальна сума винагороди';
$_['text_category_choose']         	= 'Знайти категорію';
$_['text_suggested']         		= 'Пропоновані категорії';

//Errors
$_['text_error_ship_profile']       = 'Ви повинні мати на профіль доставки за замовчуванням, налаштування';
$_['text_error_generic_profile']    = 'Ви повинні мати на профіль за замовчуванням загальний, налаштування';
$_['text_error_return_profile']     = 'Ви повинні повернути налаштунки профілю за замовчуванням';
$_['text_error_theme_profile']      = 'Ви повинні мати профіль за замовчуванням, тема створена';
$_['text_error_variants']           = 'Елементи з варіаціями не можуть бути перераховані разом і не були вибрані';
$_['text_error_stock']              = 'Деяких елементів немає в наявності тому були видалені';
$_['text_error_no_product']         = 'Немає товарів, що підходять, тому вибрано використання функції групового передавання';
$_['text_error_reverify']           = 'Сталася помилка, ви повинні змінити і повторно підтвердити елементи';
$_['error_missing_settings']   = 'Вам не вдасця здійснити групове об’єднання товарів списку поки ви не синхронізуєте налаштункики eBay';
$_['text_error_no_selection']   	= 'Слід вибрати принаймні 1 елемента до списку';