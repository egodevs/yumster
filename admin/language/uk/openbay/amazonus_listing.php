<?php
// Heading
$_['heading_title'] 				= 'Новий список Amazon';
$_['text_title_advanced'] 			= 'Розширений список';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon США';

// Buttons
$_['button_new'] 					= 'Створити новий продукт';
$_['button_amazon_price'] 			= 'Отримати ціни з Amazon';
$_['button_list'] 					= 'Список на Amazon';
$_['button_remove_error'] 			= 'Видалити повідомлення про помилки';
$_['button_save_upload'] 			= 'Збереження та завантаження';
$_['button_browse'] 				= 'Огляд';
$_['button_saved_listings'] 		= 'Перегляд збережених списків';
$_['button_remove_links'] 			= "Видалити посилання/зв'язки";
$_['button_create_new_listing'] 	= "Створити новий список";

// Help
$_['help_sku'] 						= "Унікальний ID товару, присвоєний продавцем";
$_['help_restock_date'] 			= "Дата, коли Ви зможете доставити будь які недоставлені позиції покупцеві. Вкажіть дату не пізніше 30 днів з дати публікації, інакше замовлення, отримані автоматично, можуть бути скасовані.";
$_['help_sale_price'] 				= "Ціна повинна мати дату початку і закінчення";

//Text
$_['text_products_sent'] 			= 'Товари були відправлені в обробку';
$_['button_view_on_amazon'] 		= 'Дивитись на Amazon';
$_['text_list'] 					= 'Список на Amazon';
$_['text_new'] 						= 'Новий';
$_['text_used_like_new'] 			= 'Вживанийі - як новий';
$_['text_used_very_good'] 			= 'Вживаний - дуже хороший стан';
$_['text_used_good'] 				= 'Вживаний - хороший стан';
$_['text_used_acceptable'] 			= 'Вживаний - прийнятний стан';
$_['text_collectible_like_new'] 	= 'Предмет колекції - як новий';
$_['text_collectible_very_good'] 	= 'Предмет колекції - дуже хороший стан';
$_['text_collectible_good'] 		= 'Предмет колекції - хороший стан';
$_['text_collectible_acceptable'] 	= 'Предмет колекції - прийнятний стан';
$_['text_refurbished'] 				= 'Відновлений';
$_['text_product_not_sent'] 		= 'Продукт не був надісланий на Amazon. Причина: %s';
$_['text_not_in_catalog'] 			= 'Або, якщо не в каталозі & nbsp; & nbsp; & nbsp;';
$_['text_placeholder_search'] 		= 'Введіть назву продукту, UPC, EAN, ISBN або ASIN';
$_['text_placeholder_condition'] 	= 'Це поле використовується для опису стану Вашого товару.';
$_['text_characters'] 				= 'Символи';
$_['text_uploaded'] 				= 'Збережений список (ки) завантажено!';
$_['text_saved_local'] 				= 'Список збережений, але ще не завантажений';
$_['text_product_sent'] 			= 'Товар був успішно надісланий на Amazon.';
$_['text_links_removed'] 			= 'Зв\'язки товару з Amazon видалені';
$_['text_product_links'] 			= 'Посилання на товар';
$_['text_has_saved_listings'] 		= 'Цей товар має один або декілька збережених списків, які не відвантажені';
$_['text_edit_heading'] 			= 'Редагувати список';

// Columns
$_['column_image'] 					= 'Зображення';
$_['column_asin'] 					= 'ASIN (Стандартний ідентифікаційний номер Amazon)';
$_['column_price'] 					= 'Ціна';
$_['column_action'] 				= 'Дія';
$_['column_name'] 					= 'Назва товару';
$_['column_model'] 					= 'Модель';
$_['column_combination'] 			= 'Комбінація';
$_['column_sku'] 					= 'SKU';
$_['column_amazon_sku'] 			= 'Елемент SKU, Amazon';

// Entry
$_['entry_sku'] 					= 'SKU';
$_['entry_condition'] 				= 'Стан';
$_['entry_condition_note'] 			= 'Примітка про стан';
$_['entry_price'] 					= 'Ціна';
$_['entry_sale_price'] 				= 'Ціна продажу';
$_['entry_sale_date'] 				= 'Період продажу';
$_['entry_quantity'] 				= 'Кількість';
$_['entry_start_selling'] 			= 'Доступно з дати';
$_['entry_restock_date'] 			= 'Продовжити дату';
$_['entry_country_of_origin'] 		= 'Країна-виробник';
$_['entry_release_date'] 			= 'Дата випуску';
$_['entry_from'] 					= 'Дата з';
$_['entry_to'] 						= 'Дата, до';
$_['entry_product'] 				= 'Список товарів';
$_['entry_category'] 				= 'Категорія Amazon';

//Tabs
$_['tab_main'] 						= 'Головна';
$_['tab_required'] 					= 'Необхідна інформація';
$_['tab_additional'] 				= 'Додаткові опції';

//Errors
$_['error_text_missing'] 			= 'Ви повинні ввести деталі пошуку';
$_['error_data_missing'] 			= 'Відсутні необхідні дані';
$_['error_missing_asin'] 			= 'ASIN відсутній';
$_['error_marketplace_missing'] 	= 'Будь ласка, виберіть ринок';
$_['error_condition_missing'] 		= "Будь ласка, виберіть умови";
$_['error_fetch'] 					= 'Не вдалося одержати інформацію';
$_['error_amazonus_price'] 			= 'Не вдалося отримати ціну Amazon';
$_['error_stock'] 					= 'Ви можете перерахувати товар з менш ніж 1шт. на складі';
$_['error_sku'] 					= 'Ви повинні ввести SKU (артикул) для елемента';
$_['error_price'] 					= 'Вам необхідно ввести ціну для товара';
$_['error_connecting'] 				= 'Попередження: Не вдалося підключитися до API. Будь ласка, перевірте налаштування розширення OpenBay Pro Amazon. Якщо проблема повториться, зверніться в службу підтримки.';
$_['error_required'] 				= 'Це поле є обов\'язковим!';
$_['error_not_saved'] 				= 'Список не був збережений. Перевірте введені дані.';
$_['error_char_limit'] 				= 'Перевищена кількість символів.';
$_['error_length'] 					= 'Мінімальна довжина';
$_['error_upload_failed'] 			= 'Збій завантаження продукту з артикулом SKU: "%s". Причина: "%s" процес завантаження скасовано.';
$_['error_load_nodes'] 				= 'Не вдалося завантажити огляд вузлів';
$_['error_not_searched'] 			= 'Перш ніж намагатися додати товари, пошукайте потрібні вам позиції. Елементи повинні відповідати каталогам Amazon';