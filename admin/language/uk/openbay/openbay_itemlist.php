<?php
// Heading
$_['heading_title'] 					= 'Управління оголошеннями';

// Text
$_['text_markets']                  	= 'Ринки';
$_['text_openbay']                  	= 'OpenBay Pro';
$_['text_ebay'] 						= 'eBay';
$_['text_amazon'] 						= 'Amazon ЄС';
$_['text_amazonus'] 					= 'Amazon США';
$_['text_etsy'] 						= 'Etsy';
$_['text_status_all'] 					= 'усі';
$_['text_status_ebay_active'] 			= 'eBay активний';
$_['text_status_ebay_inactive'] 		= 'eBay не активний';
$_['text_status_amazoneu_saved'] 		= 'Amazon ЄС збережений';
$_['text_status_amazoneu_processing'] 	= 'Amazon ЄС в обробці';
$_['text_status_amazoneu_active'] 		= 'Amazon ЄС активний';
$_['text_status_amazoneu_notlisted'] 	= 'Amazon ЄС не вказаний';
$_['text_status_amazoneu_failed'] 		= 'Amazon ЄС не вдалося';
$_['text_status_amazoneu_linked'] 		= 'Amazon ЄС пов’язано';
$_['text_status_amazoneu_notlinked'] 	= 'Amazon ЄС не вказаний';
$_['text_status_amazonus_saved'] 		= 'Amazon США збережено';
$_['text_status_amazonus_processing'] 	= 'Amazon США в обробці';
$_['text_status_amazonus_active'] 		= 'Amazon США активний';
$_['text_status_amazonus_notlisted'] 	= 'Amazon США не вказаний';
$_['text_status_amazonus_failed'] 		= 'Amazon США не вдалося';
$_['text_status_amazonus_linked'] 		= 'Amazon США пов\'язаний';
$_['text_status_amazonus_notlinked'] 	= 'Amazon США не вказаний';
$_['text_processing']       			= 'Обробляється';
$_['text_category_missing'] 			= 'Відсутня Категорія';
$_['text_variations'] 					= 'варіації';
$_['text_variations_stock'] 			= 'наявність';
$_['text_min']                      	= 'Мінімум';
$_['text_max']                      	= 'Максимум';
$_['text_option']                   	= 'Параметри';

// Entry
$_['entry_title'] 						= 'Заголовок';
$_['entry_model'] 						= 'Модель';
$_['entry_manufacturer'] 				= 'Виробник';
$_['entry_status'] 						= 'Стан';
$_['entry_status_marketplace'] 			= 'Стан торгівельного майданчику';
$_['entry_stock_range'] 				= 'Асортимент складу';
$_['entry_category'] 					= 'Категорія';
$_['entry_populated'] 					= 'Населений';
$_['entry_sku'] 						= 'SKU';
$_['entry_description'] 				= 'Опис';

// Button
$_['button_error_fix']              	= 'Виправлення помилок';
$_['button_amazon_eu_bulk']         	= 'Amazon ЄС масове завантаження';
$_['button_amazon_us_bulk']         	= 'Amazon США масове завантаження';
$_['button_ebay_bulk']              	= 'eBay масове завантаження';

// Error
$_['error_select_items']            	= 'Слід вибрати принаймні 1 елемент до масового списку';