<?php
// Heading
$_['heading_title']					= 'Посилання на елементи';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_amazon']					= 'Amazon ЄС';

// Text
$_['text_desc1']                    = 'Посилання на елементи дозволить забезпечити контроль під час лістингу Amazon.';
$_['text_desc2'] 					= 'Для кожної позиції, яка була оновлена ​​локально (у Вашому магазині OpenCart), буде оновлений Ваш список на Amazon';
$_['text_desc3']                    = 'Ви можете синхронізувати позиції вручну, ввівши артикул (Amazon SKU) і назву товару, або завантажити всі несинхронізовані продукти і потім ввести Amazon SKU. (Вивантаження товарів з OpenCart в Amazon буде автоматично додавати зв\'язки)';
$_['text_new_link']                 = 'Нова позиція';
$_['text_autocomplete_product']     = 'Товар (автозаповнення за іменем)';
$_['text_amazon_sku']               = 'Елемент SKU, Amazon';
$_['text_action']                   = 'Дія';
$_['text_linked_items']             = 'Зв\'язані елементи';
$_['text_unlinked_items']           = 'Непов\'язані об\'єкти';
$_['text_name']                     = 'Ім`я';
$_['text_model']                    = 'Модель';
$_['text_combination']              = 'Комбінація';
$_['text_sku']                      = 'SKU';
$_['text_amazon_sku']               = 'Елемент SKU, Amazon';

// Button
$_['button_load']                 	= 'Завантажити';

// Error
$_['error_empty_sku']        		= 'Amazon SKU не може бути пустим!';
$_['error_empty_name']       		= 'Назва товару не може бути пустою!';
$_['error_no_product_exists']       = 'Товар не існує. Будь ласка, використовуйте список автозавершення для вибору.';