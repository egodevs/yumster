<?php
// Heading
$_['heading_title']         		= 'Etsy';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_dashboard']				= 'панель керування Etsy';

// Messages
$_['text_success']         			= 'Ви зберегли свої зміни в додадок Etsye';
$_['text_heading_settings']         = 'Налаштування';
$_['text_heading_sync']             = 'Cинхронізувати';
$_['text_heading_register']         = 'Зареєструватись тут';
$_['text_heading_products']        	= 'Посилання на товар';
$_['text_heading_listings']        	= 'Etsy перелік';

// Errors
$_['error_generic_fail']			= 'Невідома помилка!';
$_['error_permission']				= 'Ви не маєте дозволу на Etsy настройки';