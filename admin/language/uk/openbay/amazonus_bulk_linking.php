<?php
// Heading
$_['heading_title'] 				= 'Основне з\'єднання';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon США';

// Button
$_['button_load'] 					= 'Завантажити';
$_['button_link'] 					= 'Посилання';

// Text
$_['text_local'] 					= 'Локально';
$_['text_load_listings'] 			= "Завантаження всіх Ваших списків з Amazon може зайняти деякий час (до двох годин в окремих випадках). Якщо Ви пов'язуєте позиції, кількість товару на Amazon буде оновлюватися з кількості товарів Вашого магазину.";
$_['text_report_requested'] 		= 'Відправлений запит списків на Amazon';
$_['text_report_request_failed'] 	= 'Не вдалося запросити списки';
$_['text_loading'] 					= 'Завантаження новин';

// Column
$_['column_asin'] 					= "ASIN";
$_['column_price'] 					= "Ціна";
$_['column_name'] 					= "Ім`я";
$_['column_sku'] 					= "SKU";
$_['column_quantity'] 				= "Кількість";
$_['column_combination'] 			= "Комбінація";

// Error
$_['error_bulk_link_permission'] 	= 'Групова синхронізація не допускається у Вашому тарифному плані. Змініть тариф';