<?php
// Heading
$_['heading_title'] 				= 'Сумарні посилання';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon ЄС';

// Button
$_['button_load'] 					= 'Завантажити';
$_['button_link'] 					= 'Посилання';

// Text
$_['text_local'] 					= 'Локально';
$_['text_load_listings'] 			= "Завантаження всіх Ваших списків з Amazon може заняти деякий час (до двух годин). Якщо Ви зв'язуєте позиції, кількість товара на Amazon буде обновлятись із кількості товарів Вашого магазина.";
$_['text_report_requested'] 		= 'Надісланий запит списків в Amazon';
$_['text_report_request_failed'] 	= 'Не вдалось запросити списки';
$_['text_loading'] 					= 'Завантаження новин';
$_['text_choose_marketplace'] 		= 'Виберіть ринок';
$_['text_uk'] 						= 'Великобританія';
$_['text_de'] 						= 'Німеччина';
$_['text_fr'] 						= 'Франція';
$_['text_it'] 						= 'Італія';
$_['text_es'] 						= 'Іспанія';

// Column
$_['column_asin'] 					= "ASIN (Стандартний ідентифікаційний номер Amazon)";
$_['column_price'] 					= "Ціна";
$_['column_name'] 					= "Назва";
$_['column_sku'] 					= "SKU";
$_['column_quantity'] 				= "Кількість";
$_['column_combination'] 			= "Комбінація";

// Error
$_['error_bulk_link_permission'] 	= 'Групова синхронізація не дозволяється у Вашому тарифному плані. Змінити тариф.';