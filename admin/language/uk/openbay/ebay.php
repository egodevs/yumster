<?php
// Heading
$_['heading_title']         		= 'eBay';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_dashboard']				= 'Панель управління eBay';

// Text
$_['text_success']         			= 'Ви зберегли свої зміни в додадок eBay';
$_['text_heading_settings']         = 'Налаштування';
$_['text_heading_sync']             = 'Cинхронізувати';
$_['text_heading_subscription']     = 'Змінити план';
$_['text_heading_usage']          	= 'Використання';
$_['text_heading_links']            = 'Посилання на елементи';
$_['text_heading_item_import']      = 'Імпортувати елементи';
$_['text_heading_order_import']     = 'Імпорт замовлень';
$_['text_heading_adds']             = 'Встановлені додатки';
$_['text_heading_summary']          = 'eBay резюме';
$_['text_heading_profile']          = 'Профілі';
$_['text_heading_template']         = 'Шаблони';
$_['text_heading_ebayacc']          = 'eBay аккаунт';
$_['text_heading_register']         = 'Зареєструватись тут';

// Error
$_['error_category_nosuggestions']  = 'Не вдалося завантажити запропоновані категорії';
$_['error_loading_catalog']         = 'Не вдалося знайти в каталозі eBay';
$_['error_generic_fail']         	= 'Невідома помилка!';