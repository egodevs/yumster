<?php
// Headings
$_['heading_title']      	= 'Створити новий список Etsy';
$_['text_title']      		= 'Etsy';
$_['text_openbay']      	= 'OpenBay Pro';

// Tabs
$_['tab_additional']      	= 'Додаткова інформація';

// Text
$_['text_option']      		= 'Виберіть параметр';
$_['text_category_selected']= 'Вибрані категорії';
$_['text_material_add']  	= 'Додати матеріал';
$_['text_material_remove']  = 'Видалити матеріал';
$_['text_tag_add']  		= 'Додати тег';
$_['text_tag_remove']  		= 'Видалити тег';
$_['text_created']  		= 'Ваш список був створений';
$_['text_listing_id']  		= 'Список ID';
$_['text_img_upload']  		= 'Завантаження зображення';
$_['text_img_upload_done']  = 'Завантажені зображення';

// Entry
$_['entry_title']      		= 'Назва продукту';
$_['entry_description']     = 'Опис';
$_['entry_price']      		= 'Ціна';
$_['entry_non_taxable']     = 'Не обкладається податком';
$_['entry_category']     	= 'Топ Категорія';
$_['entry_sub_category']    = 'Підкатегорія';
$_['entry_sub_sub_category']= 'Під-підкатегорія';
$_['entry_who_made']		= 'Хто зробив це?';
$_['entry_when_made']		= 'Коли це було зроблено?';
$_['entry_recipient']		= 'Для кого це?';
$_['entry_occasion']		= 'Що це за подія?';
$_['entry_is_supply']		= 'Це поставка?';
$_['entry_state']      		= 'Статус списку';
$_['entry_style']      		= 'Тег стиль 1';
$_['entry_style_2']      	= 'Тег стиль 2';
$_['entry_processing_min']  = 'Час обробки мін.';
$_['entry_processing_max']  = 'Час обробки Макс';
$_['entry_materials']  		= 'Матеріали';
$_['entry_tags']  			= 'Мітки';
$_['entry_shipping']  		= 'Профіль доставки';
$_['entry_shop']  			= ' Розділ магазину';
$_['entry_is_custom']  		= 'Це може бути налаштовано?';
$_['entry_image']  			= 'Головне зображення';
$_['entry_image_other']		= 'Решта зображень';

// Errors
$_['error_no_shipping']  	= 'Ви не налаштували профілі доставки!';
$_['error_no_shop_secton']  = 'Ви не налаштували розділи магазину!';
$_['error_no_img_url']  	= 'Немає зображення, вибраного для завантаження';
$_['error_no_listing_id']  	= 'Немає  ID списку постачальника';
$_['error_title_length']  	= 'Назва задовга';
$_['error_title_missing']  	= 'Заголовок відсутній';
$_['error_desc_missing']  	= 'Опис є відсутній або пустий';
$_['error_price_missing']  	= 'Ціна є відсутня або пуста';
$_['error_category']  		= 'Категорію не вибрано';
$_['error_style_1_tag']  	= 'Стиль 1 тегу неприпустимий';
$_['error_style_2_tag']  	= 'Стиль 2 тегу неприпустимий';
$_['error_materials']  		= 'Можна додавати лише 13 матеріалів';
$_['error_tags']  			= 'Можна додавати лише 13 Тегів';