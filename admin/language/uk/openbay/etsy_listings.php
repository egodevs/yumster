<?php
// Headings
$_['heading_title']        		= 'Etsy перелік';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_etsy']                 = 'Etsy';

// Text
$_['text_link_saved']           = 'Був пов\'язаний елемент';
$_['text_activate']         	= 'Активувати';
$_['text_deactivate']         	= 'Відключити';
$_['text_add_link']         	= 'Додати посилання';
$_['text_delete_link']         	= 'Видалити посилання';
$_['text_delete']         		= 'Видалити список';
$_['text_status_stock']         = 'Залишок не синхронізується';
$_['text_status_ok']         	= 'Окей, тобто Так';
$_['text_status_nolink']        = 'Не пов\'язані';
$_['text_link_added']        	= 'Продукт був доданий до списку';
$_['text_link_deleted']        	= 'Продукт був видалений зі списку';
$_['text_item_ended']        	= 'Елемент видалено з Etsy';
$_['text_item_deactivated']     = 'Елемент було деактивовано в Etsy';
$_['text_item_activated']     	= 'Елемент було активовано в Etsy';
$_['text_confirm_end']          = 'Справді видалити запис?';
$_['text_confirm_deactivate']   = 'Ви дійсно бажаєте відключити запис?';
$_['text_confirm_activate']     = 'Ви дійсно бажаєте відключити запис?';

// Columns
$_['column_listing_id']			= 'Etsy ID';
$_['column_title']				= 'Заголовок';
$_['column_listing_qty']		= 'Запис кількості';
$_['column_store_qty']			= 'Кількість магазин';
$_['column_status']				= 'Повідомлення про статус';
$_['column_link_status']		= 'Посилання статус';
$_['column_action']				= 'Дія';

// Entry
$_['entry_limit']				= 'Гранична кількість сторінок';
$_['entry_status']				= 'Стан';
$_['entry_keywords']			= 'Ключові слова';
$_['entry_name']				= 'Назва товару';
$_['entry_etsy_id']				= 'Ідентифікатор елемента Etsy';

// Help
$_['help_keywords']				= 'Ключові слова, застосовуються тільки з активних записів';

// Error
$_['error_etsy']				= 'Помилка! Etsy API відповідь: ';
$_['error_product_id']			= 'КОД товару, необхідний';
$_['error_etsy_id']				= 'Код елемента Etsy необхідний';
