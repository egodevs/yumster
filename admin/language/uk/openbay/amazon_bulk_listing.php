<?php
// Heading
$_['heading_title'] 				= 'Сумарний список';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon EU';

// Text
$_['text_searching'] 				= 'Пошук';
$_['text_finished'] 				= 'Готово';
$_['text_marketplace'] 				= 'Marketplace';
$_['text_de'] 						= 'Німеччина';
$_['text_fr'] 						= 'Франція';
$_['text_es'] 						= 'Іспанія';
$_['text_it'] 						= 'Італія';
$_['text_uk'] 						= 'Великобританія';
$_['text_dont_list'] 				= 'Не перелічити';
$_['text_listing_values'] 			= 'Список значень';
$_['text_new'] 						= 'Новий';
$_['text_used_like_new'] 			= 'Вживані - як новий';
$_['text_used_very_good'] 			= 'Використовується - Дуже добре';
$_['text_used_good'] 				= 'Використовується - Добре';
$_['text_used_acceptable'] 			= 'Вживані - прийнятний';
$_['text_collectible_like_new'] 	= 'Колекційні - як новий';
$_['text_collectible_very_good'] 	= 'Колекційні - дуже хороший';
$_['text_collectible_good'] 		= 'Колекційні - хороший';
$_['text_collectible_acceptable'] 	= 'Колекційні - прийнятний';
$_['text_refurbished'] 				= 'Відремонтований';

// Entry
$_['entry_condition'] 				= 'Стан';
$_['entry_condition_note'] 			= 'Примітка до стану';
$_['entry_start_selling'] 			= 'Почати продавати';

// Column
$_['column_name'] 					= 'Назва';
$_['column_image'] 					= 'Зображення';
$_['column_model'] 					= 'Модель';
$_['column_status'] 				= 'Стан';
$_['column_matches']				= 'Відповідно';
$_['column_result'] 				= 'Результат';

// Button
$_['button_list'] 					= 'Список';

// Error
$_['error_product_sku'] 			= 'Продукт повинен мати SKU';
$_['error_searchable_fields'] 		= 'Продукт повинен мати заповнені поля ISBN, EAN, UPC або JAN';
$_['error_bulk_listing_permission'] = 'Групове відображення не дозволяється у Вашому тарифному плані, змініть тариф, будь ласка';
$_['error_select_items'] 			= 'Слід обрати принаймні один елемент для пошуку';