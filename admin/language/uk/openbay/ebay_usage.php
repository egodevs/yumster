<?php
// Headings
$_['heading_title']             = 'Використання';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_ebay']                 = 'eBay';

// Errors
$_['error_ajax_load']      		= 'На жаль не вдалося отримати відповідь. Спробуйте пізніше.';