<?php
// Heading
$_['heading_title'] 				= 'Основний список';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon США';

// Text
$_['text_searching'] 				= 'Пошук';
$_['text_finished'] 				= 'Готово';
$_['text_dont_list'] 				= 'Не перераховувати';
$_['text_listing_values'] 			= 'Список значень';
$_['text_new'] 						= 'Новий';
$_['text_used_like_new'] 			= 'Вживаний - як новий';
$_['text_used_very_good'] 			= 'Вживаний - дуже хороший стан';
$_['text_used_good'] 				= 'Вживаний - хороший стан';
$_['text_used_acceptable'] 			= 'Вживаний - прийнятний стан';
$_['text_collectible_like_new'] 	= 'Предмет колекції - як новий';
$_['text_collectible_very_good'] 	= 'Предмет колекції - дуже хороший стан';
$_['text_collectible_good'] 		= 'Предмет колекції - хороший стан';
$_['text_collectible_acceptable'] 	= 'Предмет колекції - прийнятний стан';
$_['text_refurbished'] 				= 'Відновлений';

// Entry
$_['entry_condition'] 				= 'Стан';
$_['entry_condition_note'] 			= 'Примітка про стан';
$_['entry_start_selling'] 			= 'Почати продаж';

// Column
$_['column_name'] 					= 'Ім`я';
$_['column_image'] 					= 'Зображення';
$_['column_model'] 					= 'Модель';
$_['column_status'] 				= 'Статус';
$_['column_matches'] 				= 'Відповідно';
$_['column_result'] 				= 'Результат';

// Button
$_['button_list'] 					= 'Список';

// Error
$_['error_product_sku'] 			= 'Продукт повинен мати SKU';
$_['error_searchable_fields'] 		= 'Продукт повинен мати заповнені поля ISBN, EAN, UPC або JAN';
$_['error_bulk_listing_permission'] = 'Групова синхронізація не допускається на Вашому тарифному плані. Змініть тариф';
$_['error_select_items'] 			= 'Слід обрати принаймні один елемент для пошуку';