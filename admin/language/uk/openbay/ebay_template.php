<?php
// Headings
$_['heading_title']                 = 'Список шаблонів';
$_['text_ebay']                     = 'eBay';
$_['text_openbay']                  = 'OpenBay Pro';

// Columns
$_['column_name']            		= 'Назва шаблону';
$_['column_action']            		= 'Дія';

// Entry
$_['entry_template_name']           = 'Ім`я';
$_['entry_template_html']           = 'HTML';

// Text
$_['text_added']                    = 'Додано новий шаблон';
$_['text_updated']                  = 'Шаблон було оновлено';
$_['text_deleted']                  = 'Шаблон, було видалено.';
$_['text_confirm_delete']           = 'Ви дійсно бажаєте видалити шаблон?';

// Error
$_['error_name']               		= 'Введіть ім\'я шаблону';
$_['error_permission']            	= 'Ви не маєте дозволу для редагування шаблонів';
$_['error_no_template']             = 'Ідентифікатора шаблону не існує';