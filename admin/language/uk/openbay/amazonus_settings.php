<?php
// Heading
$_['heading_title']        				= 'Заголовок';
$_['text_openbay']						= 'OpenBay Pro';
$_['text_amazon']						= 'Amazon США';

// Text
$_['text_api_status']               	= 'Стан підключення API';
$_['text_api_ok']                   	= 'З\'єднання встановлено, авторизація успішна';
$_['text_api_auth_error']           	= 'З\'єднання встановлено, помилка авторизації';
$_['text_api_error']                	= 'Помилка підключення';
$_['text_order_statuses']           	= 'Статуси замовлення';
$_['text_unshipped']                	= 'Недоставлено';
$_['text_partially_shipped']        	= 'В дорозі';
$_['text_shipped']                  	= 'Доставлено';
$_['text_canceled']                 	= 'Скасований';
$_['text_other']                    	= 'Інші';
$_['text_marketplaces']             	= 'Магазини';
$_['text_setttings_updated']        	= 'Налаштування успішно обновлені.';
$_['text_new'] 							= 'Новий';
$_['text_used_like_new'] 				= 'Користований - Як новий';
$_['text_used_very_good'] 				= 'Користований - Дуже хороший';
$_['text_used_good'] 					= 'Користований - Хороший';
$_['text_used_acceptable'] 				= 'Користований - Задовільний';
$_['text_collectible_like_new'] 		= 'Колекційний - Як новий';
$_['text_collectible_very_good'] 		= 'Колекційний - Дуже хороший';
$_['text_collectible_good'] 			= 'Колекційний - Хороший';
$_['text_collectible_acceptable'] 		= 'Колекційний - Задовільний';
$_['text_refurbished'] 					= 'Відновлений';

// Error
$_['error_permission']         			= 'Ви не маєте доступу до цього модулю';

// Entry
$_['entry_status']                 		= 'Статус';
$_['entry_token']						= 'Токен';
$_['entry_string1']              		= 'Рядок шифрування 1';
$_['entry_string2']              		= 'Рядок шифрування 2';
$_['entry_import_tax']              	= 'Податок на імпортні товари';
$_['entry_customer_group']          	= 'Групи клієнтів';
$_['entry_tax_percentage']           	= 'Додавати відсоток до вартості товару по замовчуванню';
$_['entry_default_condition']        	= 'Стан продукту по замовчуванню';
$_['entry_notify_admin']             	= 'Повідомляти адміністратора про нове замовлення';
$_['entry_default_shipping']         	= 'Доставка по замовчуванню';

// Tabs
$_['tab_settings']            			= 'Деталі API';
$_['tab_listing']                  		= 'Списки';
$_['tab_orders']                   		= 'Замовлення';

// Help
$_['help_import_tax']          			= 'Використовувати, якщо Amazon не надає дані про податки';
$_['help_customer_group']      			= 'Вибрати групу користувачів для підтвердження відправки замовлень';
$_['help_default_shipping']    			= 'Використовується в якості попередньо обраного варіанту в порядку масового оновлення';
$_['help_tax_percentage']           	= 'Додавати відсоток до вартості товару по замовчуванню';