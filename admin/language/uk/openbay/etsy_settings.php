<?php
// Headings
$_['heading_title']        		= 'Параметри Etsy';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_etsy']                 = 'Etsy';

// Text
$_['text_success']     			= 'Ваші параметри були збережені';
$_['text_status']         		= 'Стан';
$_['text_account_ok']  			= 'Підключення до Etsy ОК';
$_['text_api_other']            = 'Посилання';
$_['text_token_register']       = 'Реєстрація';
$_['text_api_ok']       		= 'API підключення ОК';
$_['text_pull_orders']    		= 'Тягнути замовлення';
$_['text_complete']    			= 'Завершено';
$_['text_failed']    			= 'Не вдалося';
$_['text_orders_imported']    	= 'Замовити тягнути було запропоновано';
$_['text_api_status']           = 'API з\'єднання';

// Entry
$_['entry_import_def_id']       = ' За замовчуванням статус імпорту (неоплачуваний):';
$_['entry_import_paid_id']      = 'Статус платежу:';
$_['entry_import_shipped_id']   = 'Статус відправлення:';
$_['entry_enc1']            	= 'API шифрування 1';
$_['entry_enc2']            	= 'API шифрування 2';
$_['entry_token']            	= 'API маркер';
$_['entry_address_format']      = 'Формат адреси за замовчуванням';

// Buttons
$_['button_pull']    			= 'Тепер витягніть';

// Error
$_['error_api_connect']         = 'Не вдалося підключитися до API';
$_['error_account_info']    	= 'Не вдалося перевірити API підключення до Etsy ';

// Tabs
$_['tab_api_info']            	= 'Деталі API';

// Help
$_['help_address_format']  		= 'Використовується тільки якщо країна вже не має налаштованого формату адреси.';