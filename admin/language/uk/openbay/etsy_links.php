<?php
// Headings
$_['heading_title']        		= 'Etsy посилання';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_etsy']                 = 'Etsy';

// Text
$_['text_loading']              = 'Завантаження новин';
$_['text_new_link']             = 'Додати нове посилання';
$_['text_current_links']        = 'Чинні посилання';
$_['text_link_saved']           = 'Був пов\'язаний елемент';

// Columns
$_['column_product']			= 'Название товара';
$_['column_item_id']			= 'Etsy ID';
$_['column_store_stock']		= 'Склад';
$_['column_etsy_stock']			= 'Etsy наявність';
$_['column_status']				= 'Посилання статус';
$_['column_action']				= 'Дія';

// Entry
$_['entry_name']				= 'Название товара';
$_['entry_etsy_id']				= 'Ідентифікатор елемента Etsy';

// Error
$_['error_product']				= 'Продукт не існує у магазині';
$_['error_stock']				= 'Не можна зв\'язати елемент, якого немає в наявності';
$_['error_product_id']			= 'КОД товару, необхідний';
$_['error_etsy_id']				= 'Ідентифікатор елемента Etsy необхідний';
$_['error_link_id']				= 'ID посилання потрібно';
$_['error_link_exists']			= 'Активне посилання вже існує для цього елемента';
$_['error_etsy']				= 'Не вдалося зв\'язати об\'єкт, Etsy API відповідь: ';
$_['error_status']				= 'Фільтр статусу необхідно';