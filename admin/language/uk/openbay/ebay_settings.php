<?php
// Heading
$_['heading_title']        			= 'Налаштунки магазину';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_ebay']						= 'eBay';

// Text
$_['text_developer']				= 'Розробник / підтримка';
$_['text_app_settings']				= 'Параметри додатку';
$_['text_default_import']			= 'Параметри імпорту за замовчуванням';
$_['text_payments']					= 'Платежі';
$_['text_notify_settings']			= 'Параметри сповіщень';
$_['text_listing']					= 'Список значень за замовчуванням';
$_['text_token_register']			= 'Реєстрація вашого eBay ключа';
$_['text_token_renew']				= 'Оновити токен eBay';
$_['text_application_settings']		= 'Параметри додатку дають змогу налаштувати спосіб роботи та інтеграції OpenBay Pro з вашою системою.';
$_['text_import_description']		= 'Налаштуйте статус замовлення під час різних етапів. Не можна використовувати статус на замовлення eBay, якого немає в цьому списку.';
$_['text_payments_description']		= 'Попередньо заповніть ваші варіанти оплати для нових розміщень, це врятує вас від їх введення для кожного нового запису, які ви створюєте.';
$_['text_allocate_1']				= 'Коли клієнт купує';
$_['text_allocate_2']				= 'Коли клієнт заплатив';
$_['text_developer_description']	= 'Ви не повинні використовувати цей область без вказівок на її використання';
$_['text_payment_paypal']			= 'Прймаємо платежі PayPal';
$_['text_payment_paypal_add']		= 'Адреса електронної пошти PayPal ';
$_['text_payment_cheque']			= 'Приймаємо чекові платежі';
$_['text_payment_card']				= 'Приймаємо платіжні карти';
$_['text_payment_desc']				= 'Дивіться опис (наприклад, банківський переказ)';
$_['text_tax_use_listing'] 			= 'Використовуйте податкові ставки з набору в eBay';
$_['text_tax_use_value']			= 'Використовувати значення для всього';
$_['text_action_warning']			= 'Ця дія є небезпечною, тому захищено паролем.';
$_['text_notifications']			= 'Контролюйте, коли клієнти отримують сповіщення від додатку. Увімкнення оновлення електронної пошти може покращити якість DSR рейтингів, коли користувач отримає оновлення про їх стан.';
$_['text_listing_1day']             = '1 день';
$_['text_listing_3day']             = '3 дні';
$_['text_listing_5day']             = '5 днів';
$_['text_listing_7day']             = '7 днів';
$_['text_listing_10day']            = '10 днів';
$_['text_listing_30day']            = '30 днів';
$_['text_listing_gtc']              = 'GTC - Дійсний до скасування';
$_['text_api_status']               = 'Стан підключення API';
$_['text_api_ok']                   = 'Підключення встановлено, строк дії токена закінчився';
$_['text_api_failed']               = 'Помилка верифікації';
$_['text_api_other']        		= 'Інші дії';
$_['text_create_date_0']            = 'Під час додавання до OpenCart';
$_['text_create_date_1']            = 'Коли створено на eBay';
$_['text_obp_detail_update']        = 'Оновіть URL-адресу вашого магазину та контактну адресу електронної пошти';
$_['text_success']					= 'Ваші параметри були збережені';

// Entry
$_['entry_status']					= 'Статус';
$_['entry_token']					= 'Ключ';
$_['entry_secret']					= 'Таємниця';
$_['entry_string1']					= 'Шифрування рядка 1';
$_['entry_string2']					= 'Шифрування рядка 2';
$_['entry_end_items']				= 'Закінчилися позиції?';
$_['entry_relist_items']			= 'Повторно виставляти, коли з’явиться на складі?';
$_['entry_disable_soldout']			= 'Приховувати продукт, коли немає на складі?';
$_['entry_debug']					= 'Увімкнути ведення журналу';
$_['entry_currency']				= 'Валюта за замовчуванням';
$_['entry_customer_group']			= 'Група покупців';
$_['entry_stock_allocate']			= 'Виділений запас';
$_['entry_created_hours']			= 'Обмеження вікової межі нового замовлення';
$_['entry_empty_data']				= 'Очистити всі дані?';
$_['entry_developer_locks']			= 'Видалити блокування замовлення?';
$_['entry_payment_instruction']		= 'Інструкції про оплату';
$_['entry_payment_immediate']		= 'Потрібна негайна оплата';
$_['entry_payment_types']			= 'Тип платежу';
$_['entry_brand_disable']			= 'Вимкнути брендоване посилання';
$_['entry_duration']				= 'Довжина списку значень за-замовчуванням';
$_['entry_measurement']				= 'Система вимірювання';
$_['entry_address_format']			= 'Формат адреси за замовчуванням';
$_['entry_timezone_offset']			= 'Зміщення часового поясу';
$_['entry_tax_listing']				= 'Податок на товар';
$_['entry_tax']						= 'Податок %, що використовується для всіх';
$_['entry_create_date']				= 'Дата створення для нових замовлень';
$_['entry_password_prompt']			= 'Будь ласка, введіть ваш пароль адміністратора';
$_['entry_notify_order_update']		= 'Оновлення замовлення';
$_['entry_notify_buyer']			= 'Нове замовлення - покупець';
$_['entry_notify_admin']			= 'Нове замовлення - адміністратор';
$_['entry_import_pending']			= 'Імпортувати неоплачені замовлення';
$_['entry_import_def_id']			= 'Стан імпорту за замовчуванням:';
$_['entry_import_paid_id']			= 'Статус платежу:';
$_['entry_import_shipped_id']		= ' Статус відправлення:';
$_['entry_import_cancelled_id']		= 'Скасовано статус:';
$_['entry_import_refund_id']		= 'Статус повернення:';
$_['entry_import_part_refund_id']	= 'Статус частково повернений:';

// Tabs
$_['tab_api_info']					= 'Деталі API';
$_['tab_setup']						= 'Налаштування';
$_['tab_defaults']					= 'Список значень за замовчуванням';

// Help
$_['help_disable_soldout']			= 'Коли елемент проданий, товар стає недоступним в OpenCart';
$_['help_relist_items'] 			= 'У разі існування посилання на елемент, воно буде перепризначено на попередній елемент у разі повернення поточного елемента на склад';
$_['help_end_items']    			= 'Якщо предмети продані, припинити їх вивід на eBay?';
$_['help_currency']     			= 'На основі валюти з вашого магазину';
$_['help_created_hours']   			= 'Замовлення вважаються новими, якщо вони менші вказаної межі (в годинах). За замовчуванням - 72 години';
$_['help_stock_allocate'] 			= 'Коли слід виділити склад з магазину?';
$_['help_payment_instruction']  	= 'Будьте максимально інформативні. Ви вимагаєте оплату в межах зазначеного терміну? Вам телефонують, щоб оплатити картою? У вас є які-небудь особливі умови оплати?';
$_['help_payment_immediate'] 		= 'Негайна оплата зупиняє покупців, що ще не сплатили. Так само як і товар, що не вважається проданим до сплати.';
$_['help_listing_tax']     			= 'Якщо ви використовуєте ставки зі списку, переконайтеся, що ваші позиції мають відповідні значення податку на eBay';
$_['help_tax']             			= 'Використовується, коли ви імпортуєте позиції або замовлення';
$_['help_duration']    				= 'GTC доступний тільки тоді, коли ви присутні в магазині eBay.';
$_['help_address_format']      		= 'Використовується тільки якщо країна вже не має налаштованого формату адреси.';
$_['help_create_date']         		= 'Виберіть який час створення з\'явиться в замовленні, коли він імпортований';
$_['help_timezone_offset']     		= 'Базується на часах. Часовий пояс GMT — 0. Працює тільки якщо eBay час використовується для створення замовлення.';
$_['help_notify_admin']   			= 'Повідомляти адміністратора магазину за допомогою за-замовчуванням заданого листа про нове замовлення';
$_['help_notify_order_update']		= 'Це для автоматичних оновлень, наприклад, якщо ви оновили замовлення в eBay, то новий статус автоматично оновлюється в вашому магазині.';
$_['help_notify_buyer']        		= 'Повідомляти користувача за допомогою листа про нове замовлення, котре задано за-замовчуванням';
$_['help_measurement']        		= 'Виберіть систему виміру, котру ви бажаєте використовувати для списків';

// Buttons
$_['button_update']             	= 'Поновити';
$_['button_repair_links']    		= 'Виправити посилання на позицію';

// Error
$_['error_api_connect']         	= 'Помилка підключення до API';