<?php
// Heading
$_['heading_title']        				= 'Обновлення магазину';
$_['text_openbay']						= 'OpenBay Pro';
$_['text_amazon']						= 'Amazon США';

// Text
$_['text_empty']                    	= 'Немае результату!';

// Entry
$_['entry_date_start']               	= 'Дата початку';
$_['entry_date_end']                 	= 'Дата кінця';

// Column
$_['column_ref']                      	= 'Посилання';
$_['column_date_requested']           	= 'Необхідна дата';
$_['column_date_updated']             	= 'Дата оновлення';
$_['column_status']                   	= 'Стан';
$_['column_sku']                      	= 'Артикул Amazon';
$_['column_stock']                    	= 'Склад';