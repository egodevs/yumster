<?php
// Heading
$_['heading_title']   			= 'Масове оновлення стану';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_confirm_title']        = 'Огляд групового оновлення статусу';

// Button
$_['button_status']           	= 'Змінити статус';
$_['button_update']           	= 'оновити';

// Column
$_['column_channel']        	= 'Канал замовлення';
$_['column_additional']     	= 'Додаткова інформація';
$_['column_comments']      		= 'Коментарі';
$_['column_notify']        		= 'Сповістити';

// Text
$_['text_confirmed']            = '%s замовлення були позначені %t';
$_['text_no_orders']            = 'Немає замовлень, що вибрані для оновлення';
$_['text_confirm_change_text']  = 'Зміна статусу замовлення';
$_['text_other']                = 'Інші';
$_['text_error_carrier_other']  = 'В замовленні відсутній запис "Інші Перевізники"!';
$_['text_web']                  = 'Веб';
$_['text_ebay']                 = 'eBay';
$_['text_amazon']               = 'Amazon ЄС';
$_['text_amazonus']             = 'Amazon США';
$_['text_etsy']             	= 'Etsy';

// Entry
$_['entry_carrier']             = 'Перевізник';
$_['entry_tracking_no']         = 'Відстеження';
$_['entry_other']               = 'Інше';