<?php
class ControllerExtensionPaymenttranzila extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/payment/tranzila');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('payment_tranzila', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
		$data['text_test'] = $this->language->get('text_test');
		$data['text_live'] = $this->language->get('text_live');
		$data['text_authorization'] = $this->language->get('text_authorization');
		$data['text_capture'] = $this->language->get('text_capture');

		$data['entry_supplier'] = $this->language->get('entry_supplier');
		$data['entry_key'] = $this->language->get('entry_key');
		$data['entry_hash'] = $this->language->get('entry_hash');
		$data['entry_server'] = $this->language->get('entry_server');
		$data['entry_mode'] = $this->language->get('entry_mode');
		$data['entry_method'] = $this->language->get('entry_method');
		$data['entry_total'] = $this->language->get('entry_total');
		$data['entry_cumorder_status'] = $this->language->get('entry_cumorder_status');
		$data['entry_decorder_status'] = $this->language->get('entry_decorder_status');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_title'] = $this->language->get('entry_title');
		$data['entry_logo'] = $this->language->get('entry_logo');
		$data['entry_height'] = $this->language->get('entry_height');
		$data['entry_width'] = $this->language->get('entry_width');
		$data['entry_callbackurl'] = $this->language->get('entry_callbackurl');
		$data['entry_faildedurl'] = $this->language->get('entry_faildedurl');
		
		
		
		$data['callbackurl'] = $this->url->link('extension/payment/tranzila/callback', '');
		$data['callbackurl']=str_replace('admin/','',$data['callbackurl']);
		$data['faildedurl'] = $this->url->link('extension/payment/tranzila/failded', '');
		$data['faildedurl']=str_replace('admin/','',$data['faildedurl']);
		

		$data['help_total'] = $this->language->get('help_total');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->error['height'])) {
			$data['error_height'] = $this->error['height'];
		} else {
			$data['error_height'] = '';
		}
		if (isset($this->error['width'])) {
			$data['error_width'] = $this->error['width'];
		} else {
			$data['error_width'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/payment/tranzila', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/payment/tranzila', 'user_token=' . $this->session->data['user_token'], true);
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true);
		
		
		$this->load->model('tool/image');

		if (isset($this->request->post['payment_tranzila_logo'])) {
			$data['payment_tranzila_logo'] = $this->request->post['payment_tranzila_logo'];
		} else {
			$data['payment_tranzila_logo'] = $this->config->get('payment_tranzila_logo');			
		}

		if ($this->config->get('payment_tranzila_logo') && file_exists(DIR_IMAGE . $this->config->get('payment_tranzila_logo')) && is_file(DIR_IMAGE . $this->config->get('payment_tranzila_logo'))) {
			$data['logo'] = $this->model_tool_image->resize($this->config->get('payment_tranzila_logo'), 100, 100);		
		} else {
			$data['logo'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}
		
		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		
		if (isset($this->request->post['payment_tranzila_height'])) {
			$data['payment_tranzila_height'] = $this->request->post['payment_tranzila_height'];
		} else {
			$data['payment_tranzila_height'] = $this->config->get('payment_tranzila_height');
		}
		
		if (isset($this->request->post['payment_tranzila_width'])) {
			$data['payment_tranzila_width'] = $this->request->post['payment_tranzila_width'];
		} else {
			$data['payment_tranzila_width'] = $this->config->get('payment_tranzila_width');
		}
		
		if (isset($this->request->post['payment_tranzila_title'])) {
			$data['payment_tranzila_title'] = $this->request->post['payment_tranzila_title'];
		} else {
			$data['payment_tranzila_title'] = $this->config->get('payment_tranzila_title');
		}
		

		if (isset($this->request->post['payment_tranzila_supplier'])) {
			$data['payment_tranzila_supplier'] = $this->request->post['payment_tranzila_supplier'];
		} else {
			$data['payment_tranzila_supplier'] = $this->config->get('payment_tranzila_supplier');
		}

		if (isset($this->request->post['payment_tranzila_total'])) {
			$data['payment_tranzila_total'] = $this->request->post['payment_tranzila_total'];
		} else {
			$data['payment_tranzila_total'] = $this->config->get('payment_tranzila_total');
		}

		if (isset($this->request->post['payment_tranzila_cumorder_status_id'])) {
			$data['payment_tranzila_cumorder_status_id'] = $this->request->post['payment_tranzila_cumorder_status_id'];
		} else {
			$data['payment_tranzila_cumorder_status_id'] = $this->config->get('payment_tranzila_cumorder_status_id');
		}
		
		if (isset($this->request->post['payment_tranzila_decorder_status_id'])) {
			$data['payment_tranzila_decorder_status_id'] = $this->request->post['payment_tranzila_decorder_status_id'];
		} else {
			$data['payment_tranzila_decorder_status_id'] = $this->config->get('payment_tranzila_decorder_status_id');
		}

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		if (isset($this->request->post['payment_tranzila_geo_zone_id'])) {
			$data['payment_tranzila_geo_zone_id'] = $this->request->post['payment_tranzila_geo_zone_id'];
		} else {
			$data['payment_tranzila_geo_zone_id'] = $this->config->get('payment_tranzila_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['payment_tranzila_status'])) {
			$data['payment_tranzila_status'] = $this->request->post['payment_tranzila_status'];
		} else {
			$data['payment_tranzila_status'] = $this->config->get('payment_tranzila_status');
		}

		if (isset($this->request->post['payment_tranzila_sort_order'])) {
			$data['payment_tranzila_sort_order'] = $this->request->post['payment_tranzila_sort_order'];
		} else {
			$data['payment_tranzila_sort_order'] = $this->config->get('payment_tranzila_sort_order');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/payment/tranzila', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/payment/tranzila')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}


		return !$this->error;
	}
}