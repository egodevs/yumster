<?php
class ControllerExtensionModuleVideo extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/video');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/module');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			if (!isset($this->request->get['module_id'])) {
				$this->model_setting_module->addModule('video', $this->request->post);
			} else {
				$this->model_setting_module->editModule($this->request->get['module_id'], $this->request->post);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		if (!isset($this->request->get['module_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/video', 'user_token=' . $this->session->data['user_token'], true)
			);
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/video', 'user_token=' . $this->session->data['user_token'] . '&module_id=' . $this->request->get['module_id'], true)
			);
		}

		if (!isset($this->request->get['module_id'])) {
			$data['action'] = $this->url->link('extension/module/video', 'user_token=' . $this->session->data['user_token'], true);
		} else {
			$data['action'] = $this->url->link('extension/module/video', 'user_token=' . $this->session->data['user_token'] . '&module_id=' . $this->request->get['module_id'], true);
		}

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$module_info = $this->model_setting_module->getModule($this->request->get['module_id']);
		}

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($module_info)) {
			$data['name'] = $module_info['name'];
		} else {
			$data['name'] = '';
		}

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($module_info)) {
			$data['status'] = $module_info['status'];
		} else {
			$data['status'] = '';
		}
		/*oshka*/
		$data['video'] = $this->language->get('text_video');
		$data['entry_video_text1'] = $this->language->get('entry_video_text1');
		$data['entry_video_text2'] = $this->language->get('entry_video_text2');
		$data['entry_video_link'] = $this->language->get('entry_video_link');
		$data['entry_video_poster'] = $this->language->get('entry_video_poster');
		$data['entry_video_file'] = $this->language->get('entry_video_poster');
		$data['entry_video_file'] = $this->language->get('entry_video_file');
		$data['video'] = $this->language->get('text_video');

		if (isset($this->request->post['video_text1'])) {
			$data['video_text1'] = $this->request->post['video_text1'];
		} elseif (!empty($module_info)) {
			$data['video_text1'] = $module_info['video_text1'];
		} else {
			$data['video_text1'] = '';
		}

		if (isset($this->request->post['video_text2'])) {
			$data['video_text2'] = $this->request->post['video_text2'];
		} elseif (!empty($module_info)) {
			$data['video_text2'] = $module_info['video_text2'];
		} else {
			$data['video_text2'] = '';
		}

		if (isset($this->request->post['video_link'])) {
			$data['video_link'] = $this->request->post['video_link'];
		} elseif (!empty($module_info)) {
			$data['video_link'] = $module_info['video_link'];
		} else {
			$data['video_link'] = '';
		}

		$this->load->model('tool/image');
		if (isset($this->request->post['video_poster_img'])) {
			$video_poster_img = $this->request->post['video_poster_img'];
		} elseif (isset($this->request->get['module_id'])) {
			//var_dump($module_info);
			$video_poster_img = $module_info['video_poster_img'];
		} else {
			$video_poster_img="";
		}
		if (is_file(DIR_IMAGE . $video_poster_img)) {
			$data['video_poster_img'] = $video_poster_img;
			$data['video_poster_img_thumb'] = $this->model_tool_image->resize($video_poster_img, 100, 100);
		} else {
			$data['video_poster_img'] = '';
			$data['video_poster_img_thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}
		$data['poster_placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		if (isset($this->request->post['video_file'])) {
			$video_file = $this->request->post['video_file'];
		} elseif (isset($this->request->get['module_id'])) {
			$video_file = $module_info['video_file'];
		} else {
			$video_file="";
		}
		if (is_file(DIR_IMAGE . $video_file)) {
			$data['video_file'] = $video_file;
		} else {
			$data['video_file'] = 'no files was selected';
		}
		/*oshka*/

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/video', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/video')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		return !$this->error;
	}
}